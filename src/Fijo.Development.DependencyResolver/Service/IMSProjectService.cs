using JetBrains.Annotations;
using Microsoft.Build.Evaluation;

namespace Fijo.Development.DependencyResolver.Service {
	public interface IMSProjectService {
		[Pure]
		string GetProperty([NotNull] Project project, string propertyName);

		[Pure]
		string GetProperty([NotNull] Project project, string propertyName, string notFoundValue);

		[Pure]
		string GetEvaluatedProperty([NotNull] Project project, string propertyName);

		[Pure]
		string GetEvaluatedProperty([NotNull] Project project, string propertyName, string notFoundValue);
	}
}