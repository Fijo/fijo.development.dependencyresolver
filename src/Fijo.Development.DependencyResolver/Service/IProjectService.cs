using System.Collections.Generic;
using Fijo.Development.DependencyResolver.Dto;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Service {
	public interface IProjectService {
		[NotNull]
		IEnumerable<string> GetTargetExtentions([NotNull] Project project);
		bool HasDocumentationFile([NotNull] Project project);
	}
}