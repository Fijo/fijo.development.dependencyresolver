using System.Linq;
using JetBrains.Annotations;
using Microsoft.Build.Evaluation;

namespace Fijo.Development.DependencyResolver.Service {
	public class MSProjectService : IMSProjectService {
		[Pure]
		public string GetProperty(Project project, string propertyName) {
			return project.Properties.Single(x => x.Name == propertyName).UnevaluatedValue;
		}

		[Pure]
		public string GetProperty(Project project, string propertyName, string notFoundValue) {
			var firstOrDefault = project.Properties.FirstOrDefault(x => x.Name == propertyName);
			return firstOrDefault != null ? firstOrDefault.UnevaluatedValue : notFoundValue;
		}

		[Pure]
		public string GetEvaluatedProperty(Project project, string propertyName) {
			return project.Properties.Single(x => x.Name == propertyName).EvaluatedValue;
		}

		[Pure]
		public string GetEvaluatedProperty(Project project, string propertyName, string notFoundValue) {
			var firstOrDefault = project.Properties.FirstOrDefault(x => x.Name == propertyName);
			return firstOrDefault != null ? firstOrDefault.EvaluatedValue : notFoundValue;
		}
	}
}