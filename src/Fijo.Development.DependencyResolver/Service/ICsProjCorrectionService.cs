using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Service {
	public interface ICsProjCorrectionService {
		[Pure]
		string Correct(string content);
	}
}