using System.Xml.XPath;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Service {
	public class XPathService : IXPathService {
		[Pure]
		public bool GetBoolAttribute(XPathNavigator navigator, XPathExpression expr, string attributeName) {
			var str = GetAttribute(navigator, expr, attributeName);
			bool result;
			bool.TryParse(str, out result);
			return result;
		}
		
		[Pure]
		public uint GetUIntAttribute(XPathNavigator navigator, XPathExpression expr, string attributeName) {
			var str = GetAttribute(navigator, expr, attributeName);
			uint result;
			uint.TryParse(str, out result);
			return result;
		}
		
		[Pure]
		public byte GetByteAttribute(XPathNavigator navigator, XPathExpression expr, string attributeName) {
			var str = GetAttribute(navigator, expr, attributeName);
			byte result;
			byte.TryParse(str, out result);
			return result;
		}

		[Pure]
		public string GetAttribute(XPathNavigator navigator, XPathExpression expr, string attributeName) {
			var singleNode = navigator.SelectSingleNode(expr);
			return singleNode != null
			       	? GetAttribute(attributeName, singleNode)
			       	: string.Empty;
		}
		
		[Pure]
		public string GetAttribute(string attributeName, XPathNavigator singleNode) {
			return attributeName == "text()"
			       	? singleNode.Value
			       	: singleNode.GetAttribute(attributeName, string.Empty);
		}
	}
}