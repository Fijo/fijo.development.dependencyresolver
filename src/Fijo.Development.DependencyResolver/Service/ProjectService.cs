using System.Collections.Generic;
using Fijo.Development.DependencyResolver.Dto;
using Fijo.Development.DependencyResolver.Enum;

namespace Fijo.Development.DependencyResolver.Service {
	public class ProjectService : IProjectService {
		public IEnumerable<string> GetTargetExtentions(Project project) {
			var buildTarget = project.Target;
			if(buildTarget.OutputType == "Library") yield return "dll";
			var debugType = buildTarget.DebugType;
			if(debugType != DebugType.None && (debugType != DebugType.Default || buildTarget.TargetConfiguration != CompileConfigurationType.Release)) yield return "pdb";

		}

		public bool HasDocumentationFile(Project project) {
			return !string.IsNullOrEmpty(project.Target.DocumentationFile);
		}

	}
}