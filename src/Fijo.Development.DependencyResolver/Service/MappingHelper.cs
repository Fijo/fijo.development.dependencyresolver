using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Service {
	public class MappingHelper : IMappingHelper {
		protected readonly IPairFactory PairFactory = Kernel.Resolve<IPairFactory>();

		[Pure]
		public IEnumerable<IPair<string, TEnum>> GetEnumDefault<TEnum>([NotNull] Func<TEnum, bool> ignorePredicate, [NotNull] Func<string, string> enumNameSelector) where TEnum : struct, IConvertible {
			return System.Enum.GetValues(typeof (TEnum)).Cast<TEnum>().Where(x => !ignorePredicate(x)).Select(
				x => PairFactory.Pair(enumNameSelector(System.Enum.GetName(typeof (TEnum), x)), x));
		}

		[Pure]
		public IEnumerable<IPair<string, TEnum>> GetEnumDefault<TEnum>([NotNull] Func<TEnum, bool> ignorePredicate) where TEnum : struct, IConvertible {
			return GetEnumDefault(ignorePredicate, DefaultEnumNameSelector);
		}
		
		[Pure]
		public IEnumerable<IPair<string, TEnum>> GetEnumDefault<TEnum>([NotNull] Func<string, string> enumNameSelector) where TEnum : struct, IConvertible {
			return GetEnumDefault<TEnum>(DefaultIgnorePredicate, enumNameSelector);
		}

		[Pure]
		public IEnumerable<IPair<string, TEnum>> GetEnumDefault<TEnum>() where TEnum : struct, IConvertible {
			return GetEnumDefault<TEnum>(DefaultIgnorePredicate, DefaultEnumNameSelector);
		}

		[Pure]
		private string DefaultEnumNameSelector(string name) {
			return name;
		}

		[Pure]
		private bool DefaultIgnorePredicate<TEnum>(TEnum value) {
			return false;
		}
	}
}