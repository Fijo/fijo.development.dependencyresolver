using System;
using System.Collections.Generic;
using Fijo.Development.DependencyResolver.Dto;
using JetBrains.Annotations;
using MSProject = Microsoft.Build.Evaluation.Project;

namespace Fijo.Development.DependencyResolver.Service {
	public interface IProjectDependencyService {
		[Pure]
		string GetPostBuildScript(Project project, IEnumerable<Project> projects);

		void SetPostBuildScript(MSProject project, string script);
		void UpdateProjects(IEnumerable<string> updateScriptProjects, IEnumerable<string> projects);
		void UpdateProjects(string inPath);
		void UpdateProjects(string inPath, [NotNull] Func<string, bool> filter);
	}
}