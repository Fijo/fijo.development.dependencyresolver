using System.Xml.XPath;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Service {
	public interface IXPathService {
		[Pure]
		bool GetBoolAttribute([NotNull] XPathNavigator navigator, [NotNull] XPathExpression expr, string attributeName);

		[Pure]
		uint GetUIntAttribute([NotNull] XPathNavigator navigator, [NotNull] XPathExpression expr, string attributeName);

		[Pure]
		byte GetByteAttribute([NotNull] XPathNavigator navigator, [NotNull] XPathExpression expr, string attributeName);

		[Pure]
		string GetAttribute([NotNull] XPathNavigator navigator, [NotNull] XPathExpression expr, string attributeName);

		[Pure]
		string GetAttribute(string attributeName, [NotNull] XPathNavigator singleNode);
	}
}