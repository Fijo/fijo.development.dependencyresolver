using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Service {
	public interface IMappingHelper {
		[Pure]
		IEnumerable<IPair<string, TEnum>> GetEnumDefault<TEnum>([NotNull] Func<TEnum, bool> ignorePredicate, [NotNull] Func<string, string> enumNameSelector) where TEnum : struct, IConvertible;

		[Pure]
		IEnumerable<IPair<string, TEnum>> GetEnumDefault<TEnum>([NotNull] Func<TEnum, bool> ignorePredicate) where TEnum : struct, IConvertible;

		[Pure]
		IEnumerable<IPair<string, TEnum>> GetEnumDefault<TEnum>([NotNull] Func<string, string> enumNameSelector) where TEnum : struct, IConvertible;

		[Pure]
		IEnumerable<IPair<string, TEnum>> GetEnumDefault<TEnum>() where TEnum : struct, IConvertible;
	}
}