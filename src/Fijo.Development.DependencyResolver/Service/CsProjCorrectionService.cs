﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Fijo.Development.DependencyResolver.Extentions;
using Fijo.Infrastructure.Model.Pair;
using Fijo.Infrastructure.Model.Pair.Interface;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Service {
	public class CsProjCorrectionService : ICsProjCorrectionService {
		private readonly Regex _regex;
		private readonly IList<IReplacePair<string>> _replaceSpecialChars;
		private readonly Regex _headRegex;
		private readonly string _contentOpen;
		private readonly string _contentClose;
		const string Content = "content";
		const string GroupNameTag = "Tag";
		const string GroupNameAttrName = "AttrName";
		const string GroupNameAttrValue = "AttrValue";

		public CsProjCorrectionService() {
			const string argumentOrTagName = @"[^=""'<>]";
			const string mayWhitespace = @"\s{0,}";
			var pattern = string.Format(@"<(?<{2}>{0}+?)(\s{{1,}}(?<{3}>{0}{{1,}}){1}={1}""(?<{4}>[^"">]*?)""){{0,}}>", argumentOrTagName, mayWhitespace, GroupNameTag, GroupNameAttrName, GroupNameAttrValue);
			_regex = new Regex(pattern, RegexOptions.Compiled);
			_headRegex = new Regex(@"﻿<\?xml[^<]+?>", RegexOptions.Compiled);
			_contentOpen = string.Format("<{0}>", Content);
			_contentClose = string.Format("</{0}>", Content);
			_replaceSpecialChars = new Dictionary<string, string>
			{
				{"<", "&lt;"},
				{">", "&gt;"},
				{"&", "&amp;"},
				{"'", "&apos;"},
				{@"""", "&quot;"}
			}.Select(x => (IReplacePair<string>) new ReplacePair<string>(x.Key, x.Value)).ToList();
		}
		
		[Pure]
		public virtual string Correct(string content) {
			var matchIndex = 0;
			return _regex.Replace(content, match => ReplaceTagSource(match, matchIndex++));
		}
		
		[Pure]
		protected virtual string WrapWithContent(string content) {
			var contentWithOpenTag = _headRegex.Replace(content, match => string.Concat(match.Groups[0].Value, _contentOpen));
			return string.Concat(contentWithOpenTag, _contentClose);
		}

		[Pure]
		protected virtual string ReplaceTagSource(Match match, int matchIndex) {
			var groups = match.Groups;
			var tag = groups[GroupNameTag].Value;
			var attrName = groups[GroupNameAttrName].Captures.Cast<Capture>();
			var attrValue = groups[GroupNameAttrValue].Captures.Cast<Capture>();
			var attrBody = string.Join(string.Empty, attrName.MultiSelect(ReplaceAttr(matchIndex), attrValue));
			return string.Format("<{0}{1}>", ReplaceTagName(tag), attrBody);
		}
		
		[Pure]
		protected virtual Func<IList<Capture>, string> ReplaceAttr(int index) {
			return x => {
			       	var replaceAttrName = ReplaceAttrName(x.First().Value);
			       	var value = MayReplaceReplacedXmlNsValueForRootElement(index, replaceAttrName, ReplaceAttrValue(x.Second().Value));
			       	return string.Format(@" {0}=""{1}""", replaceAttrName, value);
			       };
		}

		[Pure]
		protected string MayReplaceReplacedXmlNsValueForRootElement(int index, string replaceAttrName, string value) {
			return replaceAttrName == "xmlns" && index == 0 ?  string.Empty : value;
		}

		[Pure]
		protected virtual string ReplaceTagName(string tag) {
			return ReplaceSpecialChars(tag.ToLowerInvariant());
		}

		[Pure]
		protected virtual string ReplaceSpecialChars(string key) {
			return _replaceSpecialChars.Aggregate(key, (a, c) => a.Replace(c.Needle, c.Replacement));
		}

		[Pure]
		protected virtual string ReplaceAttrName(string attrName) {
			return ReplaceSpecialChars(attrName.ToLowerInvariant());
		}
		
		[Pure]
		protected virtual string ReplaceAttrValue(string attrValue) {
			return ReplaceSpecialChars(attrValue);
		}
	}
}