using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Fijo.Development.DependencyResolver.Dto;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.Model.Pair;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.FileTools;
using FijoCore.Infrastructure.LightContrib.Repositories;
using JetBrains.Annotations;
using MSProject = Microsoft.Build.Evaluation.Project;

namespace Fijo.Development.DependencyResolver.Service {
	public class ProjectDependencyService : IProjectDependencyService {
		protected readonly IProjectService ProjectService = Kernel.Resolve<IProjectService>();
		protected readonly IMapping<MSProject, Project> ProjectMapping = Kernel.Resolve<IMapping<MSProject, Project>>();
		protected readonly IPairFactory PairFactory = Kernel.Resolve<IPairFactory>();
		protected readonly FileFilterService FileFilterService = Kernel.Resolve<FileFilterService>();
		protected readonly AllFileRepository AllFileRepository = Kernel.Resolve<AllFileRepository>();
		protected const string ProjectExtention = "csproj";

		[Pure, Description("Creates PostBuildEvent in the project that copy new created assemblies in the build process to the lib folder of all projects, that are depending on the project")]
		public string GetPostBuildScript(Project project, IEnumerable<Project> projects) {
			var assemblyNameWithoutExtention = project.Target.AssemblyName;
			var assemblyName = project.Target.AssemblyName;
			var targetDirectory = project.Target.TargetDirectory;
			var fullAssemblyNameWithoutExtention = Path.Combine(targetDirectory, assemblyNameWithoutExtention);
			var targetPathes = GetTargetPathes(project, fullAssemblyNameWithoutExtention, targetDirectory).ToList();
			var targetExtentions = targetPathes.Select(Path.GetExtension).ToList();
			var otherProjectReferences = projects.Select(x => PairFactory.Pair(x, x.Files.References.SingleOrDefault(y => !string.IsNullOrEmpty(y.Path) && y.Name == assemblyName)))
				.Where(x => x.Second != null);

			var commands = otherProjectReferences.SelectMany(x => GetCommand(x, targetPathes, targetExtentions));

			var script = string.Join(Environment.NewLine, commands);
			return script;
		}
		
		[Pure]
		protected IEnumerable<string> GetTargetPathes([NotNull] Project project, [NotNull] string fullAssemblyNameWithoutExtention, [NotNull] string targetDirectory) {
			var targetPathes = ProjectService.GetTargetExtentions(project).Select(x => string.Format("{0}.{1}", fullAssemblyNameWithoutExtention, x));
			if (ProjectService.HasDocumentationFile(project)) targetPathes = targetPathes.AddReturn(Path.Combine(targetDirectory, project.Target.DocumentationFile));
			return targetPathes;
		}

		[Pure]
		protected IEnumerable<string> GetCommand([NotNull] IPair<Project, Reference> x, [NotNull] IEnumerable<string> targetPaths, [NotNull] IList<string> targetExtentions) {
			#region PreCondition
			#if DEBUG
			targetPaths = targetPaths.ToList();
			Debug.Assert(targetExtentions.Count == targetPaths.Count(), "targetExtentions.Count == targetPaths.Count");
			#endif
			#endregion
			var path = x.Second.Path;
			var destinationPath = Path.Combine(x.First.DictionaryPath, Path.GetDirectoryName(path), Path.GetFileNameWithoutExtension(path));
			return targetPaths.Select(MapToCopyPair(targetExtentions, destinationPath))
				.Where(pair => pair.First != pair.Second)
				.Distinct()
				.Select(MapCopyPairToCommand());
		}

		[NotNull]
		private Func<Pair<string, string>, string> MapCopyPairToCommand() {
			return pair => string.Format(@"copy ""{0}"" ""{1}""", EscapeQoutes(pair.First), EscapeQoutes(pair.Second));
		}

		[NotNull]
		private Func<string, int, Pair<string, string>> MapToCopyPair([NotNull] IList<string> targetExtentions, [NotNull] string destinationPath) {
			return (targetPath, i) => {
				#region PreCondition
				Debug.Assert(targetPath.EndsWith(targetExtentions[i]), "targetPath.EndsWith(targetExtentions[i])");
				#endregion
				var currentDestination = string.Concat(destinationPath, targetExtentions[i]);
				return new Pair<string, string>(targetPath, currentDestination);
			};
		}

		// ToDo use CommandService.EscapeQoutes
		[Pure]
		private string EscapeQoutes(string value) {
			return value.Replace("\"", "\\\"");
		}
		
		public void SetPostBuildScript(MSProject project, string script) {
			project.SetProperty("PostBuildEvent", script);
		}
		
		public void UpdateProjects(IEnumerable<string> updateScriptProjects, IEnumerable<string> projects) {
			#region PreCondition
			#if DEBUG
			projects = projects.ToList();
			updateScriptProjects = updateScriptProjects.ToList();
			Debug.Assert(projects.ContainsAll(updateScriptProjects), "projects.ContainsAll(updateScriptProjects)");
			#endif
			#endregion
			var msProjects = projects.Select(x => new MSProject(x));
			var projectDict = msProjects.Select(msProject => PairFactory.Pair(msProject, ProjectMapping.Map(msProject)))
					.ToDictionary(x => x.Second.Path, x => x);

			updateScriptProjects.ForEach(currentPath => InternalUpdateProject(currentPath, projectDict));
		}

		[PublicAPI]
		protected void InternalUpdateProject([NotNull] string currentPath, [NotNull] IDictionary<string, IPair<MSProject, Project>> projectDict) {
			#region PreCondition
			Debug.Assert(projectDict.ContainsKey(currentPath), "projectDict.ContainsKey(currentPath)");
			#endregion
			var currentPair = projectDict[currentPath];
			var currentMsProject = currentPair.First;
			var currentProject = currentPair.Second;

			var projectsWithoutCurrent = projectDict.Where(x => x.Key != currentPath).Select(x => x.Value.Second);

			var script = GetPostBuildScript(currentProject, projectsWithoutCurrent);
			SetPostBuildScript(currentMsProject, script);
			currentMsProject.Save();
		}

		public void UpdateProjects(string inPath) {
			UpdateProjects(inPath, DefaultFilter);
		}

		public void UpdateProjects(string inPath, Func<string, bool> filter) {
			var files = AllFileRepository.Get(inPath);
			var candidates = FileFilterService.FilterByExt(files, ProjectExtention).Where(x => !filter(x)).Execute();
			UpdateProjects(candidates, candidates);
		}

		[Pure]
		private bool DefaultFilter(string path) {
			return false;
		}
	}
}