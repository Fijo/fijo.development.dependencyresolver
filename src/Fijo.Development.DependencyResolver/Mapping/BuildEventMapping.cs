using Fijo.Development.DependencyResolver.Dto;
using Fijo.Development.DependencyResolver.Service;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;
using Project = Microsoft.Build.Evaluation.Project;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class BuildEventMapping : IMapping<Project, BuildEvent> {
		protected readonly IMSProjectService ImsProjectService = Kernel.Resolve<IMSProjectService>();

		#region Implementation of IMapping<in Project,out BuildEvent>
		[Pure]
		public BuildEvent Map(Project project) {
			return new BuildEvent
			{
				PostBuildEvent = GetValue(project, "PostBuildEvent"),
				PreBuildEvent = GetValue(project, "PreBuildEvent")
			};
		}

		[Pure]
		private string GetValue(Project project, string propertyName) {
			return ImsProjectService.GetProperty(project, propertyName, string.Empty);
		}
		#endregion
	}
}