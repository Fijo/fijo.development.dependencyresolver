using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Mapping;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Mapping {
	public abstract class DualDictMappingBase<T1, T2> : DictMappingBase<T1, T2>, IMapping<T2, T1> {
		[NotNull] protected readonly IDictionary<T2, T1> BackContent;

		protected DualDictMappingBase() {
			BackContent = Content.ToDictionary(x => x.Value, x => x.Key);
		}

		#region Implementation of IMapping<in T2,out T1>
		public T1 Map(T2 source) {
			return InternalMap(BackContent, source);
		}
		#endregion
	}
}