using System.Diagnostics;
using System.Linq;
using Fijo.Development.DependencyResolver.Dto;
using Fijo.Infrastructure.DesignPattern.Mapping;
using JetBrains.Annotations;
using Microsoft.Build.Evaluation;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class ReferenceMapping : IMapping<ProjectItem, Reference> {
		#region Implementation of IMapping<in ResolvedImport,out Reference>
		[Pure]
		public Reference Map(ProjectItem projectItem) {
			return new Reference
			{
				Name = GetAssemblyName(projectItem),
				Path = GetPath(projectItem),
			};
		}

		[Pure]
		private string GetAssemblyName(ProjectItem projectItem) {
			var evaluatedInclude = projectItem.EvaluatedInclude;
			var nameEnd = evaluatedInclude.IndexOf(", ");
			if(nameEnd == -1) return evaluatedInclude;
			Debug.Assert(nameEnd != 0, "nameEnd != 0");
			return evaluatedInclude.Substring(0, nameEnd);
		}

		[Pure]
		private string GetPath(ProjectItem projectItem) {
			var singleOrDefault = projectItem.Metadata.SingleOrDefault(y => y.Name == "HintPath");
			return singleOrDefault != null ? singleOrDefault.EvaluatedValue : string.Empty;
		}
		#endregion
	}
}