using System.Collections.Generic;
using System.Linq;
using Fijo.Development.DependencyResolver.Enum;
using Fijo.Development.DependencyResolver.Service;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class ArchitectureMapping : DualDictMappingBase<string, Architecture> {
		#region Overrides of DictMappingBase<string,Architecture>
		protected override IEnumerable<IPair<string, Architecture>> GetContent() {
			var pairFactory = Kernel.Resolve<IPairFactory>();
			var mappingHelper = Kernel.Resolve<IMappingHelper>();
			return mappingHelper.GetEnumDefault<Architecture>(x => x.IsIn(Architecture.Default, Architecture.AnyCPU), x => x.ToLowerInvariant()).Concat(
				new List<IPair<string, Architecture>>{
					pairFactory.Pair("AnyCPU", Architecture.AnyCPU),
					pairFactory.Pair("", Architecture.Default)
				}
			);
		}
		#endregion
	}
}