using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.XPath;
using Fijo.Development.DependencyResolver.Dto;
using Fijo.Development.DependencyResolver.Enum;
using Fijo.Development.DependencyResolver.Exceptions;
using Fijo.Development.DependencyResolver.Extentions;
using Fijo.Development.DependencyResolver.Service;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;
using JetBrains.Annotations;
using Project = Microsoft.Build.Evaluation.Project;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class BuildTargetMapping : IMapping<Project, BuildTarget> {
		protected readonly IMSProjectService IMSProjectService = Kernel.Resolve<IMSProjectService>();
		protected readonly IMapping<string, CompileConfigurationType> CompileConfigurationMapping = Kernel.Resolve<IMapping<string, CompileConfigurationType>>();
		protected readonly IMapping<string, Architecture> ArchitectureMapping = Kernel.Resolve<IMapping<string, Architecture>>();
		protected readonly IMapping<string, DebugType> DebugTypeMapping = Kernel.Resolve<IMapping<string, DebugType>>();
		protected readonly IMapping<string, CompilerErrorReport> CompilerErrorReportMapping = Kernel.Resolve<IMapping<string, CompilerErrorReport>>();
		protected readonly IMapping<string, Runtime> RuntimeMapping = Kernel.Resolve<IMapping<string, Runtime>>();
		protected readonly IXPathService XPathService = Kernel.Resolve<IXPathService>();
		protected const char InnerStringSeparator = '|';
		protected const char DefineConstantSeperator = ';';
		
		[Pure]
		public BuildTarget Map(Project project) {
			var condition = DictParseExpression("");
			
			return new BuildTarget
			{
				AppDesignerFolder = IMSProjectService.GetProperty(project, "AppDesignerFolder"),
				AssemblyName = IMSProjectService.GetProperty(project, "AssemblyName"),
				DebugSymbols = GetBoolProperty(project, "DebugSymbols"),
				DebugType = GetFor(project, "DebugType", DebugTypeMapping),
				DefineConstants = GetDefineConstants(project, "DefineConstants"),
				ErrorReporting = GetFor(project, "ErrorReport", CompilerErrorReportMapping),
				FileAlignment = uint.Parse(IMSProjectService.GetProperty(project, "FileAlignment")),
				ForConfiguration = GetFor(condition, "Configuration", CompileConfigurationMapping),
				ForArchitecture = GetFor(condition, "Platform", ArchitectureMapping),
				Optimize = GetBoolProperty(project, "Optimize"),
				OutputType = IMSProjectService.GetProperty(project, "OutputType"),
				ProductVersion = IMSProjectService.GetProperty(project, "ProductVersion"),
				ProjectGuid = IMSProjectService.GetProperty(project, "ProjectGuid"),
				RootNamespace = IMSProjectService.GetProperty(project, "RootNamespace"),
				SchemaVersion = IMSProjectService.GetProperty(project, "SchemaVersion"),
				DocumentationFile = IMSProjectService.GetProperty(project, "DocumentationFile", string.Empty),
				TargetArchitecture = GetFor(project, "Platform", ArchitectureMapping),
				TargetConfiguration = GetFor(project, "Configuration", CompileConfigurationMapping),
				TargetDirectory = IMSProjectService.GetProperty(project, "OutputPath"),
				TargetRuntimeVersion = GetFor(project, "TargetFrameworkVersion", RuntimeMapping),
				WarningLevel = byte.Parse(IMSProjectService.GetProperty(project, "WarningLevel"))
			};
		}

		[Pure]
		private IList<string> GetDefineConstants(Project project, string propertyName) {
			var attribute = IMSProjectService.GetProperty(project, propertyName);
			if(string.IsNullOrEmpty(attribute)) return new List<string>();
			return attribute.Split(DefineConstantSeperator);
		}


		[Pure]
		private bool GetBoolProperty(Project project, string propertyName) {
			bool result;
			bool.TryParse(IMSProjectService.GetProperty(project, propertyName), out result);
			return result;
		}

		[Pure]
		private T GetFor<T>(Project project, string propertyName, IMapping<string, T> mapping) {
			var forStr = IMSProjectService.GetProperty(project, propertyName);
			return mapping.Map(forStr);
		}

		[Pure]
		private T GetFor<T>(IDictionary<string, string> dict, string propertyName, IMapping<string, T> mapping) {
			string forStr;
			if(!dict.TryGetValue(propertyName, out forStr)) forStr = string.Empty;
			var @for = mapping.Map(forStr);
			return @for;
		}

		[Pure]
		private IDictionary<string, string> DictParseExpression(string value) {
			return ParseExpression(value).ToDictionary();
		}

		[Pure]
		private IEnumerable<KeyValuePair<string, string>> ParseExpression(string value) {
			var parts = value.Split('=');
			if(parts.Count() != 2) return Enumerable.Empty<KeyValuePair<string, string>>(); // ToDo may handle this in another way
			var keys = GetPart(parts.First());
			var values = GetPart(parts.Second());
			Debug.Assert(keys.Count == values.Count, "keys.Count == value.Count");
			var cleanKeys = keys.Select(ExtractExpressionContent);
			return cleanKeys.MultiSelect(x => new KeyValuePair<string, string>(FirstExtention.First<string>(x), SecondExtention.Second<string>(x)), values);
		}
		
		[Pure]
		private IList<string> GetPart(string value) {
			return RemoveWrappedQuotes(value.Trim()).Split(InnerStringSeparator);
		}

		[Pure]
		private string ExtractExpressionContent(string value) {
			if(value.Length > 3 && value.StartsWith("$(") && value.Last() == ')')
				throw new InvalidFormatException("value", @"value.Length > 3 && value.StartsWith(""$("") && value.Last() == ')'", value);
			return value.Substring(2, value.Length - 3);
		}
		
		[Pure]
		private string RemoveWrappedQuotes(string value) {
			if(value.Length >= 2 && value.First() != '\'' && value.Last() != '\'')
				throw new InvalidFormatException("value", @"value.Length >= 2 && value.First() != '\'' && value.Last() != '\''", value);
			return value.Substring(1, value.Length -2);
		}
	}
}