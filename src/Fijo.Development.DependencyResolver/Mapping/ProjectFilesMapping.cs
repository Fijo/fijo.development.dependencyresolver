using System.Linq;
using Fijo.Development.DependencyResolver.Dto;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;
using Microsoft.Build.Evaluation;
using Project = Microsoft.Build.Evaluation.Project;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class ProjectFilesMapping : IMapping<Project, ProjectFiles> {
		protected readonly IMapping<ProjectItem, Reference> ReferenceMapping = Kernel.Resolve<IMapping<ProjectItem, Reference>>();

		#region Implementation of IMapping<in XPathNavigator,out BuildEvent>
		[Pure]
		public ProjectFiles Map(Project project) {
			return new ProjectFiles
			{
				References = project.Items.Where(x => x.ItemType == "Reference").Select(ReferenceMapping.Map).ToList()
			};
		}
		#endregion
	}
}