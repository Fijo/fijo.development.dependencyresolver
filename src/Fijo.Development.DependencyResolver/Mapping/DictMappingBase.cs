using System.Collections.Generic;
using Fijo.Development.DependencyResolver.Exceptions;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Mapping {
	public abstract class DictMappingBase<TSource, TResult> : IMapping<TSource, TResult> {
		[NotNull] protected readonly IDictionary<TSource, TResult> Content;

		protected DictMappingBase() {
// ReSharper disable DoNotCallOverridableMethodsInConstructor
			Content = GetContent().ToDictionary();
// ReSharper restore DoNotCallOverridableMethodsInConstructor
		}

		[NotNull]
		protected abstract IEnumerable<IPair<TSource, TResult>> GetContent();

		#region Implementation of IMapping<in TSource,out TResult>
		public TResult Map(TSource source) {
			return InternalMap(Content, source);
		}
		#endregion

		[Pure]
		protected TInnerResult InternalMap<TInnerSource, TInnerResult>([NotNull] IDictionary<TInnerSource, TInnerResult> content, TInnerSource source) {
			TInnerResult result;
			if(!content.TryGetValue(source, out result)) throw new InvalidMappingFormatException("source", "content.ContainsKey(source)", source.ToString());
			return result;
		}
	}
}