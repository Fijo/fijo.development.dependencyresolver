using System.Collections.Generic;
using Fijo.Development.DependencyResolver.Enum;
using Fijo.Development.DependencyResolver.Service;
using Fijo.Infrastructure.Model.Pair;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class DebugTypeMapping : DualDictMappingBase<string, DebugType> {
		#region Overrides of DictMappingBase<string,DebugType>
		protected override IEnumerable<IPair<string, DebugType>> GetContent() {
			var pairFactory = Kernel.Resolve<IPairFactory>();
			var mappingHelper = Kernel.Resolve<IMappingHelper>();
			return mappingHelper.GetEnumDefault<DebugType>(x => x.IsIn(DebugType.Default), x => x.ToLowerInvariant()).AddReturn(
				pairFactory.Pair("", DebugType.Default)
			);
		}
		#endregion
	}
}