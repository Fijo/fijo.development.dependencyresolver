using Fijo.Development.DependencyResolver.Dto;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using JetBrains.Annotations;
using MSProject = Microsoft.Build.Evaluation.Project;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class ProjectMapping : IMapping<MSProject, Project> {
		protected readonly IMapping<MSProject, BuildTarget> BuildTargetMapping = Kernel.Resolve<IMapping<MSProject, BuildTarget>>();
		protected readonly IMapping<MSProject, BuildEvent> BuildEventMapping = Kernel.Resolve<IMapping<MSProject, BuildEvent>>();
		protected readonly IMapping<MSProject, ProjectFiles> ProjectFilesMapping = Kernel.Resolve<IMapping<MSProject, ProjectFiles>>();
		
		#region Implementation of IMapping<in MSProject,out Project>
		[Pure]
		public Project Map(MSProject project) {
			return new Project
			{
				Target = BuildTargetMapping.Map(project),
				Files = ProjectFilesMapping.Map(project),
				Event = BuildEventMapping.Map(project),
				Path = project.FullPath,
				DictionaryPath = project.DirectoryPath
			};
		}
		#endregion
	}
}