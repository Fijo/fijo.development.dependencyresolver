using System.Collections.Generic;
using Fijo.Development.DependencyResolver.Enum;
using Fijo.Development.DependencyResolver.Service;
using Fijo.Infrastructure.Model.Pair;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class CompileConfigurationTypeMapping : DualDictMappingBase<string, CompileConfigurationType> {
		#region Overrides of DictMappingBase<string,Architecture>
		protected override IEnumerable<IPair<string, CompileConfigurationType>> GetContent() {
			var pairFactory = Kernel.Resolve<IPairFactory>();
			var mappingHelper = Kernel.Resolve<IMappingHelper>();
			return mappingHelper.GetEnumDefault<CompileConfigurationType>(x => x == CompileConfigurationType.Default).AddReturn(
				pairFactory.Pair("", CompileConfigurationType.Default)
			);
		}
		#endregion
	}
}