using System.Collections.Generic;
using Fijo.Development.DependencyResolver.Enum;
using Fijo.Development.DependencyResolver.Service;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class CompilerErrorReportMapping : DualDictMappingBase<string, CompilerErrorReport> {
		#region Overrides of DictMappingBase<string,Architecture>
		protected override IEnumerable<IPair<string, CompilerErrorReport>> GetContent() {
			var pairFactory = Kernel.Resolve<IPairFactory>();
			var mappingHelper = Kernel.Resolve<IMappingHelper>();
			return mappingHelper.GetEnumDefault<CompilerErrorReport>(x => x == CompilerErrorReport.Default, x => x.ToLowerInvariant()).AddReturn(
				pairFactory.Pair(string.Empty, CompilerErrorReport.Default)
			);
		}
		#endregion
	}
}