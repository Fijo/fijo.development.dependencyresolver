using System.Collections.Generic;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace Fijo.Development.DependencyResolver.Mapping {
	public class RuntimeMapping : DualDictMappingBase<string, Runtime> {
		#region Overrides of DictMappingBase<string,Architecture>
		protected override IEnumerable<IPair<string, Runtime>> GetContent() {
			var pairFactory = Kernel.Resolve<IPairFactory>();
			return new List<IPair<string, Runtime>>
			{
				pairFactory.Pair(string.Empty, Runtime.Default),
				pairFactory.Pair("v4.5", Runtime.Net4_5),
				pairFactory.Pair("v4.0", Runtime.Net4_0),
				pairFactory.Pair("v3.5", Runtime.Net3_5),
				pairFactory.Pair("v3.0", Runtime.Net3_0),
				pairFactory.Pair("v2.0", Runtime.Net2_0),
				pairFactory.Pair("v1.1", Runtime.Net1_1),
				pairFactory.Pair("v1.0", Runtime.Net1_0)
			};
		}
		#endregion
	}
}