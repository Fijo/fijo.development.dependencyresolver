namespace Fijo.Development.DependencyResolver.Enum {
	public enum CompileConfigurationType{
		Default,
		Debug,
		Release
	}
}