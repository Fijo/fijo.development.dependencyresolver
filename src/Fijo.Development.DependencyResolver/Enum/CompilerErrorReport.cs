namespace Fijo.Development.DependencyResolver.Enum {
	public enum CompilerErrorReport {
		Default,
		None,
		Prompt,
		Send,
		Queue
	}
}