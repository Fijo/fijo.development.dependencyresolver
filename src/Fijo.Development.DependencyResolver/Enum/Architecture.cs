using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Development.DependencyResolver.Enum {
	public enum Architecture {
		Default,
		AnyCPU,
		X86,
		X64,
	}
}