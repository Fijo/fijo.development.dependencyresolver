namespace Fijo.Development.DependencyResolver.Enum {
	public enum DebugType {
		Default,
		Full,
		PdbOnly,
		None
	}
}