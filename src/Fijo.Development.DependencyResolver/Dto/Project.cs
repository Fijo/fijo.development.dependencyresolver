using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Development.DependencyResolver.Dto {
	[Dto]
	public class Project {
		[About("CsProjPath")]
		public string Path;
		public string DictionaryPath;
		public BuildTarget Target;
		public ProjectFiles Files;
		public BuildEvent Event;
	}
}