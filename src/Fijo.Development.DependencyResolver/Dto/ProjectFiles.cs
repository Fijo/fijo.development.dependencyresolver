using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.Development.DependencyResolver.Dto {
	[Dto]
	public class ProjectFiles {
		public IList<Reference> References;
	}
}