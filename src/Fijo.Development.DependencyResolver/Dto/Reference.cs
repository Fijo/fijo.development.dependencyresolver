using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Development.DependencyResolver.Dto {
	[Dto]
	public class Reference {
		[About("LibraryName")]
		public string Name;
		public string Path;
	}
}