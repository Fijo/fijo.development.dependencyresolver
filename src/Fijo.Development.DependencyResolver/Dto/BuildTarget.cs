using System.Collections.Generic;
using Fijo.Development.DependencyResolver.Enum;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Enums;

namespace Fijo.Development.DependencyResolver.Dto {
	[Dto]
	public class BuildTarget {
		[About("BinDirectory"), About("OutputDirectory")]
		public string TargetDirectory;
		public CompileConfigurationType ForConfiguration;
		[About("ForPlatform")]
		public Architecture ForArchitecture;
		public CompileConfigurationType TargetConfiguration;
		[About("TargetPlatform")]
		public Architecture TargetArchitecture;
		public string ProductVersion;
		public string SchemaVersion;
		public string ProjectGuid;
		public string OutputType;
		[About("PropertiesFolder")]
		public string AppDesignerFolder;
		public string RootNamespace;
		public string AssemblyName;
		[About("TargetFrameworkVersion")]
		public Runtime TargetRuntimeVersion;
		public uint FileAlignment;
		public bool DebugSymbols;
		public DebugType DebugType;
		public bool Optimize;
		public IList<string> DefineConstants;
		public CompilerErrorReport ErrorReporting;
		public byte WarningLevel;
		public bool SignAssembly;
		public bool DelaySign;
		[About("AssemblyOriginatorKeyFile")]
		public string KeyFile;
		[About("XMLDocumentationFile")]
		public string DocumentationFile;
	}
}