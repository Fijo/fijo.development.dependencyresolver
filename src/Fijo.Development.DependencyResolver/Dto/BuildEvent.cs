using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.Development.DependencyResolver.Dto {
	[Dto]
	public class BuildEvent {
		public string PostBuildEvent;
		public string PreBuildEvent;
	}
}