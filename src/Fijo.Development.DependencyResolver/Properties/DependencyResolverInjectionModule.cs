﻿using System.Collections.Generic;
using Fijo.Development.DependencyResolver.Dto;
using Fijo.Development.DependencyResolver.Enum;
using Fijo.Development.DependencyResolver.Mapping;
using Fijo.Development.DependencyResolver.Service;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using Microsoft.Build.Evaluation;
using GKernel = FijoCore.Infrastructure.DependencyInjection.InitKernel.Kernel;
using MSProject = Microsoft.Build.Evaluation.Project;
using Project = Fijo.Development.DependencyResolver.Dto.Project;

namespace Fijo.Development.DependencyResolver.Properties
{
    public class DependencyResolverInjectionModule : IExtendedNinjectModule {
        public IKernel Kernel { get; private set; }
    	public IDictionary<IExtendedInitHandler, object> ExtendedInit { get; set; }

    	public void AddModule(IKernel kernel) {}

    	public void OnLoad(IKernel kernel) {
    		Kernel = kernel;
			
    		kernel.Bind<IMappingHelper>().To<MappingHelper>().InSingletonScope();
    		kernel.Bind<IXPathService>().To<XPathService>().InSingletonScope();
    		kernel.Bind<IMSProjectService>().To<MSProjectService>().InSingletonScope();
			kernel.Bind<IMapping<string, CompileConfigurationType>>().To<CompileConfigurationTypeMapping>().InSingletonScope();
    		kernel.Bind<IMapping<CompileConfigurationType, string>>().ToMethod(x => (IMapping<CompileConfigurationType, string>) GKernel.Resolve<IMapping<string, CompileConfigurationType>>()).InSingletonScope();
    		kernel.Bind<IMapping<string, Architecture>>().To<ArchitectureMapping>().InSingletonScope();
    		kernel.Bind<IMapping<Architecture, string>>().ToMethod(x => (IMapping<Architecture, string>) GKernel.Resolve<IMapping<string, Architecture>>()).InSingletonScope();
			kernel.Bind<IMapping<string, DebugType>>().To<DebugTypeMapping>().InSingletonScope();
    		kernel.Bind<IMapping<DebugType, string>>().ToMethod(x => (IMapping<DebugType, string>) GKernel.Resolve<IMapping<string, DebugType>>()).InSingletonScope();
			kernel.Bind<IMapping<string, CompilerErrorReport>>().To<CompilerErrorReportMapping>().InSingletonScope();
			kernel.Bind<IMapping<CompilerErrorReport, string>>().ToMethod(x => (IMapping<CompilerErrorReport, string>) GKernel.Resolve<IMapping<string, CompilerErrorReport>>()).InSingletonScope();
    		kernel.Bind<IMapping<string, Runtime>>().To<RuntimeMapping>().InSingletonScope();
    		kernel.Bind<IMapping<Runtime, string>>().ToMethod(x => (IMapping<Runtime, string>) GKernel.Resolve<IMapping<string, Runtime>>()).InSingletonScope();
    		kernel.Bind<IMapping<MSProject, BuildEvent>>().To<BuildEventMapping>().InSingletonScope();
    		kernel.Bind<IMapping<MSProject, BuildTarget>>().To<BuildTargetMapping>().InSingletonScope();
    		kernel.Bind<IMapping<ProjectItem, Reference>>().To<ReferenceMapping>().InSingletonScope();
    		kernel.Bind<IMapping<MSProject, ProjectFiles>>().To<ProjectFilesMapping>().InSingletonScope();
    		kernel.Bind<IMapping<MSProject, Project>>().To<ProjectMapping>().InSingletonScope();
    		kernel.Bind<ICsProjCorrectionService>().To<CsProjCorrectionService>().InSingletonScope();
    		kernel.Bind<IProjectService>().To<ProjectService>().InSingletonScope();
			kernel.Bind<IProjectDependencyService>().To<ProjectDependencyService>().InSingletonScope();
    	}

    	public void Init(IKernel kernel) {}

    	public void RegisterHandlers(IKernel kernel) {}

    	public void Validate() {}
		
    	public void Loaded(IKernel kernel) {}

        public void OnUnload(IKernel kernel) {}

    	public string Name
        {
            get { return "DependencyResolverInjectionModule"; }
        }
    }
}