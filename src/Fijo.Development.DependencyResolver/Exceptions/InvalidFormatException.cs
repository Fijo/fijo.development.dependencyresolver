using System;
using JetBrains.Annotations;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolver.Exceptions {
	[Description("will occur if an argument does not have a valid format")]
	public class InvalidFormatException : InvalidOperationException {
		public string ParamName { get; set; }
		public string ValidationCondition { get; set; }
		public string ActualValue { get; set; }
		
		public InvalidFormatException(string message, string actualValue = "", Exception innerException = null) : base(message, innerException) {
			ActualValue = actualValue;
		}

		public InvalidFormatException([InvokerParameterName] string paramName, string validationCondition) {
			ParamName = paramName;
			ValidationCondition = validationCondition;
		}

		public InvalidFormatException([InvokerParameterName] string paramName, string validationCondition, string actualValue) {
			ParamName = paramName;
			ValidationCondition = validationCondition;
			ActualValue = actualValue;
		}
	}
}