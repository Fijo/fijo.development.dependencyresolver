using System;

namespace Fijo.Development.DependencyResolver.Exceptions {
	public class InvalidMappingFormatException : InvalidFormatException {
		public InvalidMappingFormatException(string message, string actualValue = "", Exception innerException = null) : base(message, actualValue, innerException) {}
		public InvalidMappingFormatException(string paramName, string validationCondition) : base(paramName, validationCondition) {}
		public InvalidMappingFormatException(string paramName, string validationCondition, string actualValue) : base(paramName, validationCondition, actualValue) {}
	}
}