using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolver.Extentions {
	public static class MultiSelectExtention {
		[NotNull, Pure]
		public static IEnumerable<TResult> MultiSelect<TResult, TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<IList<TSource>, TResult> mergeSelector, params IEnumerable<TSource>[] otherCollections) {
			#region PreCondition
			Debug.Assert(otherCollections != null, "otherCollections != null");
			#endregion
			return InternalMultiSelect(me.IntoEnumerable().Concat(otherCollections), mergeSelector);
		}

		[NotNull, Pure]
		private static IEnumerable<TResult> InternalMultiSelect<TResult, TSource>([NotNull] IEnumerable<IEnumerable<TSource>> collections, [NotNull] Func<IList<TSource>, TResult> mergeSelector) {
			var enumerators = collections.Select(x => x.GetEnumerator()).ToList();
			while(enumerators.All(x => x.MoveNext())) {
				var currents = enumerators.Select(x => x.Current).ToList();
				yield return mergeSelector(currents);
			}
		}
	}
}