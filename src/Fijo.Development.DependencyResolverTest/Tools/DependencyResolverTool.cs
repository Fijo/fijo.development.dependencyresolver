using System.Linq;
using Fijo.Development.DependencyResolver.Service;
using Fijo.Development.DependencyResolverTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Tools {
	[TestFixture]
	public class DependencyResolverTool {
		private IProjectDependencyService _projectDependencyService;
		
		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_projectDependencyService = Kernel.Resolve<IProjectDependencyService>();
		}

		[TestCase(@"C:\Projekte\", @"C:\Projekte\_", "_Full.csproj", @" - Kopie\", @"\Res\Proj\", @"C:\Projekte\GitSharp\", @"C:\Projekte\libgit2sharp\", @"C:\Projekte\ffmpeg-sharp\", @"C:\Projekte\mftoolkit\", @"C:\Projekte\YAXLib\", @"C:\Projekte\javascriptdotnet\", @"C:\Projekte\NanoCode\", @"C:\Projekte\wos\", @"C:\Projekte\Fijo.Development.DependencyResolver\")]
		[Ignore("Tool"), Description("Creates PostBuildEvent in the projects (for all files/ projects that are in (or in a deeper place in the folder structure but have it as a parent) the dictionary path, that is set to the folder variable that have �csproj� as extention) that copy new created assemblies in the build process to the lib folder of all projects, that are depending on the current project")]
		public void UpdateProjects(string folder, params string[] ignorePathesThatContains) {
			_projectDependencyService.UpdateProjects(folder, x => ignorePathesThatContains.Any(x.Contains));
		}
	}
}