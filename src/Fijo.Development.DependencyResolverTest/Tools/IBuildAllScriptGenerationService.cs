using System;

namespace Fijo.Development.DependencyResolverTest.Tools {
	// ToDo move to a Fijo.Development.Tools project
	public interface IBuildAllScriptGenerationService {
		void GenerateBuildAllScript(string inPath, Func<string, bool> filter, string buildAllScriptName);
	}
}