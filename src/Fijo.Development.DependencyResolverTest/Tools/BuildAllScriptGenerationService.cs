using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.FileTools;
using FijoCore.Infrastructure.LightContrib.Repositories;
using JetBrains.Annotations;

namespace Fijo.Development.DependencyResolverTest.Tools {
	// ToDo move to a Fijo.Development.Tools project
	public class BuildAllScriptGenerationService : IBuildAllScriptGenerationService {
		protected readonly FileFilterService FileFilterService = Kernel.Resolve<FileFilterService>();
		protected readonly AllFileRepository AllFileRepository = Kernel.Resolve<AllFileRepository>();
		protected const string ProjectExtention = "sln";

		public void GenerateBuildAllScript(string inPath, Func<string, bool> filter, string buildAllScriptName) {
			var candidates = GetCandidates(inPath, filter);
			var script = GetScript(candidates);
			File.WriteAllText(buildAllScriptName, script);
		}

		[NotNull, Pure]
		protected virtual IEnumerable<string> GetCandidates(string inPath, [NotNull] Func<string, bool> filter) {
			var files = AllFileRepository.Get(inPath);
			var candidates = FileFilterService.FilterByExt(files, ProjectExtention).Where(x => !filter(x));
			return candidates;
		}

		[NotNull, Pure]
		protected virtual string GetScript([NotNull] IEnumerable<string> candidates) {
			return string.Join(Environment.NewLine, candidates.Select(x => string.Format(@"msbuild ""{0}""", EscapeQoutes(x))));
		}

		// ToDo use CommandService.EscapeQoutes
		[Pure]
		private string EscapeQoutes(string value) {
			return value.Replace("\"", "\\\"");
		}
		
	}
}