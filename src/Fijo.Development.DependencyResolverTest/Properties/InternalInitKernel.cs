using Fijo.Development.DependencyResolver.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.LightContrib.Properties;

namespace Fijo.Development.DependencyResolverTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel
	{
		public override void PreInit()
		{
			LoadModules(new LightContribInjectionModule(), new DependencyResolverInjectionModule());
		}
	}
}