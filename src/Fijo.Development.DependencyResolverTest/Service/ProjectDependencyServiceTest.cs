using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Fijo.Development.DependencyResolver.Dto;
using Fijo.Development.DependencyResolver.Service;
using Fijo.Development.DependencyResolverTest.Properties;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using MSProject = Microsoft.Build.Evaluation.Project;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Service {
	[TestFixture]
	public class ProjectDependencyServiceTest {
		private IProjectDependencyService _projectDependencyService;
		private IMapping<MSProject, Project> _projectMapping;
		private readonly string _testProjectsPath = Path.Combine(Environment.CurrentDirectory, "Res", "Proj");

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_projectDependencyService = new ProjectDependencyService();
			_projectMapping = Kernel.Resolve<IMapping<MSProject, Project>>();
		}

		[Test]
		public void CorrectGivenPathes_GetPostBuildScript_MustWorkAsExpected() {
			lock (typeof (ProjectDependencyServiceTest)) {
				var testProjectsPath = _testProjectsPath;
				var pathes = new List<string>
				{
					@"documentation\src\Fijo.Infrastructure.Documentation\Fijo.Infrastructure.Documentation.csproj",
					@"designpattern\src\Fijo.Infrastructure.DesignPattern\Fijo.Infrastructure.DesignPattern.csproj",
					@"model\src\Fijo.Infrastructure.Model\Fijo.Infrastructure.Model.csproj",
					@"model\src\Fijo.Infrastructure.ModelTest\Fijo.Infrastructure.ModelTest.csproj",
					@"lightcontrib\src\LC\FijoCore.Infrastructure.LightContrib.csproj",
					@"lightcontrib\src\LCTest\FijoCore.Infrastructure.LightContribTest.csproj"
				}.Select(x => Path.Combine(testProjectsPath, x));
				var msProjects = pathes.Select(x => new MSProject(x)).ToList();
				var projects = msProjects.Select(_projectMapping.Map).ToList();

				var currentMsProject = msProjects.First();
				var currentProject = projects.First();
				var projectsWithoutCurrent = projects.Where(x => x.Target.AssemblyName != currentProject.Target.AssemblyName).ToList();
				var script = _projectDependencyService.GetPostBuildScript(currentProject, projectsWithoutCurrent);
				_projectDependencyService.SetPostBuildScript(currentMsProject, script);
				currentMsProject.Save();
				var documentationAssemblyPathWithoutExtention = Path.Combine(testProjectsPath, @"designpattern\src\Fijo.Infrastructure.DesignPattern\..\..\lib\Fijo.Infrastructure.Documentation.");
				var documentationAssemblyPath = string.Format("{0}dll", documentationAssemblyPathWithoutExtention);
				var documentationPdbPath = string.Format("{0}pdb", documentationAssemblyPathWithoutExtention);
				File.Delete(documentationAssemblyPath);
				File.Delete(documentationPdbPath);
				currentMsProject.Build();
				Assert.True(File.Exists(documentationAssemblyPath));
				Assert.True(File.Exists(documentationPdbPath));
			}
		}
	}
}