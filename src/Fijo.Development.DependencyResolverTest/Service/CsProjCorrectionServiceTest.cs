﻿using Fijo.Development.DependencyResolver.Service;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Service {
	[TestFixture]
	public class CsProjCorrectionServiceTest {
		private CsProjCorrectionService _csProjCorrectionService;

		[SetUp]
		public void SetUp() {
			new InitKernel().Init();
			_csProjCorrectionService = new CsProjCorrectionService();
		}

		[Test]
		public void Test() {
			var xml = @"
				﻿<?xml version=""1.0"" encoding=""utf-8""?>
				<Project ToolsVersion=""4.0"" DefaultTargets=""Build"" xmlns=""http://schemas.microsoft.com/developer/msbuild/2003"">
					<PropertyGroup>
						<Configuration Condition="" '$(Configuration)' == '' "">Debug</Configuration>
						<Platform Condition="" '$(Platform)' == '' "">AnyCPU</Platform>
						<ProductVersion>8.0.30703</ProductVersion>
						<SchemaVersion>2.0</SchemaVersion>
						<ProjectGuid>{A6C4590F-6C2A-41DF-8FB3-41D45E31E413}</ProjectGuid>
						<OutputType>Library</OutputType>						<AppDesignerFolder>Properties</AppDesignerFolder>
						<RootNamespace>FijoCore.Infrastructure.LightContrib</RootNamespace>
						<AssemblyName>FijoCore.Infrastructure.LightContrib</AssemblyName>
						<TargetFrameworkVersion>v4.0</TargetFrameworkVersion>
						<FileAlignment>512</FileAlignment>						<OutputPath>R:\FijoCore.Components\x86\</OutputPath>
						<DefineConstants>TRACE</DefineConstants>
						<ErrorReport>prompt</ErrorReport>
						<WarningLevel>4</WarningLevel>
						<DebugSymbols>true</DebugSymbols>						<DebugType>full</DebugType>						<Optimize>true</Optimize>
					</PropertyGroup>
				</Project>
			".Trim();

			var wanted = @"
				﻿<?xml version=""1.0"" encoding=""utf-8""?>
				<project toolsversion=""4.0"" defaulttargets=""Build"" xmlns="""">
					<propertygroup>
						<configuration condition="" &apos;$(Configuration)&apos; == &apos;&apos; "">Debug</configuration>
						<platform condition="" &apos;$(Platform)&apos; == &apos;&apos; "">AnyCPU</platform>
						<productversion>8.0.30703</productversion>
						<schemaversion>2.0</schemaversion>
						<projectguid>{A6C4590F-6C2A-41DF-8FB3-41D45E31E413}</projectguid>
						<outputtype>Library</outputtype>						<appdesignerfolder>Properties</appdesignerfolder>
						<rootnamespace>FijoCore.Infrastructure.LightContrib</rootnamespace>
						<assemblyname>FijoCore.Infrastructure.LightContrib</assemblyname>
						<targetframeworkversion>v4.0</targetframeworkversion>
						<filealignment>512</filealignment>						<outputpath>R:\FijoCore.Components\x86\</outputpath>
						<defineconstants>TRACE</defineconstants>
						<errorreport>prompt</errorreport>
						<warninglevel>4</warninglevel>
						<debugsymbols>true</debugsymbols>						<debugtype>full</debugtype>						<optimize>true</optimize>
					</propertygroup>
				</project>
			".Trim();

			var actual = _csProjCorrectionService.Correct(xml);
			Assert.AreEqual(wanted, actual);
		}
	}
}