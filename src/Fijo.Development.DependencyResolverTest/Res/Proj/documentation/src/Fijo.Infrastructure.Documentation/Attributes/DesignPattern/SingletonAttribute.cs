using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
	public class SingletonAttribute : DesignPatternAttribute {
		public SingletonAttribute() : base(Enums.DesignPattern.Singleton) {}
	}
}