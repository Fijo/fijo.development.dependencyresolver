using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	public class BridgeAttribute : DesignPatternAttribute {
		public BridgeAttribute() : base(Enums.DesignPattern.Bridge) {}
	}
}