using System;
using System.ComponentModel;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Description("Adds a related link to the function, for example of a location where you got some infos how to use something.")]
	public class RelatedLinkAttribute : LinkAttribute {
		public RelatedLinkAttribute(params string[] links) : base(links) {}
	}
}