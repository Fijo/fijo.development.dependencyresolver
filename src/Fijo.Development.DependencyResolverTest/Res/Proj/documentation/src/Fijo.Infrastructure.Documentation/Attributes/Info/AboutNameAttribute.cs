using System;
using System.ComponentModel;

namespace Fijo.Infrastructure.Documentation.Attributes.Info
{
	[AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
	[Description("Descripes an abbreviation that is used in the name of something.")]
	public class AboutNameAttribute : Attribute {
		public AboutNameAttribute(string abbreviation, string meaning) {}
	}
}