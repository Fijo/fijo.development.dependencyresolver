using System;
using System.ComponentModel;

namespace Fijo.Infrastructure.Documentation.Attributes.Info
{
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Description("Descripes a somethings general function, very short, if its name doesn�t say enough")]
	public class AboutAttribute : Attribute {
		public AboutAttribute(string about) {}
	}
}