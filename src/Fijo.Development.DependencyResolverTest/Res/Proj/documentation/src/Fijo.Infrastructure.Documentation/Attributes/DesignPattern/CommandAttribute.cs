using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Delegate | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Module , Inherited = true, AllowMultiple = false)]
	public class CommandAttribute : DesignPatternAttribute {
		public CommandAttribute() : base(Enums.DesignPattern.Command) {}
	}
}