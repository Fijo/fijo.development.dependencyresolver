using System;
using System.ComponentModel;

namespace Fijo.Infrastructure.Documentation.Attributes.Info
{
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Description("Says, that using you have to think of some thing(s) - and you should be warned because of easyly usage without the result the user expects.")]
	public class DangerousAttribute : Attribute {
		public DangerousAttribute(string why) {}
		public DangerousAttribute(string condition, string why) {}
	}
}