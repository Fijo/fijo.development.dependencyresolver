using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	public class ChainOfResponsibilityAttribute : DesignPatternAttribute {
		public ChainOfResponsibilityAttribute() : base(Enums.DesignPattern.ChainOfResponsibility) {}
	}
}