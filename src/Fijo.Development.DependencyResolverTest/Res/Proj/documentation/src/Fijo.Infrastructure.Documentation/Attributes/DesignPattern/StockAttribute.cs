using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern {
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct, Inherited = true, AllowMultiple = false)]
	public class StockAttribute : DesignPatternAttribute {
		public StockAttribute() : base(Enums.DesignPattern.Stock) {}
	}
}