using System;
using System.ComponentModel;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Description("Informs about a supported feture for the attribute target.")]
	public class SupportsAttribute : Attribute {
		public SupportsAttribute(string feture) {}
	}
}