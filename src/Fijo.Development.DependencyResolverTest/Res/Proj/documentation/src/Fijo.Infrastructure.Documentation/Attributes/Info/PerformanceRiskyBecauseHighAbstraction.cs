using System;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class PerformanceRiskyBecauseHighAbstraction : PerformanceRisky {
		public PerformanceRiskyBecauseHighAbstraction() : base("HighAbstraction") {}
		public PerformanceRiskyBecauseHighAbstraction(string why) : base(string.Concat("HighAbstraction, ", why)) {}
	}
}