using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	public class AdapterAttribute : DesignPatternAttribute {
		public AdapterAttribute() : base(Enums.DesignPattern.Adapter) {}
	}
}