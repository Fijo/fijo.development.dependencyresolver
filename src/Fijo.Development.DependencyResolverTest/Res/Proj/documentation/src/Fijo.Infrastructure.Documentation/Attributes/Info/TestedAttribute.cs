using System;

namespace Fijo.Infrastructure.Documentation.Attributes.Info
{
	[AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
	public class TestedAttribute : Attribute {
		public TestedAttribute() {}

		public TestedAttribute(string testClassInfo) {}
	}
}