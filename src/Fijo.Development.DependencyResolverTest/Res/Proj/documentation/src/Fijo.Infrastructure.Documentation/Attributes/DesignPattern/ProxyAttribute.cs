using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Delegate | AttributeTargets.Method | AttributeTargets.Module | AttributeTargets.Struct, Inherited = false, AllowMultiple = false)]
	public class ProxyAttribute : DesignPatternAttribute {
		public ProxyAttribute() : base(Enums.DesignPattern.Proxy) {}
	}
}