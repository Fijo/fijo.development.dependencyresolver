using System;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class OnlyPublicForTestingAttribute : OnlyPublicForAttribute {
		public OnlyPublicForTestingAttribute() : base("Testing") {}

		public OnlyPublicForTestingAttribute(string message) : base(string.Concat("Testing, ", message)) {}
	}
}