using System;
using System.ComponentModel;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Description("Adds a link where you got information about that function or, that your used in the body of this function or something related.")]
	public class LinkAttribute : Attribute {
		public LinkAttribute(params string[] links) {}
	}
}