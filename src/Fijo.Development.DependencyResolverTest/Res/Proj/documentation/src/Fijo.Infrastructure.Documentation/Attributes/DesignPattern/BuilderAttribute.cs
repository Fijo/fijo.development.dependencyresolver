using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Delegate | AttributeTargets.Interface | AttributeTargets.Method | AttributeTargets.Module , Inherited = true, AllowMultiple = false)]
	public class BuilderAttribute : DesignPatternAttribute {
		public BuilderAttribute() : base(Enums.DesignPattern.Builder) {}
	}
}