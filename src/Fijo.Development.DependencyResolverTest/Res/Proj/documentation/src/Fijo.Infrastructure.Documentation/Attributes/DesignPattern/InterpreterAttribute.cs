using System;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern.Base;

namespace Fijo.Infrastructure.Documentation.Attributes.DesignPattern
{
	[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
	public class InterpreterAttribute : DesignPatternAttribute {
		public InterpreterAttribute() : base(Enums.DesignPattern.Interpreter) {}
	}
}