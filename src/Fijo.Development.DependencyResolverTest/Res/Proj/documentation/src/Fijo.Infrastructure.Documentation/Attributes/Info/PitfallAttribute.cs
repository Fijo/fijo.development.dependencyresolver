using System;
using System.ComponentModel;

namespace Fijo.Infrastructure.Documentation.Attributes.Info {
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	[Description("Says that something has a pitfall, that often make developers, don�t want that function to do when using it. Such things are no bugs.")]
	public class PitfallAttribute : Attribute {
		public PitfallAttribute(string painfall, string solution = "") {}
		public PitfallAttribute(string condition, string painfall, string solution = "") {}
	}
}