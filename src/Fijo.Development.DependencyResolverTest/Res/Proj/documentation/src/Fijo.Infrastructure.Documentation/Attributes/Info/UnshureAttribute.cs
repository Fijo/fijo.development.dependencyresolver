using System;

namespace Fijo.Infrastructure.Documentation.Attributes.Info
{
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
	public class UnshureAttribute : Attribute {
		public UnshureAttribute(string condition) {}
	}
}