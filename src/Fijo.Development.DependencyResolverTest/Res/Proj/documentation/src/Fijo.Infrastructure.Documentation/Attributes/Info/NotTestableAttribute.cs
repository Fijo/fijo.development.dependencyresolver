using System;

namespace Fijo.Infrastructure.Documentation.Attributes.Info
{
	[AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = false)]
	public class NotTestableAttribute : Attribute {
		public NotTestableAttribute() {}

		public NotTestableAttribute(string reason) {}
	}
}