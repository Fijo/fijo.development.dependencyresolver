using System.Collections;
using System.Collections.Generic;

namespace Fijo.Infrastructure.DesignPattern.Repository.Collection {
	public interface ICollectionRepository<out T> : ICollectionRepository, IRepository<IEnumerable<T>> {
		new IEnumerable<T> Get();
	}

	public interface ICollectionRepository : IRepository<IEnumerable> {}
}