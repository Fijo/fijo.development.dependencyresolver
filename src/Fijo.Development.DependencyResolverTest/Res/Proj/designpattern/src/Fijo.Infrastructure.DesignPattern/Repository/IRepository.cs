﻿using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace Fijo.Infrastructure.DesignPattern.Repository {
	[Repository]
	public interface IRepository {
		object Get();
	}

	public interface IRepository<out T> : IRepository {
		new T Get();
	}
}