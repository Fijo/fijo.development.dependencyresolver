using System.Collections.Generic;
using System.ComponentModel;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface
{
	public interface IAlgorithmFinder<TAlgorithm, TSection> : IAlgorithmFinder<TAlgorithm, TSection, TSection>
	{
	}

	public interface IAlgorithmFinder<TAlgorithm, TSection, TKey> {
		TKey KeySelector(TSection section);
		TSection SectionSelector(TKey key);
		TAlgorithm FallbackAlgorithm { set; }
		[NotNull, EditorBrowsable(EditorBrowsableState.Never)]
		IDictionary<TKey, TAlgorithm> Algorithms { get; }
		TAlgorithm GetByKey(IDictionary<TKey, TAlgorithm> algorithms, TKey key, bool useSection = false, TSection internalSection = default(TSection));
		TAlgorithm GetBySection(IDictionary<TKey, TAlgorithm> algorithms, TSection section);
	}
}