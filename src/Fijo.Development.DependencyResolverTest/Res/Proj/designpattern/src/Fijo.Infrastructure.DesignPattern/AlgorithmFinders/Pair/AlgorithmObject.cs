using System.Data;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface;
using Fijo.Infrastructure.Model.Pair.Base;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Pair
{
	public class AlgorithmObject<TAlgorithm, TSection> : PairBase<TSection, TAlgorithm>, IAlgorithmObject<TAlgorithm, TSection>, IAlgorithmObject
	{
		private readonly TSection _section;
		private readonly TAlgorithm _algorithm;

		public TSection Section{get { return _section; }}
		public TAlgorithm Algorithm{get { return _algorithm; }}
		object IAlgorithmObject.Algorithm{get { return _section; }}
		object IAlgorithmObject.Section{get { return _algorithm; }}

		public AlgorithmObject(TSection section, TAlgorithm algorithm)
		{
			_section = section;
			_algorithm = algorithm;
		}

		public override TSection First
		{
			get { return _section; }
			protected set { throw new ReadOnlyException(); }
		}

		public override TAlgorithm Second
		{
			get { return _algorithm; }
			protected set { throw new ReadOnlyException(); }
		}
	}
}