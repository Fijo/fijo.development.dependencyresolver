using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Exception;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface;
using Fijo.Infrastructure.DesignPattern.Extentions;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Base
{
	public abstract class AlgorithmFinderBase<TAlgorithm, TSection, TKey> : IAlgorithmFinder<TAlgorithm, TSection, TKey>
	{
		[NotNull] private readonly IDictionary<TKey, TAlgorithm> _algorithms;
		[CanBeNull] private TAlgorithm _fallbackAlgorithm;
		private bool _fallbackAlgorithmHasSet;
		
		#region Properties
		public virtual TAlgorithm FallbackAlgorithm { set {
			_fallbackAlgorithm = value;
			_fallbackAlgorithmHasSet = true;
		} }
		public IDictionary<TKey, TAlgorithm> Algorithms { get { return _algorithms; } }
		#endregion

		#region Constructor
		protected AlgorithmFinderBase()
		{
			_algorithms = new Dictionary<TKey, TAlgorithm>();
		}
		#endregion

		#region KeySelector
		public abstract TKey KeySelector(TSection section);

		public abstract TSection SectionSelector(TKey key);
		#endregion

		#region GetBy
		#region GetBySection
		public virtual TAlgorithm GetBySection(IDictionary<TKey, TAlgorithm> algorithms, TSection section)
		{
			return GetByKey(algorithms, KeySelector(section), true, section);
		}
		#endregion

		#region GetByKey
		public virtual TAlgorithm GetByKey(IDictionary<TKey, TAlgorithm> algorithms, TKey key, bool useSection = false, TSection internalSection = default(TSection))
		{
			var algorithm = default(TAlgorithm);
			var found = false;
			if(algorithms != null) found = algorithms.TryGetValue(key, out algorithm);
			if(!found) found = _algorithms.TryGetValue(key, out algorithm);
			if(!found) algorithm = GetFallbackAlgorithm(algorithms, key, useSection, internalSection);
			return algorithm;
		}

		protected virtual TAlgorithm GetFallbackAlgorithm(IDictionary<TKey, TAlgorithm> algorithms, TKey key, bool useSection, TSection internalSection) {
			return UseFallback()
			       	? _fallbackAlgorithm
			       	: ThrowNotFound(algorithms, key, useSection, internalSection);
		}

		private bool UseFallback() {
			return !_fallbackAlgorithm.IsUndefined() && _fallbackAlgorithmHasSet;
		}

		private TAlgorithm ThrowNotFound(IDictionary<TKey, TAlgorithm> algorithms, TKey key, bool useSection, TSection internalSection) {
			throw GetNotFoundException(algorithms, key, useSection, internalSection);
		}
		#endregion
		#endregion

		private AlgorithmNotFoundException GetNotFoundException(IEnumerable<KeyValuePair<TKey, TAlgorithm>> algorithms, TKey key, bool useSection = false, TSection internalSection = default(TSection)) {
			#region PreCondition
			Debug.Assert(!key.IsUndefined(), "key.IsUndefined()");
			Debug.Assert(!useSection || !internalSection.IsUndefined(), "!useSection || !internalSection.IsUndefined()");
			#endregion
			var type = GetType();
			var keyString = key.ToString();
			var usedAlgorithms = algorithms != null ? _algorithms.Concat(algorithms) : _algorithms;
			return useSection
			       	? new AlgorithmNotFoundException<TKey, TAlgorithm>(type, usedAlgorithms, keyString, internalSection.ToString())
			       	: new AlgorithmNotFoundException<TKey, TAlgorithm>(type, usedAlgorithms, keyString);
		}
	}
}