namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface
{
	public interface IAlgorithmObject<out TAlgorithm, out TSection>
	{
		TSection Section { get; }
		TAlgorithm Algorithm { get; }
	}

	public interface IAlgorithmObject
	{
		object Section { get; }
		object Algorithm { get; }
	}
}