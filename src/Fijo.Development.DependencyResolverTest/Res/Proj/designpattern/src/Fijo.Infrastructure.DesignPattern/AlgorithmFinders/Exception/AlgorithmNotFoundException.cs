using System;
using System.Collections.Generic;
using System.Linq;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Exception
{
	public class AlgorithmNotFoundException : System.Exception
	{
		public AlgorithmNotFoundException(string message) : base(message)
		{
			
		}
	}
	
	public class AlgorithmNotFoundException<TKey, TAlgorithm> : AlgorithmNotFoundException
	{
		public AlgorithmNotFoundException(Type algorithmFinderImpl, IEnumerable<KeyValuePair<TKey, TAlgorithm>> algorithms, string key) : base(string.Format("The AlgorithmFinder �{0}� could not find an algorithm for �{3}� by {2}.\nUsed algorithms\n{1}", algorithmFinderImpl.Name, GetAlgorithmsString(algorithms), "Key", key))
		{

		}
		
		public AlgorithmNotFoundException(Type algorithmFinderImpl, IEnumerable<KeyValuePair<TKey, TAlgorithm>> algorithms, string key, string section) : base(string.Format("The AlgorithmFinder �{0}� could not find an algorithm for �{3}� converted from Section �{4}� by {2}.\nUsed algorithms\n{1}", algorithmFinderImpl.Name, GetAlgorithmsString(algorithms), "Section", key, section))
		{

		}

		private static string GetAlgorithmsString(IEnumerable<KeyValuePair<TKey, TAlgorithm>> algorithms)
		{
			return string.Join(",\n", algorithms.Select(x => string.Format("{0}: {1}", x.Key, x.Value.GetType().Name)));
		}
	}
}