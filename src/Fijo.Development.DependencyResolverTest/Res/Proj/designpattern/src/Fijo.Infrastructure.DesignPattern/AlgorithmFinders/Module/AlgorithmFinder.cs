using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Base;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface;

namespace Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Module
{
	public abstract class AlgorithmFinder<TAlgorithm, TSection> : AlgorithmFinderBase<TAlgorithm, TSection, TSection>, IAlgorithmFinder<TAlgorithm, TSection>
	{
		public override TSection KeySelector(TSection section)
		{
			return section;
		}

		public override TSection SectionSelector(TSection section)
		{
			return section;
		}
	}

	public abstract class AlgorithmFinder<TAlgorithm, TSection, TKey> : AlgorithmFinderBase<TAlgorithm, TSection, TKey>
	{
	}
}