﻿namespace Fijo.Infrastructure.DesignPattern.Mapping {
	public interface IMapping<in TSource, out TResult> {
		TResult Map(TSource source);
	}
}