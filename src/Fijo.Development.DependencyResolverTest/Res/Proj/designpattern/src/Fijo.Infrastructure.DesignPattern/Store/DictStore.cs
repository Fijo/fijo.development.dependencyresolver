using System.Collections.Generic;

namespace Fijo.Infrastructure.DesignPattern.Store {
	public abstract class DictStore<TEntity, TKey> : DefaultStoreBase<TEntity, TKey, IDictionary<TKey, TEntity>> {
		#region Overrides of DefaultStoreBase<TEntity,TKey,IDictionary<TKey,TEntity>>
		protected override IDictionary<TKey, TEntity> CreateStore() {
			return new Dictionary<TKey, TEntity>();
		}

		protected override bool TryGet(IDictionary<TKey, TEntity> content, TKey key, out TEntity result) {
			return content.TryGetValue(key, out result);
		}

		protected override TEntity Set(IDictionary<TKey, TEntity> content, TKey key, TEntity value) {
			if (!content.ContainsKey(key)) return content[key] = Overwrite(key, value, content[key]);
			content.Add(key, value);
			return value;
		}

		protected virtual TEntity Overwrite(TKey key, TEntity value, TEntity oldValue) {
			return value;
		}
		#endregion
	}

	public abstract class DictStore<TEntity> : DictStore<TEntity, object> {}
}