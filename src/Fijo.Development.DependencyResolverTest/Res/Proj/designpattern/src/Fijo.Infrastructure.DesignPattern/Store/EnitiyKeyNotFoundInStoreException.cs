using System.Collections.Generic;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store {
	public class EnitiyKeyNotFoundInStoreException : KeyNotFoundException {
		public readonly IStore Store;
		public readonly object Key;

		public EnitiyKeyNotFoundInStoreException([NotNull] IStore store, [NotNull] object key) : base(string.Format("Key �{0}� in store {1}", key, store.GetType().Name)) {
			Store = store;
			Key = key;
		}
	}
}