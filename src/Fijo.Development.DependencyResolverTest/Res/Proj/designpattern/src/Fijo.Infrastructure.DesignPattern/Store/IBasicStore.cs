using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store {
	[Store]
	public interface IBasicStore {
		object Get();
		void Store([NotNull] object entity);
	}

	public interface IBasicStore<TEntity> : IBasicStore {
		new TEntity Get();
		void Store([NotNull] TEntity entity);
	}
}