using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store {
	public abstract class DefaultStoreBase<TEntity, TKey, TStoreType> : StoreBase<TEntity, TKey> {
		[NotNull] private readonly TStoreType _content;

		protected DefaultStoreBase() {
			// ReSharper disable DoNotCallOverridableMethodsInConstructor
			_content = CreateStore();
			// ReSharper restore DoNotCallOverridableMethodsInConstructor
		}

		#region Create
		[NotNull]
		protected abstract TStoreType CreateStore();
		#endregion

		#region TryGet, Set
		protected abstract bool TryGet([NotNull] TStoreType content, [NotNull] TKey key, out TEntity result);
		protected abstract TEntity Set([NotNull] TStoreType content, [NotNull] TKey key, [NotNull] TEntity value);
		#endregion

		#region Get
		protected virtual TEntity Get([NotNull] TStoreType content, [NotNull] TKey key) {
			TEntity result;
			if (!TryGet(content, key, out result)) throw new EnitiyKeyNotFoundInStoreException(this, key);
			return result;
		}
		#endregion

		#region Get, Store
		public override TEntity Get(TKey key) {
			return Get(_content, key);
		}

		public override void Store(TEntity entity) {
			Set(_content, GetKey(entity), entity);
		}
		#endregion

		public abstract TKey GetKey([NotNull] TEntity entity);
	}
}