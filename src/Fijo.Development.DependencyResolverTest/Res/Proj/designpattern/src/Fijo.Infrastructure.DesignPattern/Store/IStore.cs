﻿using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Store {
	[Store]
	public interface IStore {
		object Get([NotNull] object key);
		void Store([NotNull] object entity);
	}

	public interface IStore<TEntity> : IStore {
		new TEntity Get([NotNull] object key);
		void Store([NotNull] TEntity entity);
	}

	public interface IStore<TEntity, in TKey> : IStore<TEntity> {
		TEntity Get([NotNull] TKey key);
	}
}