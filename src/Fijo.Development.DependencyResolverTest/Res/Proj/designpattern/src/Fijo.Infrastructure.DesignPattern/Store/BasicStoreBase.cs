namespace Fijo.Infrastructure.DesignPattern.Store {
	public abstract class BasicStoreBase<TEntity> : IBasicStore<TEntity> {
		#region Implementation of IBasicStore
		public abstract void Store(TEntity entity);
		public abstract TEntity Get();
		
		object IBasicStore.Get() {
			return Get();
		}

		public void Store(object entity) {
			Store((TEntity) entity);
		}
		#endregion
	}
}