using System.Diagnostics;

namespace Fijo.Infrastructure.DesignPattern.Store {
	public abstract class StoreBase<TEntity> : IStore<TEntity> {
		#region Implementation of IStore
		object IStore.Get(object key) {
			return Get(key);
		}

		public void Store(object entity) {
			#region PreCondition
			Debug.Assert(entity is TEntity, "entity is TEntity");
			#endregion
			Store((TEntity) entity);
		}

		public abstract void Store(TEntity entity);
		public abstract TEntity Get(object key);
		#endregion
	}

	public abstract class StoreBase<TEntity, TKey> : StoreBase<TEntity>, IStore<TEntity, TKey> {
		#region Implementation of IStore<TEntity>
		public override TEntity Get(object key) {
			#region PreCondition
			Debug.Assert(key is TKey, "key is TKey");
			#endregion
			return Get((TKey) key);
		}
		#endregion

		#region Implementation of IStore<TEntity,in TKey>
		public abstract TEntity Get(TKey key);
		#endregion
	}
}