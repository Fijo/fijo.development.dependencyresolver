using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Extentions {
	internal static class WhenNotNullExtention {
		[CanBeNull, Pure, DebuggerStepThrough, Description("execute a func with this as first param only if this is not null and return the result. Otherwise return null.")]
		internal static TResult WhenNotNull<TSource, TResult>(this TSource me, [NotNull] System.Func<TSource, TResult> toResultFunc) where TResult : class {
			return !Equals(me, null) ? toResultFunc(me) : null;
		}
	}
}