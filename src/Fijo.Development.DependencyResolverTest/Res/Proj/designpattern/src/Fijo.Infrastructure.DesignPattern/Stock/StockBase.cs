using System.Collections;
using System.Collections.Generic;

namespace Fijo.Infrastructure.DesignPattern.Stock {
	public abstract class StockBase<T> : IStock<T> {
		#region Implementation of IStock
		public void Remove(object item) {
			Remove((T) item);
		}

		public void Add(object item) {
			Add((T) item);
		}

		IEnumerable IStock.GetAll() {
			return GetAll();
		}

		public abstract IEnumerable<T> GetAll();
		public abstract void Remove(T item);
		public abstract void Add(T item);
		#endregion
	}
}