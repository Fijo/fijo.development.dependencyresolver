using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Stock {
	public class Stock<T> : StockBase<T> {
		[NotNull] protected readonly IList<T> Content = new List<T>();
		#region Overrides of StockBase<T>
		public override void Add(T item) {
			Content.Add(item);
		}

		public override IEnumerable<T> GetAll() {
			return Content;
		}

		public override void Remove(T item) {
			Content.Remove(item);
		}
		#endregion
	}

	public class Stock : IStock {
		[NotNull] protected readonly IList Content = new List<object>();
		#region Implementation of IStock
		public void Remove(object item) {
			Content.Remove(item);
		}

		public void Add(object item) {
			Content.Add(item);
		}

		public IEnumerable GetAll() {
			return Content;
		}
		#endregion
	}
}