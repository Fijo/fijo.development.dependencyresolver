using System.Collections;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.Infrastructure.DesignPattern.Stock {
	public interface IStock<T> : IStock {
		void Remove([NotNull] T item);
		void Add([NotNull] T item);
		[NotNull]
		new IEnumerable<T> GetAll();
	}

	[Stock]
	public interface IStock {
		void Remove([NotNull] object item);
		void Add([NotNull] object item);
		[NotNull]
		IEnumerable GetAll();
	}
}