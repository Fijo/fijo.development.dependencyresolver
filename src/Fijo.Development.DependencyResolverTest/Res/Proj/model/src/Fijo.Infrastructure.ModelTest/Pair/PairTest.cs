using NUnit.Framework;
using Fijo.Infrastructure.Model.Pair;

namespace Fijo.Infrastructure.ModelTest.Pair
{
    [TestFixture]
    public class PairTest
    {
        [Test]
        public void GetHashCodeTest() {
        	var pair = new Pair<object, object>(null, "test");
// ReSharper disable ReturnValueOfPureMethodIsNotUsed
			Assert.DoesNotThrow(() => pair.GetHashCode());
// ReSharper restore ReturnValueOfPureMethodIsNotUsed
        }
    }
}