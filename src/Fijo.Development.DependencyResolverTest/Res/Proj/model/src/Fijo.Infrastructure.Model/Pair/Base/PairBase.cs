using System.Diagnostics.Contracts;
using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace Fijo.Infrastructure.Model.Pair.Base {
	public abstract class PairBase<TObject> : PairBase<TObject, TObject> {}

	public abstract class PairBase<TFirst, TSecond> : PairBase, IPair<TFirst, TSecond> {
		#region Implementation of IPair<TFirst,TSecond>
		public abstract TFirst First { get; protected set; }
		public abstract TSecond Second { get; protected set; }
		#endregion

		#region Implementation of IPair
		public override object GenericFirst {
			get { return First; }
		}

		public override object GenericSecond {
			get { return Second; }
		}
		#endregion
	}

	public abstract class PairBase : IPair {
		#region Implementation of IPair
		public abstract object GenericFirst { get; }
		public abstract object GenericSecond { get; }
		#endregion
		
		#region Equals
		protected bool Equals(IPair other) {
			return Equals(GenericFirst, other.GenericFirst) && Equals(GenericSecond, other.GenericSecond);
		}

		public override int GetHashCode() {
			unchecked {
				return (NullableGetHashCode(GenericFirst) * 397) ^ NullableGetHashCode(GenericSecond);
			}
		}

		[Pure]
		private int NullableGetHashCode<T>(T me) {
			return !typeof(T).IsValueType && Equals(null, me) ? 0 : me.GetHashCode();
		}
		#endregion
	}
}