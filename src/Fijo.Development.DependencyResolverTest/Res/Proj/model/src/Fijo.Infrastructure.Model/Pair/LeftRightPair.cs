using System.Data;
using Fijo.Infrastructure.Model.Pair.Base;
using Fijo.Infrastructure.Model.Pair.Interface;
using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace Fijo.Infrastructure.Model.Pair {
#pragma warning disable 659
	public class LeftRightPair<TObject> : PairBase<TObject>, ILeftRightPair<TObject> {
		public TObject Left { get; private set; }
		public TObject Right { get; private set; }

		public LeftRightPair(TObject left, TObject right) {
			Left = left;
			Right = right;
		}

		#region Implementation of IPair<TObject,TObject>
		public override TObject First {
			get { return Left; }
			protected set { throw new ReadOnlyException(); }
		}

		public override TObject Second {
			get { return Right; }
			protected set { throw new ReadOnlyException(); }
		}
		#endregion

		#region Equals
		public override bool Equals(object obj) {
			return obj is LeftRightPair<TObject> && Equals((IPair) obj);
		}
		#endregion
	}
#pragma warning restore 659
}