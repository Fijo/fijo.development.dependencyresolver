using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace Fijo.Infrastructure.Model.Pair.Interface {
	public interface IReplacePair<out TEntry> : IReplacePair<TEntry, TEntry> {}

	public interface IReplacePair<out TNeedle, out TReplacement> : IPair<TNeedle, TReplacement> {
		TNeedle Needle { get; }
		TReplacement Replacement { get; }
	}
}