using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace Fijo.Infrastructure.Model.Pair.Interface {
	public interface ILeftRightPair<out TObject> : IPair<TObject, TObject> {
		TObject Left { get; }
		TObject Right { get; }
	}
}