namespace Fijo.Infrastructure.Model.Pair.Interface.Base {
	public interface IPair<out T1, out T2> : IPair {
		T1 First { get; }
		T2 Second { get; }
	}

	public interface IPair {
		object GenericFirst { get; }
		object GenericSecond { get; }
	}
}