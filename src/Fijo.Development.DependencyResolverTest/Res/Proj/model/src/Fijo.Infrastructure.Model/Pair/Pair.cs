using Fijo.Infrastructure.Model.Pair.Base;
using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace Fijo.Infrastructure.Model.Pair {
#pragma warning disable 659
	public class Pair<T1, T2> : PairBase<T1, T2> {
		public override T1 First { get; protected set; }
		public override T2 Second { get; protected set; }
		
		public Pair(T1 first, T2 second) {
			Second = second;
			First = first;
		}

		#region Equals
		public override bool Equals(object obj) {
			return obj is Pair<T1, T2> && Equals((IPair) obj);
		}
		#endregion
	}
#pragma warning restore 659
}