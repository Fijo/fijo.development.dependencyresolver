using System.Data;
using Fijo.Infrastructure.Model.Pair.Base;
using Fijo.Infrastructure.Model.Pair.Interface;
using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace Fijo.Infrastructure.Model.Pair {
#pragma warning disable 659
	public class ReplacePair<TEntry> : ReplacePair<TEntry, TEntry>, IReplacePair<TEntry> {
		public ReplacePair(TEntry needle, TEntry replacement) : base(needle, replacement) {}

		#region Equals
		public override bool Equals(object obj) {
			return obj is ReplacePair<TEntry> && Equals((IPair) obj);
		}
		#endregion
	}
#pragma warning restore 659

#pragma warning disable 659
	public class ReplacePair<TNeedle, TReplacement> : PairBase<TNeedle, TReplacement>, IReplacePair<TNeedle, TReplacement> {
		public TNeedle Needle { get; private set; }
		public TReplacement Replacement { get; private set; }
		
		public ReplacePair(TNeedle needle, TReplacement replacement) {
			Needle = needle;
			Replacement = replacement;
		}

		#region Implementation of IPair<TNeedle,TReplacement>
		public override TNeedle First {
			get { return Needle; }
			protected set { throw new ReadOnlyException(); }
		}

		public override TReplacement Second {
			get { return Replacement; }
			protected set { throw new ReadOnlyException(); }
		}
		#endregion
		
		#region Equals
		public override bool Equals(object obj) {
			return obj is ReplacePair<TNeedle, TReplacement> && Equals((IPair) obj);
		}
		#endregion
	}
#pragma warning restore 659
}