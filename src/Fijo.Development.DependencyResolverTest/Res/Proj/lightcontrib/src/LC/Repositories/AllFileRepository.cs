﻿using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace FijoCore.Infrastructure.LightContrib.Repositories {
	// ToDo use a repository for this
	[Repository]
	public class AllFileRepository {
		public IEnumerable<string> Get(string folder) {
			return System.IO.Directory.GetDirectories(folder).SelectMany(Get).Concat(System.IO.Directory.GetFiles(folder));
		}
	}
}