﻿using System;
using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface;
using Fijo.Infrastructure.DesignPattern.Mapping;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.DefaultFunctions;
using FijoCore.Infrastructure.LightContrib.Default.Service.DefaultFunctions.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.IListFunc;
using FijoCore.Infrastructure.LightContrib.Default.Service.IListFunc.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.ListFunc;
using FijoCore.Infrastructure.LightContrib.Default.Service.ListFunc.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Extentions.AlgorithmFinder;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Reflection.Resolve;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.List;
using FijoCore.Infrastructure.LightContrib.Factories;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Mappings;
using FijoCore.Infrastructure.LightContrib.Module.Assembly;
using FijoCore.Infrastructure.LightContrib.Module.AssemblyConfiguration;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using FijoCore.Infrastructure.LightContrib.Module.Converter.Module;
using FijoCore.Infrastructure.LightContrib.Module.Converter.TypeCode;
using FijoCore.Infrastructure.LightContrib.Module.Encoding;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Custom;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Custom.Interface;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Interface;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Recursive;
using FijoCore.Infrastructure.LightContrib.Module.FileTools;
using FijoCore.Infrastructure.LightContrib.Module.Path;
using FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Custom;
using FijoCore.Infrastructure.LightContrib.Module.Serialization;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using FijoCore.Infrastructure.LightContrib.Repositories;
using FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals;
using FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals.Interface;
using FijoCore.Infrastructure.LightContrib.Service.SequenceEqualService;
using FijoCore.Infrastructure.LightContrib.Service.SequenceEqualService.Interface;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface;
using Newtonsoft.Json;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;
using RecursiveSequenceEqualExtention = FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator.RecursiveSequenceEqualExtention;
using SequenceEqualExtention = FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator.SequenceEqualExtention;
using GKernel = FijoCore.Infrastructure.DependencyInjection.InitKernel.Kernel;

namespace FijoCore.Infrastructure.LightContrib.Helper.InitKernel
{
    public class LightContribInjectionModule : IExtendedNinjectModule {
        public IKernel Kernel { get; private set; }
    	public IDictionary<IExtendedInitHandler, object> ExtendedInit { get; set; }

    	public void AddModule(IKernel kernel) {
    		kernel.Load(new NumberInjectionModule());
    	}

    	public void OnLoad(IKernel kernel)
        {
            Kernel = kernel;

        	kernel.Bind<IOut>().To<Out>().InSingletonScope();
        	kernel.Bind<IDefaultFunctions>().To<DefaultFunctions>().InSingletonScope();
			kernel.Bind<IExecutedEnumerableFactory>().To<ExecutedEnumerableFactory>().InSingletonScope();
			kernel.Bind<IOrderlessEqualsService>().To<OrderlessEqualsService>().InSingletonScope();
        	kernel.Bind<AssembliesProvider>().ToSelf().InSingletonScope();
        	kernel.Bind<AssemblyDynamicCustomConfigurationResolver>().ToSelf().InSingletonScope();
        	kernel.Bind<AssemblyEnabledDefenitionResolver>().ToSelf().InSingletonScope();
        	kernel.Bind<RelativeOperationComparingEnumerableRightService>().ToSelf().InSingletonScope();
        	kernel.Bind<RuntimeConverter>().ToSelf().InSingletonScope();
        	kernel.Bind<VersionToULongConverter>().ToSelf().InSingletonScope();
        	kernel.Bind<NumericConverterService>().ToSelf().InSingletonScope();
    		kernel.Bind<IPairFactory>().To<PairFactory>().InSingletonScope();
    		kernel.Bind<FileFilterService>().ToSelf().InSingletonScope();
    		kernel.Bind<AllFileRepository>().ToSelf().InSingletonScope();
    		kernel.Bind<IStreamService>().To<StreamService>().InSingletonScope();
    		kernel.Bind<IRepository<IDictionary<SerializationFormat, ISerialization>>>().To<SerializationRepository>().InSingletonScope();
    		kernel.Bind<IMapping<SerializationFormatting, Formatting>>().To<FormattingMapping>().InSingletonScope();
    		kernel.Bind<ISerializationService>().To<SerializationService>().InSingletonScope();
    		kernel.Bind<ISerializationProvider>().To<SerializationProvider>().InSingletonScope();
    		kernel.Bind<ISerializationFacade>().To<SerializationFacade>().InSingletonScope();
    		kernel.Bind<IConfigurationService>().To<ConfigurationService>().InSingletonScope();
    		kernel.Bind<IRepository<IDictionary<string, object>>>().To<ConfigurationRepository>().InSingletonScope();
    		kernel.Bind<IEncodingProvider>().To<EncodingProvider>().InSingletonScope();
    		kernel.Bind<IPathService>().To<PathService>().InSingletonScope();
			kernel.Bind<IUniqueAddService>().To<UniqueAddService>().InSingletonScope();
			kernel.Bind<IUniqueAddRangeService>().To<UniqueAddRangeService>().InSingletonScope();
			kernel.Bind<IDefaultListFunc>().To<DefaultListFunc>().InSingletonScope();
			kernel.Bind<IDefaultIListFunc>().To<DefaultIListFunc>().InSingletonScope();
			kernel.Bind<ISequenceEqualService>().To<SequenceEqualService>().InSingletonScope();
			kernel.Bind<EqualityComparerFactory>().ToSelf().InSingletonScope();
			kernel.Bind<ICustomRecursiveEqualityComparerFactory>().To<CustomRecursiveEqualityComparerFactory>().InSingletonScope();
			kernel.Bind<ICustomInternalRecursiveEqualityComparerFactory>().To<CustomInternalRecursiveEqualityComparerFactory>().InSingletonScope();
			kernel.Bind<IRecursiveEqualityComparerService>().To<RecursiveEqualityComparerService>().InSingletonScope();
			kernel.Bind<IEqualityComparerFactory>().To<EqualityComparerFactory>().InSingletonScope();
			kernel.Bind<FuncEqualityComparerFactory>().ToSelf().InSingletonScope();
    		kernel.Bind<ITypeChangeFactory>().To<TypeChangeFactory>().InSingletonScope();
    		kernel.Bind<IConverter<string, DateTime>>().To<DateTimeConverter>().InSingletonScope();
    		kernel.Bind<IConverter<long, short>>().To<Int16Converter>().InSingletonScope();
    		kernel.Bind<IConverter<long, ushort>>().To<UInt16Converter>().InSingletonScope();
    		kernel.Bind<IConverter<long, int>>().To<Int32Converter>().InSingletonScope();
    		kernel.Bind<IConverter<long, uint>>().To<UInt32Converter>().InSingletonScope();
    		kernel.Bind<IConverter<long, ulong>>().To<UInt64Converter>().InSingletonScope();
    		kernel.Bind<IConverter>().To<DefaultConverter>().InSingletonScope();
    		kernel.Bind<IAlgorithmFinder<IConverter, TypeChangeDto>>().To<ConfigurationConverterFinder>().InSingletonScope().OnActivation(
    			x => {
    				var typeChangeFactory = GKernel.Resolve<ITypeChangeFactory>();
    				x.Add(new Dictionary<TypeChangeDto, IConverter>
    				{
    					{typeChangeFactory.Create(typeof (string), typeof (DateTime)), GKernel.Resolve<IConverter<string, DateTime>>()},
    					{typeChangeFactory.Create(typeof (long), typeof (short)), GKernel.Resolve<IConverter<long, short>>()},
    					{typeChangeFactory.Create(typeof (long), typeof (ushort)), GKernel.Resolve<IConverter<long, ushort>>()},
    					{typeChangeFactory.Create(typeof (long), typeof (int)), GKernel.Resolve<IConverter<long, int>>()},
    					{typeChangeFactory.Create(typeof (long), typeof (uint)), GKernel.Resolve<IConverter<long, uint>>()},
    					{typeChangeFactory.Create(typeof (long), typeof (ulong)), GKernel.Resolve<IConverter<long, ulong>>()},
    				});
    				x.FallbackAlgorithm = GKernel.Resolve<IConverter>();
    			});
    	}

    	public void Init(IKernel kernel) {
    		TryGetEnumeratorExtention.Init();
			ExecuteExtention.Init();
			UniqueAddExtention.Init();
			SequenceEqualExtention.Init();
			RecursiveSequenceEqualExtention.Init();
			OrderlessEqualsExtention.Init();
    	}

    	public void RegisterHandlers(IKernel kernel) {}

    	public void Validate() {}
		
    	public void Loaded(IKernel kernel) {}

        public void OnUnload(IKernel kernel) {}

    	public string Name
        {
            get { return "LightContribInjectionModule"; }
        }
    }
}