﻿using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Number;
using FijoCore.Infrastructure.LightContrib.Module.Number.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace FijoCore.Infrastructure.LightContrib.Helper.InitKernel
{
    public class NumberInjectionModule : IExtendedNinjectModule {
        public IKernel Kernel { get; private set; }
    	public IDictionary<IExtendedInitHandler, object> ExtendedInit { get; set; }

    	public void AddModule(IKernel kernel) {}

    	public void OnLoad(IKernel kernel)
        {
            Kernel = kernel;

    		kernel.Bind<INumberServiceProvider>().To<NumberServiceProvider>().InSingletonScope();
    		kernel.Bind<INumberConcat>().To<NumberConcat>().InSingletonScope();
    		kernel.Bind<INumberServiceHelper>().To<NumberServiceHelper>().InSingletonScope();
			kernel.Bind<INumberService<byte>>().To<ByteNumberService>().InSingletonScope();
			kernel.Bind<INumberService<char>>().To<CharNumberService>().InSingletonScope();
    		kernel.Bind<INumberService<short>>().To<ShortNumberService>().InSingletonScope();
    		kernel.Bind<INumberService<ushort>>().To<UShortNumberService>().InSingletonScope();
    		kernel.Bind<INumberService<int>>().To<IntNumberService>().InSingletonScope();
    		kernel.Bind<INumberService<uint>>().To<UIntNumberService>().InSingletonScope();
    		kernel.Bind<INumberService<long>>().To<LongNumberService>().InSingletonScope();
    		kernel.Bind<INumberService<ulong>>().To<ULongNumberService>().InSingletonScope();
			kernel.Bind<INumberService<decimal>>().To<DecimalNumberService>().InSingletonScope();
			kernel.Bind<INumberService<float>>().To<FloatNumberService>().InSingletonScope();
			kernel.Bind<INumberService<double>>().To<DoubleNumberService>().InSingletonScope();
    	}

    	public void Init(IKernel kernel) {
    	}

    	public void RegisterHandlers(IKernel kernel) {}

    	public void Validate() {}
		
    	public void Loaded(IKernel kernel) {}

        public void OnUnload(IKernel kernel) {}

    	public string Name
        {
            get { return "NumberInjectionModule"; }
        }
    }
}