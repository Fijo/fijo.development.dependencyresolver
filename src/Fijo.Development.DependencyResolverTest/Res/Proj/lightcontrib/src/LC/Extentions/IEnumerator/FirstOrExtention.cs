using System;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class FirstOrExtention {
		[Pure, DebuggerStepThrough]
		public static TSource FirstOr<TSource>([NotNull] this IEnumerator<TSource> me, [NotNull] Func<TSource> defaultValueProvider) {
			return me.MoveNext()
			       	? me.Current
			       	: defaultValueProvider();
		}
	}
}