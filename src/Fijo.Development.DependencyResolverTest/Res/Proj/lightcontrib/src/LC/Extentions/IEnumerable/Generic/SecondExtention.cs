using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class SecondExtention {
		[DebuggerStepThrough]
		public static TSource Second<TSource>([NotNull] this IEnumerable<TSource> me) {
			return me.IndexTh(2);
		}
	}
}