using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class OptActionExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TResult> OptAction<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<IEnumerable<TSource>, IEnumerable<TResult>> notOpt, [NotNull] Func<ParallelQuery<TSource>, IEnumerable<TResult>> opt, bool executeParallel) {
			return executeParallel ? opt(me.AsParallel()) : notOpt(me);
		}
	}
}