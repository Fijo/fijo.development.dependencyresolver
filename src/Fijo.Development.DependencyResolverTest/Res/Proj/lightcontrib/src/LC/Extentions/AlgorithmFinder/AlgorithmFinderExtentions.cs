using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.AlgorithmFinder {
	public static class AlgorithmFinderExtentions {
		#region Add
		#region KeyValuePair
		public static void Add<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, IEnumerable<KeyValuePair<TSection, TAlgorithm>> algorithms)
		{
			#region PreCondition
			CheckAlgorithms(algorithms);
			#endregion
			GetAlgorithms(me).AddRange(algorithms, me.KeySelector);
		}

		public static void AddWithKey<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, IEnumerable<KeyValuePair<TKey, TAlgorithm>> algorithms)
		{
			#region PreCondition
			CheckAlgorithms(algorithms);
			#endregion
			GetAlgorithms(me).AddRange(algorithms);
		}

		public static void Add<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, params KeyValuePair<TSection, TAlgorithm>[] algorithms)
		{
			#region PreCondition
			Debug.Assert(algorithms != null, "algorithms != null");
			#endregion
			Add(me, (IEnumerable<KeyValuePair<TSection, TAlgorithm>>)algorithms);
		}

		public static void AddWithKey<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, params KeyValuePair<TKey, TAlgorithm>[] algorithms)
		{
			#region PreCondition
			Debug.Assert(algorithms != null, "algorithms != null");
			#endregion
			AddWithKey(me, (IEnumerable<KeyValuePair<TKey, TAlgorithm>>)algorithms);
		}
		#endregion

		#region AlgorithmObject
		public static void Add<TAlgorithm, TSection, TKey>(this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, IEnumerable<IAlgorithmObject<TAlgorithm, TSection>> algorithms)
		{
			#region PreCondition
			CheckAlgorithms(algorithms);
			#endregion
			GetAlgorithms(me).AddRange(Convert(algorithms), me.KeySelector);
		}

		public static void AddWithKey<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, [NotNull] IEnumerable<IAlgorithmObject<TAlgorithm, TKey>> algorithms)
		{
			#region PreCondition
			CheckAlgorithms(algorithms);
			#endregion
			GetAlgorithms(me).AddRange(Convert(algorithms));
		}

		public static void Add<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, [NotNull] params IAlgorithmObject<TAlgorithm, TSection>[] algorithms)
		{
			#region PreCondition
			Debug.Assert(algorithms != null, "algorithms != null");
			#endregion
			Add(me, (IEnumerable<IAlgorithmObject<TAlgorithm, TSection>>)algorithms);
		}
		
		public static void AddWithKey<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, params IAlgorithmObject<TAlgorithm, TKey>[] algorithms) {
			#region PreCondition
			Debug.Assert(algorithms != null, "algorithms != null");
			#endregion
			AddWithKey(me, (IEnumerable<IAlgorithmObject<TAlgorithm, TKey>>)algorithms);
		}
		#endregion
		#endregion
		
		#region Get
		#region GetBy
		#region GetByKey
		private static TAlgorithm GetByKey<TInner, TAlgorithm, TSection, TKey>([NotNull] IAlgorithmFinder<TAlgorithm, TSection, TKey> me, TInner algorithms, [NotNull] Func<TInner, IDictionary<TKey, TAlgorithm>> innerConverter, TKey key)
		{
			#region PreCondition
			CheckKey(key);
			#endregion
			return me.GetByKey(algorithms.WhenNotNull(innerConverter), key);
		}
		#endregion

		#region GetBySection
		private static TAlgorithm GetBySection<TInner, TAlgorithm, TSection, TKey>([NotNull] IAlgorithmFinder<TAlgorithm, TSection, TKey> me, TInner algorithms, [NotNull] Func<TInner, IDictionary<TKey, TAlgorithm>> innerConverter, TSection section)
		{
			#region PreCondition
			CheckSection(section);
			#endregion
			return me.GetBySection(algorithms.WhenNotNull(innerConverter), section);
		}
		#endregion
		#endregion

		#region ByKey
		public static TAlgorithm GetByKey<TAlgorithm, TSection, TKey>(this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, TKey key)
		{
			#region PreCondition
			CheckKey(key);
			#endregion
			return me.GetByKey(default(IDictionary<TKey, TAlgorithm>), key);
		}

		public static TAlgorithm GetByKeyWithKey<TAlgorithm, TSection, TKey>(this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, IEnumerable<IPair<TKey, TAlgorithm>> algorithms, TKey key)
		{
			return GetByKey(me, algorithms, x => x.ToDictionary(), key);
		}
		
		public static TAlgorithm GetByKey<TAlgorithm, TSection, TKey>(this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, IEnumerable<IPair<TSection, TAlgorithm>> algorithms, TKey key)
		{
			return GetByKey(me, algorithms, x => x.ToDictionary(me.KeySelector), key);
		}

		public static TAlgorithm GetByKeyWithKey<TAlgorithm, TSection, TKey>(this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, IEnumerable<KeyValuePair<TKey, TAlgorithm>> algorithms, TKey key)
		{
			return GetByKey(me, algorithms, x => x.ToDictionary(), key);
		}

		public static TAlgorithm GetByKey<TAlgorithm, TSection, TKey>(this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, IEnumerable<KeyValuePair<TSection, TAlgorithm>> algorithms, TKey key)
		{
			return GetByKey(me, algorithms, x => x.ToDictionary(me.KeySelector), key);
		}
		#endregion

		#region BySection
		public static TAlgorithm Get<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, TSection section)
		{
			#region PreCondition
			CheckSection(section);
			#endregion
			return me.GetBySection(default(IDictionary<TKey, TAlgorithm>), section);
		}

		public static TAlgorithm GetWithKey<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, [NotNull] IEnumerable<IPair<TKey, TAlgorithm>> algorithms, TSection section) {
			var enumerable = algorithms.ToArray();
			return GetBySection(me, enumerable, x => enumerable.ToDictionary(), section);
		}

		public static TAlgorithm Get<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, [NotNull] IEnumerable<IPair<TSection, TAlgorithm>> algorithms, TSection section)
		{
			return GetBySection(me, algorithms, x => x.ToDictionary(me.KeySelector), section);
		}

		public static TAlgorithm GetWithKey<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, [NotNull] IEnumerable<KeyValuePair<TKey, TAlgorithm>> algorithms, TSection section)
		{
			return GetBySection(me, algorithms, x => x.ToDictionary(), section);
		}
		
		public static TAlgorithm Get<TAlgorithm, TSection, TKey>([NotNull] this IAlgorithmFinder<TAlgorithm, TSection, TKey> me, [NotNull] IEnumerable<KeyValuePair<TSection, TAlgorithm>> algorithms, TSection section)
		{
			return GetBySection(me, algorithms, x => x.ToDictionary(me.KeySelector), section);
		}
		#endregion
		#endregion

		[NotNull, Pure]
		private static IDictionary<TKey, TAlgorithm> GetAlgorithms<TAlgorithm, TSection, TKey>([NotNull] IAlgorithmFinder<TAlgorithm, TSection, TKey> me) {
			return me.Algorithms;
		}

		[NotNull, Pure]
		private static IEnumerable<IPair<T, TAlgorithm>> Convert<TAlgorithm, T>([NotNull] IEnumerable<IAlgorithmObject<TAlgorithm, T>> algorithms) {
			return algorithms.Cast<IPair<T, TAlgorithm>>();
		}

		#region CheckParams
		[Description("only used in add methodes")]
		private static void CheckAlgorithms<T>(T algorithms)
		{
			if(algorithms.IsUndefined()) throw new ArgumentNullException("algorithms");
		}
		
		private static void CheckKey<TKey>(TKey key)
		{
			if(key.IsUndefined()) throw new ArgumentNullException("key");
		}
		
		private static void CheckSection<TSection>(TSection section)
		{
			if(section.IsUndefined()) throw new ArgumentNullException("section");
		}
		#endregion
	}
}