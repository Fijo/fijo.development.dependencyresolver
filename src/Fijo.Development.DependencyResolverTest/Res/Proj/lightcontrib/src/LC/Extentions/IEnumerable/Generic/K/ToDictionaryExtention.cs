using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair {
	public static class ToDictionaryExtention
	{
		#region KeyValuePair
		public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> me) {
			return me.ToDictionary(DefaultKeySelector, DefaultValueSelector);
		}

		public static Dictionary<TResultKey, TResultValue> ToDictionary<TSourceKey, TSourceValue, TResultKey, TResultValue>(this IEnumerable<KeyValuePair<TSourceKey, TSourceValue>> me, System.Func<TSourceKey, TResultKey> keySelector, System.Func<TSourceValue, TResultValue> valueSelector) {
			return me.ToDictionary(x => keySelector(DefaultKeySelector(x)), x => valueSelector(DefaultValueSelector(x)));
		}
		
		public static Dictionary<TResultKey, TValue> ToDictionary<TSourceKey, TResultKey, TValue>(this IEnumerable<KeyValuePair<TSourceKey, TValue>> me, System.Func<TSourceKey, TResultKey> keySelector) {
			return me.ToDictionary(x => keySelector(DefaultKeySelector(x)), DefaultValueSelector);
		}

		public static Dictionary<TKey, TResultValue> ToDictionary<TKey, TSourceValue, TResultValue>(this IEnumerable<KeyValuePair<TKey, TSourceValue>> me, System.Func<TSourceValue, TResultValue> valueSelector) {
			return me.ToDictionary(DefaultKeySelector, x => valueSelector(DefaultValueSelector(x)));
		}
		#endregion

		#region IPair
		public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<IPair<TKey, TValue>> me) {
			return me.ToDictionary(DefaultKeySelector, DefaultValueSelector);
		}

		public static Dictionary<TResultKey, TResultValue> ToDictionary<TSourceKey, TSourceValue, TResultKey, TResultValue>(this IEnumerable<IPair<TSourceKey, TSourceValue>> me, System.Func<TSourceKey, TResultKey> keySelector, System.Func<TSourceValue, TResultValue> valueSelector) {
			return me.ToDictionary(x => keySelector(DefaultKeySelector(x)), x => valueSelector(DefaultValueSelector(x)));
		}
		
		public static Dictionary<TResultKey, TValue> ToDictionary<TSourceKey, TResultKey, TValue>(this IEnumerable<IPair<TSourceKey, TValue>> me, System.Func<TSourceKey, TResultKey> keySelector) {
			return me.ToDictionary(x => keySelector(DefaultKeySelector(x)), DefaultValueSelector);
		}

		public static Dictionary<TKey, TResultValue> ToDictionary<TKey, TSourceValue, TResultValue>(this IEnumerable<IPair<TKey, TSourceValue>> me, System.Func<TSourceValue, TResultValue> valueSelector) {
			return me.ToDictionary(DefaultKeySelector, x => valueSelector(DefaultValueSelector(x)));
		}
		#endregion

		#region KeySelector
		private static TKey DefaultKeySelector<TKey, TValue>(KeyValuePair<TKey, TValue> entry)
		{
			return entry.Key;
		}
		
		private static TKey DefaultKeySelector<TKey, TValue>(IPair<TKey, TValue> entry)
		{
			return entry.First;
		}
		#endregion
		
		#region ValueSelector
		private static TValue DefaultValueSelector<TKey, TValue>(KeyValuePair<TKey, TValue> entry)
		{
			return entry.Value;
		}
		
		private static TValue DefaultValueSelector<TKey, TValue>(IPair<TKey, TValue> entry)
		{
			return entry.Second;
		}
		#endregion
	}
}