using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator
{
    public static class ForEachExtention
    {
		[DebuggerStepThrough]
        public static void ForEach<T>(this IEnumerator<T> me, Action<T> action) {
			#region PreCondition
			if (action == null) throw new ArgumentNullException("action");
			#endregion

        	while (me.MoveNext()) action(me.Current);
        }
		
		[DebuggerStepThrough]
        public static void ForEach<T>(this IEnumerator<T> me, Action<T, int> action) {
			#region PreCondition
			if (action == null) throw new ArgumentNullException("action");
			#endregion

        	var i = 0;
        	while (me.MoveNext()) action(me.Current, i++);
        }
    }
}