using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator
{
	public static class GetIndexExtention
	{
		[DebuggerStepThrough]
		public static TSource GetIndex<TSource>([NotNull] this IEnumerator<TSource> me, int index)
		{
			#region PreCondition
			Debug.Assert(index >= 0);
			#endregion

			var current = -1;
			while (current < index && me.MoveNext()) current++;
			return me.Current;
		}
	}
}