using System.Diagnostics;
using System;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Interface;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator
{
	public static class RecursiveSequenceEqualExtention
	{
		private static IRecursiveEqualityComparerService _recursiveEqualityComparerService;
		private static IEqualityComparerFactory _equalityComparerFactory;
		
		[DebuggerStepThrough]
		internal static void Init() {
			_recursiveEqualityComparerService = Kernel.Resolve<IRecursiveEqualityComparerService>();
			_equalityComparerFactory = Kernel.Resolve<IEqualityComparerFactory>();
		}
		
		[DebuggerStepThrough]
		public static bool RecursiveSequenceEqual(this System.Collections.IEnumerator me, System.Collections.IEnumerator other, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true) {
			return me.SequenceEqual(other, GetEqualsFunc(genericEnumerable, enumerable, enumerator));
		}
		
		[DebuggerStepThrough]
		private static Func<object, object, bool> GetEqualsFunc(bool genericEnumerable, bool enumerable, bool enumerator)
		{
			return genericEnumerable && enumerable && enumerator
			       	? (Func<object, object, bool>) _recursiveEqualityComparerService.DefaultRecursiveSimpleCompare
			       	: (x, y) => _recursiveEqualityComparerService.DefaultRecursiveCompare(x, y, genericEnumerable, enumerable, enumerator);
		}
		
		[DebuggerStepThrough]
		public static bool RecursiveSequenceEqual(this System.Collections.IEnumerator me, System.Collections.IEnumerator other, int deepness, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true)
		{
			return me.SequenceEqual(other, deepness-- > 1 ? GetDeppnessEqualsFunc(deepness, genericEnumerable, enumerable, enumerator) : _equalityComparerFactory.Default().Equals);
		}
		
		[DebuggerStepThrough]
		private static Func<object, object, bool> GetDeppnessEqualsFunc(int deepness, bool genericEnumerable, bool enumerable, bool enumerator) {
			return genericEnumerable && enumerable && enumerator
					? (Func<object, object, bool>)((x, y) => _recursiveEqualityComparerService.DefaultRecursiveSimpleCompareDeepness(x, y, deepness))
					: (x, y) => _recursiveEqualityComparerService.DefaultRecursiveCompare(x, y, deepness, genericEnumerable, enumerable, enumerator);
		}
	}
}