using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class EveryFourthExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TSource> EveryFourth<TSource>([NotNull] this IEnumerable<TSource> me) {
			return me.EveryTh(4);
		}
	}
}