using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Integer
{
	public static class InvariantToStringExtention {
		[Pure, DebuggerStepThrough, Description("Converts a int to string using invariant culture settings")]
		public static string InvariantToString(this int me)
		{
			return me.ToString(CultureInfo.InvariantCulture);
		}
	}
}