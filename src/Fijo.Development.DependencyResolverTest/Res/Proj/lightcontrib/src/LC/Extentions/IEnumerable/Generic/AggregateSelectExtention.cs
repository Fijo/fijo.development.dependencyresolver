using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class AggregateSelectExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TAccumulate> AggregateSelect<TSource, TAccumulate>([NotNull] this IEnumerable<TSource> me, TAccumulate seed, [NotNull] Func<TAccumulate, TSource, TAccumulate> func) { 
			#region PreCondition
			if (seed.IsUndefined()) throw new ArgumentNullException("seed");
			#endregion
			var result = seed;
			return me.Select(source => result = func(result, source));				
		}

		[DebuggerStepThrough]
		public static IEnumerable<TResult> AggregateSelect<TSource, TAccumulate, TResult>([NotNull] this IEnumerable<TSource> me, TAccumulate seed, [NotNull] Func<TAccumulate, TSource, TAccumulate> func, [NotNull] Func<TAccumulate, TSource, TResult> resultItemSelector) { 
			#region PreCondition
			if (seed.IsUndefined()) throw new ArgumentNullException("seed");
			#endregion
			var result = seed;
			foreach (var source in me) {
				yield return resultItemSelector(result, source);
				result = func(result, source);
			}
		}
	}
}