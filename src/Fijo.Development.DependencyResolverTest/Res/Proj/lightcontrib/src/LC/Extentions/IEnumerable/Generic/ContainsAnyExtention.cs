using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
    public static class ContainsAnyExtention
    {
		[DebuggerStepThrough]
        public static bool ContainsAny<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection) {
            return collection.Any(me.Contains);
        }
		
		[DebuggerStepThrough]
		public static bool ContainsAny<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection, [NotNull] Func<T, T, bool> equals) {
			return collection.Any(x => me.Any(y => equals(x, y)));
		}
		
		// ToDo Refactor this
		[DebuggerStepThrough]
        public static bool ContainsAny<T>([NotNull] this IEnumerable<T> me, params IEnumerable<T>[] collections) {
			#region PreCondition
			if (collections == null) throw new ArgumentNullException("collections");
			#endregion
        	return collections.Aggregate(false, (current, collection) => current || me.ContainsAny(collection));
        }
		
		[DebuggerStepThrough]
		public static bool ContainsAny<T>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, T, bool> equals, params IEnumerable<T>[] collections) {
			#region PreCondition
			if (collections == null) throw new ArgumentNullException("collections");
			#endregion
        	return collections.Aggregate(false, (current, collection) => current || me.ContainsAny(collection, equals));
		}
    }
}