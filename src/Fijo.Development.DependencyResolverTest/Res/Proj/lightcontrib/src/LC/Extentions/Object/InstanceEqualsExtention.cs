using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Object {
	public static class InstanceEqualsExtention {
		[Pure, DebuggerStepThrough, Description("returns if the given instances are the same instance")]
		public static bool InstanceEquals(this object me, object other) {
			return RuntimeHelpers.Equals(me, other);
		}
	}
}