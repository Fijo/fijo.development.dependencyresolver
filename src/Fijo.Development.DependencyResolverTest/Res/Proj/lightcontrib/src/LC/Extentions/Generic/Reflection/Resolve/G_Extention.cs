using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Type;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.Reflection.Resolve {
	public static class GenericEnumerableToEnumeratorExtention {
		[DebuggerStepThrough]
		public static System.Collections.IEnumerator GenericEnumerableToEnumerator<T>(this T me) {
			#region PreCondition
			Debug.Assert(me.IsUndefined());
			Debug.Assert(me.GetType().ImplementsInteface(typeof (IEnumerable<>)));
			#endregion
			var type = me.GetType();
			var genericType = type.GetGenericArguments().FirstOrDefault();
			if (genericType == null) throw new InvalidCastException();
			var method = typeof (IEnumerable<>).GetMethod("GetEnumerator");
			return (System.Collections.IEnumerator) method.Invoke(me, null);
		}
	}
}