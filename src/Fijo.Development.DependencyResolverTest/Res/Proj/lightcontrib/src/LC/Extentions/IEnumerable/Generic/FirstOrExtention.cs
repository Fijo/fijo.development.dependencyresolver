using System;
using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class FirstOrExtention {
		[Pure, DebuggerStepThrough]
		public static TSource FirstOr<TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource> defaultValueProvider) {
			using (var enumerator = me.GetEnumerator()) return enumerator.FirstOr(defaultValueProvider);
		}
	}
}