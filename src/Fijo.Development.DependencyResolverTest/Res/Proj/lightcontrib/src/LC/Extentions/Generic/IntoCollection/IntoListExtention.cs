using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection {
	public static class IntoListExtention {
		[Pure, DebuggerStepThrough, Description("Creates a new List<T> with this a only entry")]
		public static List<T> IntoList<T>(this T me) {
			return new List<T> {me};
		}
	}
}