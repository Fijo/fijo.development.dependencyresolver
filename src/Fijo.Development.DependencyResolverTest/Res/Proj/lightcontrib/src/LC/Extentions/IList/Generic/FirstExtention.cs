using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	public static class FirstExtention {
		[DebuggerStepThrough]
		public static TSource First<TSource>([NotNull] this IList<TSource> me) {
			return me.IndexTh(1);
		}
	}
}