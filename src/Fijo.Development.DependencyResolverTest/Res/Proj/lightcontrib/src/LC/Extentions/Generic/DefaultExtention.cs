﻿using System.ComponentModel;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic {
	public static class DefaultExtention {
		[Pure, DebuggerStepThrough, Dangerous("T is struct || T is enum", "default(T) will be handled as defaultValue - for example when ´T is int´ and ´me == 0´ you will get your defaultValue."), Description("uses a given default value instead of default(T)")]
		public static T Default<T>(this T me, T defaultValue) {
			return Equals(me, default(T)) ? defaultValue : me;
		}
	}
}