using System;
using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double {
	public static class DecMoveExtention {
		[Pure, DebuggerStepThrough, Description("Moves the comma of x decimal digits left and returns the result.")]
		public static double DecMove(this double me, int movement) {
			return me / Math.Pow(10, movement);
		}
	}
}