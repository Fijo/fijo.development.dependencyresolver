using System;
using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class RecursiveDoExtention {
		[DebuggerStepThrough]
		public static IEnumerable<T> RecursiveDo<T>([NotNull] this IEnumerable<T> me, [NotNull] Func<IEnumerable<T>, IEnumerable<T>> predicate, int times) {
			#region PreCondition
			if (times <= 0) throw new InvalidCountInputException();
			#endregion
			var collection = me;
			for (var i = 0; i < times; i++)
				collection = predicate(collection);
			return collection;
		}
	}
}