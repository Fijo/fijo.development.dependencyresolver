using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.GetHashCode {
	public static class NullableGetHashCodeExtention {
		[Pure, DebuggerStepThrough, Description("returns the hash code of this allowing this to be null")]
		public static int NullableGetHashCode<T>(this T me) {
			return me.IsUndefined() ? 0 : me.GetHashCode();
		}
	}
}