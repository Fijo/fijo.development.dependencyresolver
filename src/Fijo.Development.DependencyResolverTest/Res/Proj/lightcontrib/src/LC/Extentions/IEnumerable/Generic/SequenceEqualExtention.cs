using System.Diagnostics;
using System;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	public static class SequenceEqualExtention
	{
		[DebuggerStepThrough]
		public static bool SequenceEqual<T>([NotNull] this System.Collections.Generic.IEnumerable<T> me, [NotNull] System.Collections.Generic.IEnumerable<T> other, [NotNull] Func<T, T, bool> equalsFunc) {
			return me.GetEnumerator().SequenceEqual(other.GetEnumerator(), equalsFunc);
		}
	}
}