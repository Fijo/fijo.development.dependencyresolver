﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Boolean
{
    public static class ToIntegerExtention
    {
		[Pure, DebuggerStepThrough, Description("Converts a bool to int")]
        public static int ToInteger(this bool me)
        {
			return Convert.ToInt32(me);
        }
    }
}