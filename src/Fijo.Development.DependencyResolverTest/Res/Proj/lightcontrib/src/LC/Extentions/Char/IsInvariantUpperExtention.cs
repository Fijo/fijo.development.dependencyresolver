using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char
{
	public static class IsInvariantUpperExtention {
		[Pure, DebuggerStepThrough, Description("checks if a char is in upper case using an invariant culture settings")]
		public static bool IsInvariantUpper(this char me)
		{
			return char.ToUpperInvariant(me).Equals(me);
		}
	}
}