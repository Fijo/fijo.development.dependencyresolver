using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	public static class ExecuteExtention
	{
		private static IExecutedEnumerableFactory _executedEnumerableFactory;

		internal static void Init()
		{
			_executedEnumerableFactory = Kernel.Resolve<IExecutedEnumerableFactory>();
		}

		[DebuggerStepThrough]
		public static ICollection<T> Execute<T>([NotNull] this IEnumerable<T> me)
		{
			return _executedEnumerableFactory.CreateExecutedEnumerable(me);
		}
	}
}