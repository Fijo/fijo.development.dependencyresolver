using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class AggregateExtention {
		[DebuggerStepThrough]
		public static TAccumulate Aggregate<TSource, TAccumulate>([NotNull] this IEnumerable<TSource> me, TAccumulate seed, [NotNull] Func<TAccumulate, TSource, int, TAccumulate> func) {
			var i = 0;
			return me.Aggregate(seed, (a, x) => func(a, x, i++));
		}
	}
}