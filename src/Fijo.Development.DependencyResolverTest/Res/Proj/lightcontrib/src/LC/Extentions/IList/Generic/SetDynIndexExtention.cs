using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Delegates;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic
{
	public static class SetDynIndexExtention {
		[DebuggerStepThrough]
		public static void SetDynIndex<TSource>([NotNull] this IList<TSource> me, int index, TSource value, TSource fillupValue = default(TSource)) where TSource : class
		{
			me.SetDynIndex(index, value, DefaultUseExisingValue, DefaultConcatWithExistingValue);
		}

		[DebuggerStepThrough]
		public static void SetDynIndex<TSource>([NotNull] this IList<TSource> me, int index, TSource value, [NotNull] MergeWithExisting<TSource> mergeWithExistingValue, TSource fillupValue = default(TSource)) where TSource : class
		{
			me.SetDynIndex(index, value, DefaultUseExisingValue, mergeWithExistingValue);
		}

		[DebuggerStepThrough]
		public static void SetDynIndex<TSource>([NotNull] this IList<TSource> me, int index, TSource value, [NotNull] System.Func<TSource, bool> useExistingValue, TSource fillupValue = default(TSource)) where TSource : class
		{
			me.SetDynIndex(index, value, useExistingValue, DefaultConcatWithExistingValue);
		}

		[DebuggerStepThrough]
		private static bool DefaultUseExisingValue<TSource>(TSource value) where TSource : class
		{
			return !Equals(value, null);
		}

		[DebuggerStepThrough]
		private static TSource DefaultConcatWithExistingValue<TSource>(TSource old, TSource value)
		{
			return value;
		}

		[DebuggerStepThrough]
		public static void SetDynIndex<TSource>([NotNull] this IList<TSource> me, int index, TSource value, [NotNull] System.Func<TSource, bool> useExistingValue, [NotNull] MergeWithExisting<TSource> mergeWithExistingValue, TSource fillupValue = default(TSource))
		{
			var count = me.Count;
			for (; count <= index; count++) me.Add(fillupValue);
			if (count == index) me.Add(value);
			else
			{
				var existing = me[index];
				if (useExistingValue(existing)) me[index] = mergeWithExistingValue(existing, value);
				else me[index] = value;
			}
		}
	}
}