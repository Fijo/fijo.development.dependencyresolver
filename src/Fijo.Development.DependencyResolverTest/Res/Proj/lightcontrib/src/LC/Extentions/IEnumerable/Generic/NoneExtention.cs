using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
    public static class NoneExtention
    {
		[DebuggerStepThrough]
		public static bool None<TSource>([NotNull] this IEnumerable<TSource> me) {
			return !me.Any();
		}
		
		[DebuggerStepThrough]
        public static bool None<TSource>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, bool> predicate)
        {
            return !me.Any(predicate);
        }
    }
}