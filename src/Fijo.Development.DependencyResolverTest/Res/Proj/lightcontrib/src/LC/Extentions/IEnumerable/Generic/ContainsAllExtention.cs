using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
    public static class ContainsAllExtention
    {
		[DebuggerStepThrough]
        public static bool ContainsAll<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection)
        {
            return collection.All(me.Contains);
        }
		
		[DebuggerStepThrough]
		public static bool ContainsAll<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection, [NotNull] Func<T, T, bool> equals) {
			return collection.All(x => me.All(y => equals(x, y)));
		}

		// ToDo Refactor this
		[DebuggerStepThrough]
        public static bool ContainsAll<T>([NotNull] this IEnumerable<T> me, params IEnumerable<T>[] collections) {
			#region PreCondition
			if (collections == null) throw new ArgumentNullException("collections");
			#endregion

        	return collections.Aggregate(false, (current, collection) => current || me.ContainsAll(collection));
        }
		
		[DebuggerStepThrough]
		public static bool ContainsAll<T>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, T, bool> equals, params IEnumerable<T>[] collections) {
			#region PreCondition
			if (collections == null) throw new ArgumentNullException("collections");
			#endregion

        	return collections.Aggregate(false, (current, collection) => current || me.ContainsAll(collection, equals));
		}
    }
}