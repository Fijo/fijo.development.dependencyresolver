using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class SelectManyExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TResult> SelectMany<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, IEnumerable<TResult>> collectionSelector, bool executeParallel) {
			return me.OptAction(x => x.SelectMany(collectionSelector), x => x.SelectMany(collectionSelector), executeParallel);
		}
		
		[DebuggerStepThrough]
		public static IEnumerable<TResult> SelectMany<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, int, IEnumerable<TResult>> collectionSelector, bool executeParallel) {
			return me.OptAction(x => x.SelectMany(collectionSelector), x => x.SelectMany(collectionSelector), executeParallel);
		}
	}
}