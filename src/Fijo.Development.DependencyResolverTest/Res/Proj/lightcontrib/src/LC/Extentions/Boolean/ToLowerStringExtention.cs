using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Boolean
{
	public static class ToLowerStringExtention
	{
		[NotNull, Pure, DebuggerStepThrough, Description("Converts a bool to a lowercase string (�true�/ �false�)")]
		public static string ToLowerString(this bool me) {
			return me ? "true" : "false";
		}
	}
}