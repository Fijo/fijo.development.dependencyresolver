using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Type
{
	public static class ImplementsIntefaceExtention
    {
		[Pure, DebuggerStepThrough, Description("returns if this is implements the given interface")]
        public static bool ImplementsInteface([NotNull] this System.Type me, [NotNull] System.Type @interface) {
			Debug.Assert(@interface.IsInterface, "@interface.IsInterface");
			return me.GetInterface(@interface.FullName) != null;
        }
    }
}