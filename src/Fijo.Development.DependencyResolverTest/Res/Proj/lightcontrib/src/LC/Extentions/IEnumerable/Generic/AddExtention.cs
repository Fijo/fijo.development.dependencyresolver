using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
    public static class AddExtention
    {
		[DebuggerStepThrough]
		public static IEnumerable<T> AddReturn<T>([NotNull] this IEnumerable<T> me, T item) {
			return me.AddRangeReturn(item.IntoEnumerable());
		}
    }
}