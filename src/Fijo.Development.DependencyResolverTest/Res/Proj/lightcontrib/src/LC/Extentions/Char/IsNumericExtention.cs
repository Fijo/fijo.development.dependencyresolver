using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char
{
	public static class IsNumericExtention
	{
		[NotNull]
        private static readonly IList<char> NumericChars = new List<char>("0123456789".ToCharArray());
		
		[Pure, DebuggerStepThrough, Description("Returns if if the given char is one of these �0123456789�. Otherwise it will return false.")]
		public static bool IsNumeric([NotNull] this char? me)
		{
// ReSharper disable PossibleInvalidOperationException
			return ((char) me).IsNumeric();
// ReSharper restore PossibleInvalidOperationException
		}
		
		[Pure, DebuggerStepThrough, Description("Returns if if the given char is one of these �0123456789�. Otherwise it will return false.")]
        public static bool IsNumeric(this char me)
		{
            return NumericChars.Contains(me);
		}
	}
}
