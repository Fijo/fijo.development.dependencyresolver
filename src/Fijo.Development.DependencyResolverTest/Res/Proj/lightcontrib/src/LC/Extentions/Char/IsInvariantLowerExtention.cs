using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char
{
	public static class IsInvariantLowerExtention {
		[Pure, DebuggerStepThrough, Description("checks if a char is in lower case using an invariant culture settings")]
		public static bool IsInvariantLower(this char me)
		{
			return char.ToLowerInvariant(me).Equals(me);
		}
	}
}