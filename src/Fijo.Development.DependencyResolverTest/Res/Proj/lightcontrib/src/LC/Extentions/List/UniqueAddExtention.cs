using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.ListFunc.Interface;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.List
{
    public static class UniqueAddExtention
    {
        private static IUniqueAddRangeService _uniqueAddRangeService;
        private static IDefaultListFunc _defaultListFunc;
 
		[DebuggerStepThrough]
        internal static void Init()
        {
            _uniqueAddRangeService = Kernel.Resolve<IUniqueAddRangeService>();
            _defaultListFunc = Kernel.Resolve<IDefaultListFunc>();
        }
		
		[DebuggerStepThrough]
        public static void AssertUniqueAddRange<T>([NotNull] this List<T> me, [NotNull] IEnumerable<T> collection)
        {
            #region PreCondition
            Debug.Assert(_uniqueAddRangeService != null, "_uniqueAddRangeService != null");
            Debug.Assert(_defaultListFunc != null, "_defaultListFunc != null");
            #endregion

            _uniqueAddRangeService.AssertUniqueAddRange(me, collection, _defaultListFunc.Contains, _defaultListFunc.AddRange);
        }
		
		[DebuggerStepThrough]
        public static void UniqueAddRange<T>([NotNull] this List<T> me, [NotNull] IEnumerable<T> collection)
        {
            #region PreCondition
            Debug.Assert(_uniqueAddRangeService != null, "_uniqueAddRangeService != null");
            Debug.Assert(_defaultListFunc != null, "_defaultListFunc != null");
            #endregion

            _uniqueAddRangeService.UniqueAddRange(me, collection, _defaultListFunc.Contains, _defaultListFunc.AddRange);
        }
    }
}