using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair {
	public static class ToDistinctedDictionaryExtention
	{
		public static Dictionary<TKey, TValue> ToDistinctedDictionary<TElement, TKey, TValue>(this IEnumerable<TElement> me, Func<TElement, TKey> keySelector, Func<TElement, TValue> valueSelector) {
			var dict = new Dictionary<TKey, TValue>();
			foreach (var entry in me) {
				var key = keySelector(entry);
				if(dict.ContainsKey(key)) continue;
				dict.Add(key, valueSelector(entry));
			}
			return dict;
		}

		public static Dictionary<TKey, TValue> ToDistinctedDictionary<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> me) {
			return me.ToDistinctedDictionary(x => x.Key, x => x.Value);
		}

		public static Dictionary<TKey, TValue> ToDistinctedDictionary<TKey, TValue>(this IEnumerable<IPair<TKey, TValue>> me) {
			return me.ToDistinctedDictionary(x => x.First, x => x.Second);
		}
	}
}