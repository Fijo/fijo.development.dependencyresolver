using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class FivethExtention {
		[DebuggerStepThrough]
		public static TSource Fourth<TSource>(this IEnumerator<TSource> me) {
			return me.IndexTh(5);
		}
	}
}