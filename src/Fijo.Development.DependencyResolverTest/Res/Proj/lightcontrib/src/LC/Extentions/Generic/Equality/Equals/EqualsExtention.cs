using System;
using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.Equals {
	public static class EqualsExtention {
		[Pure, DebuggerStepThrough, Description("Compares two instances of a type if they are the equal using a selector to get the values to comare")]
		public static bool Equals<TSource, TEquals>(this TSource me, TSource other, [NotNull] Func<TSource, TEquals> innerSelector) {
			return Equals(innerSelector(me), innerSelector(other));
		}
	}
}