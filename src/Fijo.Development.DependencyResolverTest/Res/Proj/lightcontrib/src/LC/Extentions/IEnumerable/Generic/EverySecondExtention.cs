using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class EverySecondExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TSource> EverySecond<TSource>([NotNull] this IEnumerable<TSource> me) {
			return me.EveryTh(2);
		}
	}
}