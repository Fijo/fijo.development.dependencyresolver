using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
/*
	// ToDo Buggy
	public static class RecursiveSequenceEqualExtention
	{
		private static EqualityComparerFactory _equalsComparer;

		internal static void Init() {
			_equalsComparer = Kernel.Resolve<EqualityComparerFactory>();
		}

		public static bool RecursiveSequenceEqual<T>(this IEnumerable<T> me, IEnumerable<T> other) where T : class {
			#region PreCondition
			Debug.Assert(_equalsComparer != null, "_equalsComparer != null");
			#endregion

			return me.SequenceEqual(other, _equalsComparer.TypedDefaultRecursiveCompare<T>());
		}

		public static bool RecursiveSequenceEqual<T>(this IEnumerable<T> me, IEnumerable<T> other, int deepness) where T : class {
			#region PreCondition
			Debug.Assert(_equalsComparer != null, "_equalsComparer != null");
			Debug.Assert(deepness > 0);
			Debug.WriteLine(string.Format("IEnumerable.Generic.RecursiveSequenceEqual called. deepness = {0}", deepness));
			#endregion

			return me.SequenceEqual(other, new RecursiveLimitedEqualityComparer<T>((a, b, comparer) => a.RecursiveSequenceEqual(b, comparer), (a, b, maxDeepness) => a.RecursiveSequenceEqual(b, maxDeepness), deepness -1));
		}

		public static bool RecursiveSequenceEqual<T>(this IEnumerable<T> me, IEnumerable<T> other, RecursiveLimitedEqualityComparer<T> limitedEqualityComparer) where T : class {
			#region PreCondition
			Debug.Assert(_equalsComparer != null, "_equalsComparer != null");
			Debug.Assert(limitedEqualityComparer != null, "limitedEqualityComparer != null");
			#endregion

			limitedEqualityComparer.BeginCompare();
			var valueToReturn = me.SequenceEqual(other, limitedEqualityComparer);
			limitedEqualityComparer.EndCompare();
			return valueToReturn;
		}
	}
	*/


	public static class RecursiveSequenceEqualExtention
	{
		[DebuggerStepThrough]
		public static bool RecursiveSequenceEqual<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> other, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true) {
			return me.GetEnumerator().RecursiveSequenceEqual(other.GetEnumerator(), genericEnumerable, enumerable, enumerator);
		}
		
		[DebuggerStepThrough]
		public static bool RecursiveSequenceEqual<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> other, int deepness, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true) {
			return me.GetEnumerator().RecursiveSequenceEqual(other.GetEnumerator(), deepness, genericEnumerable, enumerable, enumerator);
		}
	}
}