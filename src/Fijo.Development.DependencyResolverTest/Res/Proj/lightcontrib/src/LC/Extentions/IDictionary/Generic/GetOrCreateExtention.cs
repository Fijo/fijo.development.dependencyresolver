using System;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic
{
	public static class GetOrCreateExtention
	{
		[DebuggerStepThrough]
		public static TValue GetOrCreate<TCreateValue, TKey, TValue>(this System.Collections.Generic.IDictionary<TKey, TValue> me, TKey key) where TCreateValue : TValue, new()
		{
			return me.GetOrCreate(key, () => new TCreateValue());
		}

		[DebuggerStepThrough]
		public static TValue GetOrCreate<TKey, TValue>(this System.Collections.Generic.IDictionary<TKey, TValue> me, TKey key) where TValue : new()
		{
			return me.GetOrCreate(key, () => new TValue());
		}

		[DebuggerStepThrough]
		public static TValue GetOrCreate<TKey, TValue>(this System.Collections.Generic.IDictionary<TKey, TValue> me, TKey key, Func<TValue> createValue)
		{
			TValue value;
			lock(me) if (!me.TryGetValue(key, out value)) me.Add(key, value = createValue());
			return value;
		}
	}
}