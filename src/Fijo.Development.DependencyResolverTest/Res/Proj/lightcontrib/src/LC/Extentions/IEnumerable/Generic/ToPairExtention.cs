using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.To;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	public static class ToPairExtention
	{
		[DebuggerStepThrough]
		public static IEnumerable<IPair<TKey, TValue>> ToPair<TSource, TKey, TValue>([NotNull] this IEnumerable<TSource> me, [NotNull] System.Func<TSource, TKey> keySelector, [NotNull] System.Func<TSource, TValue> valueSelector) {
			return me.Select(x => x.ToPair(keySelector, valueSelector));
		}

		[DebuggerStepThrough]
		public static IEnumerable<KeyValuePair<TKey, TValue>> ToKeyValuePair<TSource, TKey, TValue>([NotNull] this IEnumerable<TSource> me, [NotNull] System.Func<TSource, TKey> keySelector, [NotNull] System.Func<TSource, TValue> valueSelector) {
			return me.Select(x => x.ToKeyValuePair(keySelector, valueSelector));
		}
	}
}