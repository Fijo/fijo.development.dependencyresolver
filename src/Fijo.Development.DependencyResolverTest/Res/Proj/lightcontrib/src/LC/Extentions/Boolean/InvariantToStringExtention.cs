using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Boolean
{
	public static class InvariantToStringExtention {
		[NotNull, Pure, DebuggerStepThrough, Description("Converts a bool to string using invariant culture settings")]
		public static string InvariantToString(this bool me)
		{
			return me.ToString(CultureInfo.InvariantCulture);
		}
	}
}