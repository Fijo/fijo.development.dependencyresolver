using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class IndexOfExtention {
		[Pure, DebuggerStepThrough]
		public static int IndexOf<TSource>([NotNull] this IEnumerator<TSource> me, TSource item, [CanBeNull] IEqualityComparer equalityComparer) {
			return GetFirstOrFailValue(me.IndexesOf(item, equalityComparer));
		}

		[Pure, DebuggerStepThrough]
		public static int IndexOf<TSource>([NotNull] this IEnumerator<TSource> me, TSource item) {
			return GetFirstOrFailValue(me.IndexesOf(item));
		}
		
		[Pure, DebuggerStepThrough]
		private static int GetFirstOrFailValue([NotNull] IEnumerable<int> matchingIndexes) {
			return matchingIndexes.FirstOr(IndexFailDefaultValueProvider);
		}
		
		[Pure, DebuggerStepThrough]
		private static int IndexFailDefaultValueProvider() {
			return -1;
		}
	}
}