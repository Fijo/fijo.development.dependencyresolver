using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	public static class IndexThExtention {
		[DebuggerStepThrough]
		public static TSource IndexTh<TSource>([NotNull] this IList<TSource> me, int index) {
			return me.Count > --index ? me[index] : default(TSource);
		}
	}
}