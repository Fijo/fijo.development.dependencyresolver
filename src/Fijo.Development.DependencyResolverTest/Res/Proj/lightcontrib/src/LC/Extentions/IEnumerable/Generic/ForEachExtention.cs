using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
    public static class ForEachExtention
    {
		[DebuggerStepThrough]
        public static void ForEach<T>([NotNull] this IEnumerable<T> me, [NotNull] Action<T> action)
        {
            using (var enumerator = me.GetEnumerator()) enumerator.ForEach(action);
        }
		
		[DebuggerStepThrough]
        public static void ForEach<T>([NotNull] this IEnumerable<T> me, [NotNull] Action<T, int> action)
        {
            using (var enumerator = me.GetEnumerator()) enumerator.ForEach(action);
        }

		[DebuggerStepThrough]
        public static void ForEach<T>([NotNull] this IEnumerable<T> me, [NotNull] Action<T> action, bool executeParalell)
        {
			if(executeParalell) Parallel.ForEach(me, action);
			else me.ForEach(action);
        }

    }
}