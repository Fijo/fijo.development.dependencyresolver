using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic {
	public static class IsInExtention {
		[Pure, DebuggerStepThrough, Description("returns if this is contained in a the given param array")]
		public static bool IsIn<T>(this T me, params T[] searchParam) {
			return searchParam.Contains(me);
		}

		[Pure, DebuggerStepThrough, Description("returns if this is contained in a the given collection")]
		public static bool IsIn<T>(this T me, System.Collections.Generic.IEnumerable<T> searchParam) {
			return searchParam.Contains(me);
		}
	}
}