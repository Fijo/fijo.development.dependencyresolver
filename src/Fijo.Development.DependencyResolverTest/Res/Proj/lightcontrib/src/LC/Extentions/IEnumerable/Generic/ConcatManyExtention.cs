using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class ConcatManyExtention
	{
		[DebuggerStepThrough]
		public static IEnumerable<TSource> ConcatMany<TSource>([NotNull] this IEnumerable<TSource> me, params IEnumerable<TSource>[] collections) {
			return collections.Aggregate(me, (current, entry) => current.Concat(entry));
		}
		
		[DebuggerStepThrough, Description("Select �me�, too")]
		public static IEnumerable<TResult> ConcatManySelect<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<IEnumerable<TSource>, IEnumerable<TResult>> selector, params IEnumerable<TSource>[] collections) {
			return collections.Aggregate(selector(me), (current, entry) => current.Concat(selector(entry)));
		}
		
		[DebuggerStepThrough, Description("Do not select �me�")]
		public static IEnumerable<TResult> ConcatManySelect<TSource, TResult>([NotNull] this IEnumerable<TResult> me, [NotNull] Func<IEnumerable<TSource>, IEnumerable<TResult>> selector, params IEnumerable<TSource>[] collections) {
			return collections.Aggregate(me, (current, entry) => current.Concat(selector(entry)));
		}
	}
}