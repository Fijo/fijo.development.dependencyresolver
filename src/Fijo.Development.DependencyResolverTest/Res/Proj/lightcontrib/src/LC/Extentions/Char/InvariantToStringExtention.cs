using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char
{
	public static class InvariantToStringExtention {
		[Pure, DebuggerStepThrough, Description("Converts a char to string using invariant culture settings")]
		public static string InvariantToString(this char me)
		{
			return me.ToString(CultureInfo.InvariantCulture);
		}
	}
}