using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic {
	public static class DefaultClassExtention {
		[Pure, DebuggerStepThrough, Description("uses a given default value instead of null")]
		public static T DefaultClass<T>(this T me, T defaultValue) {
			#region PreCondition
			Debug.Assert(me.GetType().IsClass, "me.GetType().IsClass");
			#endregion
			return Equals(me, null) ? defaultValue : me;
		}
	}
}