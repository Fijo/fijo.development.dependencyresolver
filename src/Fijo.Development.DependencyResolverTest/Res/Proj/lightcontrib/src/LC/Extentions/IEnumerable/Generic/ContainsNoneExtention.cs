﻿using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
    public static class ContainsNoneExtention
    {
		[DebuggerStepThrough]
        public static bool ContainsNone<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection)
        {
            return !me.ContainsAny(collection);
        }
    }
}