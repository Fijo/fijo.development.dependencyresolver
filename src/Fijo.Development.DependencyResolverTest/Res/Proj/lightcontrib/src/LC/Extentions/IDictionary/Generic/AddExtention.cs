using System.Collections.Generic;
using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic
{
	public static class AddExtention
	{
		#region Generic
		public static void Add<TKey, TValue, TElement>(this IDictionary<TKey, TValue> me, TElement element, System.Func<TElement, TKey> keySelector, System.Func<TElement, TValue> valueSelector)
		{
			me.Add(keySelector(element), valueSelector(element));
		}
		
		public static void Add<TKey, TSelectorKey, TValue, TElement>(this IDictionary<TKey, TValue> me, TElement element, System.Func<TElement, TSelectorKey> keySelector, System.Func<TElement, TValue> valueSelector, System.Func<TSelectorKey, TKey> resultKeySelector)
		{
			me.Add(resultKeySelector(keySelector(element)), valueSelector(element));
		}
		
		public static void Add<TKey, TValue, TSelectorValue, TElement>(this IDictionary<TKey, TValue> me, TElement element, System.Func<TElement, TKey> keySelector, System.Func<TElement, TSelectorValue> valueSelector, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			me.Add(keySelector(element), resultValueSelector(valueSelector(element)));
		}
		
		public static void Add<TKey, TSelectorKey, TValue, TSelectorValue, TElement>(this IDictionary<TKey, TValue> me, TElement element, System.Func<TElement, TSelectorKey> keySelector, System.Func<TElement, TSelectorValue> valueSelector, System.Func<TSelectorKey, TKey> resultKeySelector, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			me.Add(resultKeySelector(keySelector(element)), resultValueSelector(valueSelector(element)));
		}
		#endregion

		#region KeyValuePair
		public static void Add<TKey, TValue>(this IDictionary<TKey, TValue> me, KeyValuePair<TKey, TValue> element)
		{
			me.Add(element, DefaultKeySelector, DefaultValueSelector);
		}
		
		public static void Add<TKey, TSelectorKey, TValue>(this IDictionary<TKey, TValue> me, KeyValuePair<TSelectorKey, TValue> element, System.Func<TSelectorKey, TKey> resultKeySelector)
		{
			me.Add(element, DefaultKeySelector, DefaultValueSelector, resultKeySelector);
		}
		
		public static void Add<TKey, TValue, TSelectorValue>(this IDictionary<TKey, TValue> me, KeyValuePair<TKey, TSelectorValue> element, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			me.Add(element, DefaultKeySelector, DefaultValueSelector, resultValueSelector);
		}
		
		public static void Add<TKey, TSelectorKey, TValue, TSelectorValue>(this IDictionary<TKey, TValue> me, KeyValuePair<TSelectorKey, TSelectorValue> element, System.Func<TSelectorKey, TKey> resultKeySelector, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			me.Add(element, DefaultKeySelector, DefaultValueSelector, resultKeySelector, resultValueSelector);
		}
		#endregion

		#region IPair
		public static void Add<TKey, TValue>(this IDictionary<TKey, TValue> me, IPair<TKey, TValue> element)
		{
			me.Add(element, DefaultKeySelector, DefaultValueSelector);
		}
		
		public static void Add<TKey, TSelectorKey, TValue>(this IDictionary<TKey, TValue> me, IPair<TSelectorKey, TValue> element, System.Func<TSelectorKey, TKey> resultKeySelector)
		{
			me.Add(element, DefaultKeySelector, DefaultValueSelector, resultKeySelector);
		}
		
		public static void Add<TKey, TValue, TSelectorValue>(this IDictionary<TKey, TValue> me, IPair<TKey, TSelectorValue> element, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			me.Add(element, DefaultKeySelector, DefaultValueSelector, resultValueSelector);
		}
		
		public static void Add<TKey, TSelectorKey, TValue, TSelectorValue>(this IDictionary<TKey, TValue> me, IPair<TSelectorKey, TSelectorValue> element, System.Func<TSelectorKey, TKey> resultKeySelector, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			me.Add(element, DefaultKeySelector, DefaultValueSelector, resultKeySelector, resultValueSelector);
		}
		#endregion

		#region KeySelector
		private static TKey DefaultKeySelector<TKey, TValue>(KeyValuePair<TKey, TValue> entry)
		{
			return entry.Key;
		}
		
		private static TKey DefaultKeySelector<TKey, TValue>(IPair<TKey, TValue> entry)
		{
			return entry.First;
		}
		#endregion
		
		#region ValueSelector
		private static TValue DefaultValueSelector<TKey, TValue>(KeyValuePair<TKey, TValue> entry)
		{
			return entry.Value;
		}
		
		private static TValue DefaultValueSelector<TKey, TValue>(IPair<TKey, TValue> entry)
		{
			return entry.Second;
		}
		#endregion
	}
}