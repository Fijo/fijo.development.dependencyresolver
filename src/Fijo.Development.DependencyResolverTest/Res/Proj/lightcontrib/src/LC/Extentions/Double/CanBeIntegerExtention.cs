using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double
{
    public static class CanBeIntegerExtention {
		[Pure, DebuggerStepThrough, Description("Returns if a double can be lossless converted into an integer.")]
        public static bool CanBeInteger(this double me)
        {
// ReSharper disable CompareOfFloatsByEqualityOperator
            return me - me.ToInteger() == 0;
// ReSharper restore CompareOfFloatsByEqualityOperator
        }
    }
}