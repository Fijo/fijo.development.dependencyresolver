using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class EveryThExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TSource> EveryTh<TSource>([NotNull] this IEnumerable<TSource> me, int index) {
			return me.Where((x, i) => i % index == 0);
		}
	}
}