using System;
using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double {
	public static class IsNegativeExtention {
		[Pure, DebuggerStepThrough, Description("returns if the given double is smaller than 0 what means that it is negative or not")]
		public static bool IsNegative(this double me) {
			return (BitConverter.GetBytes(me)[7] & 128) == 128;
		}
	}
}