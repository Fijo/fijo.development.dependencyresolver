using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator
{
    public static class ShouldResetedExtention
	{
		[DebuggerStepThrough]
		[Unshure("if(returnValue == true && !T.IsNullable())")]
        public static bool ShouldReseted<T>(this IEnumerator<T> me)
        {
            return Equals(me.Current, null);
        }
    }
}