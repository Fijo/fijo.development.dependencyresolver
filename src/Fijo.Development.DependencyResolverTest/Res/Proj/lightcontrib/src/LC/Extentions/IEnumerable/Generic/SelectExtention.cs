using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class SelectExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TResult> Select<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, TResult> resultSelector, bool executeParallel) {
			return me.OptAction(x => x.Select(resultSelector), x => x.Select(resultSelector), executeParallel);
		}
		
		[DebuggerStepThrough]
		public static IEnumerable<TResult> Select<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, int, TResult> resultSelector, bool executeParallel) {
			return me.OptAction(x => x.Select(resultSelector), x => x.Select(resultSelector), executeParallel);
		}
	}
}