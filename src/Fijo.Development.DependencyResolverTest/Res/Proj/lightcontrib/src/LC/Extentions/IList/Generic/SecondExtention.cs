using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	public static class SecondExtention {
		[DebuggerStepThrough]
		public static TSource Second<TSource>([NotNull] this IList<TSource> me) {
			return me.IndexTh(2);
		}
	}
}