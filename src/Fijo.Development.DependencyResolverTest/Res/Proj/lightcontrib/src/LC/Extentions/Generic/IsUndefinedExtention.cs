using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic {
	public static class IsUndefinedExtention {
		[Pure, DebuggerStepThrough, Description("Compares T with null being shure that it do not compare a value type with null.")]
		public static bool IsUndefined<T>(this T me) {
			return !typeof(T).IsValueType && Equals(null, me);
		}
	}
}