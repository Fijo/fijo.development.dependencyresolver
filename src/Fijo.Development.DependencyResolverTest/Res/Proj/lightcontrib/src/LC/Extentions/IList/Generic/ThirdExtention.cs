using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	public static class ThirdExtention {
		[DebuggerStepThrough]
		public static TSource Third<TSource>([NotNull] this IList<TSource> me) {
			return me.IndexTh(3);
		}
	}
}