using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class FivethExtention {
		[DebuggerStepThrough]
		public static TSource Fourth<TSource>([NotNull] this IEnumerable<TSource> me) {
			return me.IndexTh(5);
		}
	}
}