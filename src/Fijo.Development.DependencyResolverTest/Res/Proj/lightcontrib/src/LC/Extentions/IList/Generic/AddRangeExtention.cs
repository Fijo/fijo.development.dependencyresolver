using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic
{
	public static class AddRangeExtention
	{
		[DebuggerStepThrough]
		public static void AddRange<T>([NotNull] this IList<T> me, [NotNull] IEnumerable<T> collection)
		{
		    me.AddRange(collection.ToList());
		}
		
		[DebuggerStepThrough]
        public static void AddRange<T>([NotNull] this IList<T> me, [NotNull] params T[] collection)
        {
            foreach (var entry in collection) me.Add(entry);
        }
		
		[DebuggerStepThrough]
        public static void AddRange<T>([NotNull] this IList<T> me, [NotNull] List<T> collection)
        {
            collection.ForEach(me.Add);
        }
		
		[DebuggerStepThrough]
		public static IEnumerable<T> AddRangeReturn<T>([NotNull] this IList<T> me, [NotNull] IEnumerable<T> collection) {
			me.AddRange(collection);
			return me;
		}
		
		[DebuggerStepThrough]
		public static IEnumerable<T> AddRangeReturn<T>([NotNull] this IList<T> me, [NotNull] params T[] collection) {
			me.AddRange(collection);
			return me;
		}
		
		[DebuggerStepThrough]
		public static IEnumerable<T> AddRangeReturn<T>([NotNull] this IList<T> me, [NotNull] List<T> collection) {
			me.AddRange(collection);
			return me;
		}
	}
}