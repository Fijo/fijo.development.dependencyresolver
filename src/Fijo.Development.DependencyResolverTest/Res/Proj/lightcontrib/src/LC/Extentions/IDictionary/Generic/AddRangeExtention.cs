using System.Collections.Generic;
using Fijo.Infrastructure.Model.Pair.Interface.Base;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic
{
	public static class AddRangeExtention
	{
		#region Generic
		public static void AddRange<TKey, TValue, TElement>(this IDictionary<TKey, TValue> me, IEnumerable<TElement> collection, System.Func<TElement, TKey> keySelector, System.Func<TElement, TValue> valueSelector)
		{
			ForEach(collection, x => me.Add(x, keySelector, valueSelector));
		}
		
		public static void AddRange<TKey, TSelectorKey, TValue, TElement>(this IDictionary<TKey, TValue> me, IEnumerable<TElement> collection, System.Func<TElement, TSelectorKey> keySelector, System.Func<TElement, TValue> valueSelector, System.Func<TSelectorKey, TKey> resultKeySelector)
		{
			ForEach(collection, x => me.Add(x, keySelector, valueSelector, resultKeySelector));
		}
		
		public static void AddRange<TKey, TValue, TSelectorValue, TElement>(this IDictionary<TKey, TValue> me, IEnumerable<TElement> collection, System.Func<TElement, TKey> keySelector, System.Func<TElement, TSelectorValue> valueSelector, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			ForEach(collection, x => me.Add(x, keySelector, valueSelector, resultValueSelector));
		}
		
		public static void AddRange<TKey, TSelectorKey, TValue, TSelectorValue, TElement>(this IDictionary<TKey, TValue> me, IEnumerable<TElement> collection, System.Func<TElement, TSelectorKey> keySelector, System.Func<TElement, TSelectorValue> valueSelector, System.Func<TSelectorKey, TKey> resultKeySelector, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			ForEach(collection, x => me.Add(x, keySelector, valueSelector, resultKeySelector, resultValueSelector));
		}
		#endregion

		#region KeyValuePair
		public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> me, IEnumerable<KeyValuePair<TKey, TValue>> collection)
		{
			ForEach(collection, me.Add);
		}
		
		public static void AddRange<TKey, TSelectorKey, TValue>(this IDictionary<TKey, TValue> me, IEnumerable<KeyValuePair<TSelectorKey, TValue>> collection, System.Func<TSelectorKey, TKey> resultKeySelector)
		{
			ForEach(collection, x => me.Add(x, resultKeySelector));
		}
		
		public static void AddRange<TKey, TValue, TSelectorValue>(this IDictionary<TKey, TValue> me, IEnumerable<KeyValuePair<TKey, TSelectorValue>> collection, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			ForEach(collection, x => me.Add(x, resultValueSelector));
		}
		
		public static void AddRange<TKey, TSelectorKey, TValue, TSelectorValue>(this IDictionary<TKey, TValue> me, IEnumerable<KeyValuePair<TSelectorKey, TSelectorValue>> collection, System.Func<TSelectorKey, TKey> resultKeySelector, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			ForEach(collection, x => me.Add(x, resultKeySelector, resultValueSelector));
		}
		#endregion

		#region IPair
		public static void AddRange<TKey, TValue>(this IDictionary<TKey, TValue> me, IEnumerable<IPair<TKey, TValue>> collection)
		{
			ForEach(collection, me.Add);
		}
		
		public static void AddRange<TKey, TSelectorKey, TValue>(this IDictionary<TKey, TValue> me, IEnumerable<IPair<TSelectorKey, TValue>> collection, System.Func<TSelectorKey, TKey> resultKeySelector)
		{
			ForEach(collection, x => me.Add(x, resultKeySelector));
		}
		
		public static void AddRange<TKey, TValue, TSelectorValue>(this IDictionary<TKey, TValue> me, IEnumerable<IPair<TKey, TSelectorValue>> collection, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			ForEach(collection, x => me.Add(x, resultValueSelector));
		}
		
		public static void AddRange<TKey, TSelectorKey, TValue, TSelectorValue>(this IDictionary<TKey, TValue> me, IEnumerable<IPair<TSelectorKey, TSelectorValue>> collection, System.Func<TSelectorKey, TKey> resultKeySelector, System.Func<TSelectorValue, TValue> resultValueSelector)
		{
			ForEach(collection, x => me.Add(x, resultKeySelector, resultValueSelector));
		}
		#endregion

		private static void ForEach<TElement>(IEnumerable<TElement> collection, System.Action<TElement> action)
		{
			foreach (var element in collection)
			{
				action(element);
			}
		}
	}
}