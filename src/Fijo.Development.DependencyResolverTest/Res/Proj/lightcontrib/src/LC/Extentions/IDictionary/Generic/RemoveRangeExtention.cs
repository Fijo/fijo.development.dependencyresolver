using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic
{
	public static class RemoveRangeExtention {
		[DebuggerStepThrough]
		public static void RemoveRange<TKey, TValue>(this IDictionary<TKey, TValue> me, IEnumerable<TKey> candidates)
		{
			candidates.ForEach(candidate => me.Remove(candidate));
		}
		
		[DebuggerStepThrough]
		public static void RemoveRange<TKey, TValue>(this IDictionary<TKey, TValue> me, IEnumerable<KeyValuePair<TKey, TValue>> candidates)
		{
			candidates.ForEach(candidate => me.Remove(candidate));
		}
	}
}