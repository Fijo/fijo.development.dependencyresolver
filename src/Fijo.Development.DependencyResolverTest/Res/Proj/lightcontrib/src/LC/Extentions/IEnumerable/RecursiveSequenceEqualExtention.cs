using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable
{
/*
	// ToDo Buggy
	public static class RecursiveSequenceEqualExtention
	{
		private static EqualityComparerFactory _equalsComparer;

		internal static void Init()
		{
			_equalsComparer = Kernel.Resolve<EqualityComparerFactory>();
		}

		private static Func<object, object, bool> GetDefaultComparer(int deepness)
		{
			return (x, y) => {
				Debug.WriteLine(string.Format("IEnumerable.RecursiveSequenceEqual => DefaultComparer called. deepness = {0}, x = {1}, y = {2}, EqualMethode = recursive", deepness, x, y));
				Debug.WriteLine(string.Format("x is IEnumerable = {0}, y is IEnumerable = {1}", x is System.Collections.IEnumerable, y is System.Collections.IEnumerable));
			                 return x is System.Collections.IEnumerable && y is System.Collections.IEnumerable
										? RecursiveInternalSequenceEqual((System.Collections.IEnumerable) x,
																		 (System.Collections.IEnumerable) y, deepness)
			                 	       	: x.Equals(y);
			};
		}

		public static bool RecursiveSequenceEqual(this System.Collections.IEnumerable me, System.Collections.IEnumerable other) {
			#region PreCondition
			Debug.Assert(_equalsComparer != null, "_equalsComparer != null");
			#endregion

			return me.GetEnumerator().SequenceEqual(other.GetEnumerator(), _equalsComparer.DefaultRecursiveCompare);
		}

		public static bool RecursiveSequenceEqual(this System.Collections.IEnumerable me, System.Collections.IEnumerable other, int deepness) {
			#region PreCondition
			Debug.Assert(_equalsComparer != null, "_equalsComparer != null");
			Debug.Assert(deepness > 0);
			Debug.WriteLine(string.Format("IEnumerable.RecursiveSequenceEqual called. deepness = {0}, EqualMethode = {1}", deepness, deepness == -1 ? "default" : "recursive"));
			#endregion

			return me.SequenceEqual(other, --deepness == 0 ? _equalsComparer.Default : GetDefaultComparer(deepness));
		}

		private static bool RecursiveInternalSequenceEqual(System.Collections.IEnumerable me, System.Collections.IEnumerable other, int deepness) {
			#region PreCondition
			Debug.Assert(_equalsComparer != null, "_equalsComparer != null");
			Debug.WriteLine(string.Format("IEnumerable.RecursiveInternalSequenceEqual called. deepness = {0}, EqualMethode = {1}", deepness, deepness >= -1 ? "default" : "recursive"));
			#endregion

			return me.SequenceEqual(other, --deepness >= 0 ? _equalsComparer.Default : GetDefaultComparer(deepness));
		}
	}
 * */

	public static class RecursiveSequenceEqualExtention
	{
		[DebuggerStepThrough]
		public static bool RecursiveSequenceEqual([NotNull] this System.Collections.IEnumerable me, [NotNull] System.Collections.IEnumerable other, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true)
		{
			return me.GetEnumerator().RecursiveSequenceEqual(other.GetEnumerator(), genericEnumerable, enumerable, enumerator);
		}
		
		[DebuggerStepThrough]
		public static bool RecursiveSequenceEqual([NotNull] this System.Collections.IEnumerable me, [NotNull] System.Collections.IEnumerable other, int deepness, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true) {
			return me.GetEnumerator().RecursiveSequenceEqual(other.GetEnumerator(), deepness, genericEnumerable, enumerable, enumerator);
		}
	}
}