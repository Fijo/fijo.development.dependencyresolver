using System;
using System.ComponentModel;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.Object;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.Equals {
	public static class InstanceEqualsExtention {
		[Pure, DebuggerStepThrough, Description("Compares two instances of a type if they are the same using a selector to get the values to comare")]
		public static bool InstanceEquals<TSource, TEquals>(this TSource me, TSource other, [NotNull] Func<TSource, TEquals> innerSelector) {
			return innerSelector(me).InstanceEquals(innerSelector(other));
		}
	}
}