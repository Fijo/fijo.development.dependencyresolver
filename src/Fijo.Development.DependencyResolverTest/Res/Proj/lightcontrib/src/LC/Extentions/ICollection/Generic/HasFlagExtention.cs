using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Enum {
	public static class HasFlagExtention {
		[Pure, DebuggerStepThrough, Description("Checks if an instance of an flags enum has a flag �x� set or not."), RelatedLink("http://msdn.microsoft.com/en-us/library/cc138362.aspx")]
		public static bool HasFlag<T>(this T me, T flag) where T : struct, IConvertible {
			#region PreCondition
			Debug.Assert(typeof(T).IsEnum, "typeof(T).IsEnum");
			Debug.Assert(typeof(T).GetCustomAttributes(typeof(FlagsAttribute), false).Any(), "typeof(T).GetCustomAttributes(typeof(FlagsAttribute), false).Any()");
			Debug.Assert(me.GetType() == flag.GetType(), "me.GetType() == flag.GetType()");
			Debug.Assert(me.GetTypeCode().IsIn(TypeCode.Byte, TypeCode.Char, TypeCode.Decimal, TypeCode.Double, TypeCode.Int16, TypeCode.Int32, TypeCode.Int64, TypeCode.UInt16, TypeCode.UInt32, TypeCode.UInt64));
			#endregion
			var isLongEnum = me is long;
			return isLongEnum
			       	? InternalHasFlag(Convert.ToInt32(me), Convert.ToInt32(flag))
			       	: InternalHasFlag(Convert.ToInt64(me), Convert.ToInt64(flag));
		}

		private static bool InternalHasFlag(int me, int flag) {
			return (me & flag) == flag;
		}
		
		private static bool InternalHasFlag(long me, long flag) {
			return (me & flag) == flag;
		}
	}
}