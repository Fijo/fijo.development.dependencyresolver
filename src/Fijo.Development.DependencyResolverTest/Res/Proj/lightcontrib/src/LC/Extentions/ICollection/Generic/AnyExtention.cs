using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic {
	public static class AnyExtention {
		[DebuggerStepThrough]
		public static bool Any<T>(this ICollection<T> me) {
			return me.Count != 0;
		}
	}
}