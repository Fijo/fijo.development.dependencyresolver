using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class GetIndexExtention {
		[Pure, DebuggerStepThrough]
		public static TSource GetIndex<TSource>([NotNull] this IEnumerable<TSource> me, int index) {
			return me.GetEnumerator().GetIndex(index);
		}
	}
}