using System;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Factories;
using FijoCore.Infrastructure.LightContrib.Service.SequenceEqualService.Interface;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator
{
	public static class SequenceEqualExtention
	{
		private static EqualityComparerFactory _equalityComparerFactory;
		private static ISequenceEqualService _sequenceEqual;
		
		[DebuggerStepThrough]
		internal static void Init()
		{
			_equalityComparerFactory = Kernel.Resolve<EqualityComparerFactory>();
			_sequenceEqual = Kernel.Resolve<ISequenceEqualService>();
		}
		
		[DebuggerStepThrough]
		public static bool SequenceEqual(this System.Collections.IEnumerator me, System.Collections.IEnumerator other)
		{
			return _sequenceEqual.SequenceEqual(me, other, _equalityComparerFactory.Default().Equals);
		}
		
		[DebuggerStepThrough]
		public static bool SequenceEqual(this System.Collections.IEnumerator me, System.Collections.IEnumerator other, Func<object, object, bool> equalsFunc)
		{
			return _sequenceEqual.SequenceEqual(me, other, equalsFunc);
		}
		
		[DebuggerStepThrough]
		public static bool SequenceEqual<T>(this System.Collections.Generic.IEnumerator<T> me, System.Collections.Generic.IEnumerator<T> other, Func<T, T, bool> equalsFunc)
		{
			return _sequenceEqual.SequenceEqual((System.Collections.IEnumerator) me, (System.Collections.IEnumerator) other, (x, y) => equalsFunc((T) x, (T) y));
		}
	}
}