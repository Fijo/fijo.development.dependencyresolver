using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class IndexThExtention {
		[DebuggerStepThrough]
		public static TSource IndexTh<TSource>(this IEnumerator<TSource> me, int index) {
			#region PreCondition
			Debug.Assert(me != null, "me != null");
			Debug.Assert(index > 0);
			#endregion

			return me.GetIndex(index - 1);
		}
	}
}