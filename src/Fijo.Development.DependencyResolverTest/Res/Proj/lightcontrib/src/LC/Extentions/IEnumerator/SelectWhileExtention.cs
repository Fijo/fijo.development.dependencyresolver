using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class SelectWhileExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TResult> SelectWhile<TSource, TResult>(this IEnumerator<TSource> me, Func<TSource, TResult> selector, Func<TResult, bool> @while) {
			while (me.MoveNext()) {
				var current = selector(me.Current);
				if(!@while(current)) yield break;
				yield return current;
			}
		}
	}
}