using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	public static class AddExtention {
		[DebuggerStepThrough]
		public static IEnumerable<T> AddReturn<T>([NotNull] this IList<T> me, T item) {
			me.Add(item);
			return me;
		}
	}
}