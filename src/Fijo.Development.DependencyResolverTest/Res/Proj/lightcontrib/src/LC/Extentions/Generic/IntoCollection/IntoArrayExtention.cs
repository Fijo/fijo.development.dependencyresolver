using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection {
	public static class IntoArrayExtention {
		[Pure, DebuggerStepThrough, Description("Creates a new array[1] of type T with this as index 0 value")]
		public static T[] IntoArray<T>(this T me) {
			return new[] {me};
		}
	}
}