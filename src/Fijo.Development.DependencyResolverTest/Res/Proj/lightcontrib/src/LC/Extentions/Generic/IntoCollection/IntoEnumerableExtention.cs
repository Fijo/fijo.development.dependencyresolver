using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection {
	public static class IntoEnumerableExtention {
		[Pure, DebuggerStepThrough, LinqTunnel, Description("returns a IEnumerable<T> with this a only entry")]
		public static System.Collections.Generic.IEnumerable<T> IntoEnumerable<T>(this T me) {
			yield return me;
		}
	}
}