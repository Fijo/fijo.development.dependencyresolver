using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic
{
	public static class ReplaceExtention
	{
		[DebuggerStepThrough]
		public static void Replace<T>([NotNull] this IList<T> me, [NotNull] T @new, [NotNull] T old)
		{
			lock (me) {
				if(!me.Contains(old)) throw new KeyNotFoundException();
				me[me.IndexOf(old)] = @new;
			}
		}
	}
}