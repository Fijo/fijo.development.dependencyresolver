using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.List {
	public static class MaxLengthExtention
	{
		[DebuggerStepThrough]
		public static void MaxLength<T>([NotNull] this List<T> me, int maxLenth)
		{
			Debug.Assert(maxLenth >= 0);
			lock(me) {
				if (me.Count > maxLenth) me.RemoveRange(maxLenth, me.Count - maxLenth);
			}
		}
	}
}