using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Integer {
	public static class GetHashCodeAlgorithmExtention {
		[Pure, DebuggerStepThrough]
		public static int GetHashCodeAlgorithm(this int me, [NotNull] System.Collections.Generic.IEnumerable<int> collection) {
			return collection.Aggregate(me, (a, x) => unchecked ((a * 397) ^ x));
		}
		
		[Pure, DebuggerStepThrough]
		public static int GetHashCodeAlgorithm(this int me, [NotNull] params int[] collection) {
			return me.GetHashCodeAlgorithm((System.Collections.Generic.IEnumerable<int>) collection);
		}
	}
}