using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Delegates;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic
{
	public static class SetExtention {
		[DebuggerStepThrough]
		private static bool DefaultUseExisingValue<TValue>(TValue value)
		{
			return true;
		}
		
		[DebuggerStepThrough]
		public static void Set<TKey, TValue>(this System.Collections.Generic.IDictionary<TKey, TValue> me, TKey key, TValue value, MergeWithExisting<TValue> mergeWithExistingValue)
		{
			me.Set(key, value, DefaultUseExisingValue, mergeWithExistingValue);
		}

		[DebuggerStepThrough]
		public static void Set<TKey, TValue>(this System.Collections.Generic.IDictionary<TKey, TValue> me, TKey key, TValue value, System.Func<TValue, bool> useExistingValue, MergeWithExisting<TValue> mergeWithExistingValue)
		{
			TValue currentValue;
			lock(me)
				me[key] = !me.TryGetValue(key, out currentValue) || !useExistingValue(currentValue)
				          	? value
				          	: mergeWithExistingValue(currentValue, value);
		}
	}
}