using System;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable
{
	public static class SequenceEqualExtention
	{
		[DebuggerStepThrough]
		public static bool SequenceEqual([NotNull] this System.Collections.IEnumerable me, [NotNull] System.Collections.IEnumerable other) {
			return me.GetEnumerator().SequenceEqual(other.GetEnumerator());
		}
		
		[DebuggerStepThrough]
		public static bool SequenceEqual([NotNull] this System.Collections.IEnumerable me, [NotNull] System.Collections.IEnumerable other, [NotNull] Func<object, object, bool> equalsFunc) {
			return me.GetEnumerator().SequenceEqual(other.GetEnumerator(), equalsFunc);
		}
	}
}