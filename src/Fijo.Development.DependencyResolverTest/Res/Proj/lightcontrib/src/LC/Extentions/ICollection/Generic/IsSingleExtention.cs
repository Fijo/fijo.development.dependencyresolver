using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic {
	public static class IsSingleExtention {
		[DebuggerStepThrough]
		public static bool IsSingle<T>(this ICollection<T> me) {
			return me.Count == 1;
		}
	}
}