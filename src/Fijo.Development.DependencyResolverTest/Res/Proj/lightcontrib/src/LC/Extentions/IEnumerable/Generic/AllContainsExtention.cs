using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	public static class AllContainsExtention
	{
		[DebuggerStepThrough]
		public static bool AllContains<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<T> collection)
		{
			return me.All(collection.Contains);
		}
	}
}