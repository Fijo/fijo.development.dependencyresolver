using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class IndexesOfExtention {
		[Pure, DebuggerStepThrough]
		public static IEnumerable<int> IndexesOf<TSource>([NotNull] this IEnumerator<TSource> me, TSource item, [CanBeNull] IEqualityComparer equalityComparer) {
			return InternalGetIndexes(me, item, equalityComparer ?? GetDefaultEqualityComparer<TSource>());
		}

		[Pure, DebuggerStepThrough]
		public static IEnumerable<int> IndexesOf<TSource>([NotNull] this IEnumerator<TSource> me, TSource item) {
			return InternalGetIndexes(me, item, GetDefaultEqualityComparer<TSource>());
		}

		[Pure, DebuggerStepThrough]
		private static EqualityComparer<TSource> GetDefaultEqualityComparer<TSource>() {
			return EqualityComparer<TSource>.Default;
		}

		[Pure, DebuggerStepThrough]
		private static IEnumerable<int> InternalGetIndexes<TSource>([NotNull] IEnumerator<TSource> me, TSource item, [NotNull] IEqualityComparer equalityComparer) {
			for(var index = 0; me.MoveNext(); index++)
				if (equalityComparer.Equals(me.Current, item))
					yield return index;
		}
	}
}