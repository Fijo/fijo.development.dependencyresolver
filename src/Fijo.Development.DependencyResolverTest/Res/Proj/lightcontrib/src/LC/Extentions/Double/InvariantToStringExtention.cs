using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double
{
	public static class InvariantToStringExtention {
		[Pure, DebuggerStepThrough, Description("Converts a double to string using invariant culture settings")]
		public static string InvariantToString(this double me)
		{
			return me.ToString(CultureInfo.InvariantCulture);
		}
	}
}