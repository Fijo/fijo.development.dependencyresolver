using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class SecondExtention {
		[DebuggerStepThrough]
		public static TSource Second<TSource>(this IEnumerator<TSource> me) {
			return me.IndexTh(2);
		}
	}
}