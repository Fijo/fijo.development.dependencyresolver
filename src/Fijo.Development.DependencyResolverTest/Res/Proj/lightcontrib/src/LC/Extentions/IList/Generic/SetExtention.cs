using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic {
	public static class SetExtention {
		// TSource : class - to be shure, that i have not to use default(TSource) and that this can be a value and so not undefined
		[DebuggerStepThrough]
		public static void Set<TSource>([NotNull] this IList<TSource> me, int index, TSource value) where TSource : class {
			var count = me.Count;
			while(count++ < index) me.Add(null);
			me[index] = value;
		}
	}
}