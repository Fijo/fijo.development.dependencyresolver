using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic.Main
{
	public static class ShiftExtention
	{
		[DebuggerStepThrough]
		public static T Shift<T>([NotNull] this IList<T> me) {
			lock (me) {
				var removedEntry = me[0];
				me.RemoveAt(0);
				return removedEntry;
			}
		}
	}
}
