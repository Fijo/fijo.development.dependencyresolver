using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.IntoCollection;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	// ToDo May Check and Improve the performance
	public static class RecursiveEachSelectExtention
	{
		[DebuggerStepThrough]
		public static IEnumerable<IEnumerable<TSource>> RecursiveEachSelect<TSource>([NotNull] this IEnumerable<IEnumerable<TSource>> me)
		{
			var collection = me.ToList();
			if (collection.None()) return Enumerable.Empty<IEnumerable<TSource>>();
			return collection.Skip(1).Aggregate(collection.First().Select(x => x.IntoEnumerable()), RecursiveEachSelectTrasform);
		}
		
		[DebuggerStepThrough]
		private static IEnumerable<IEnumerable<TSource>> RecursiveEachSelectTrasform<TSource>([NotNull] IEnumerable<IEnumerable<TSource>> last, IEnumerable<TSource> entry) {
			#region PreCondition
			if (entry == null) throw new ArgumentNullException("entry");
			if (last == null) throw new ArgumentNullException("last");
			#endregion

			return last.SelectMany(x => entry, (x, subEntry) => x.AddReturn(subEntry));
		}
	}
}