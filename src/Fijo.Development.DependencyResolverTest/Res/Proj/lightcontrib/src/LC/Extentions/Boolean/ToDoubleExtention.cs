using System;
using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Boolean
{
    public static class ToDoubleExtention
    {
		[Pure, DebuggerStepThrough, Description("Converts a bool to double")]
        public static double ToDouble(this bool me)
        {
			return Convert.ToDouble(me);
        }
    }
}