using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
	public static class ConcatExtention
	{
		[DebuggerStepThrough]
		public static IEnumerable<T> Concat<T>([NotNull] this IEnumerable<T> me, [NotNull] IEnumerable<IEnumerable<T>> collections)
		{
			return collections.Aggregate(me, Enumerable.Concat);
		}

		[DebuggerStepThrough]
		public static IEnumerable<T> Concat<T>([NotNull] this IEnumerable<T> me, params IEnumerable<T>[] collections) {
			#region PreCondition
			if (collections == null) throw new ArgumentNullException("collections");
			#endregion
			return me.Concat((IEnumerable<IEnumerable<T>>)collections);
		}
	}
}
