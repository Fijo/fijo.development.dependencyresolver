using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Fijo.Infrastructure.Model.Pair;
using FijoCore.Infrastructure.LightContrib.Extentions.IList.Generic;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	// ToDo finish and test this
	public static class ParallelizableAggregateExtention {
		public static TAccumulate ParallelizableAggregate<TSource, TSelected, TAccumulate>(this IEnumerable<TSource> me, TAccumulate seed, Func<TAccumulate, TSource, TSelected, TAccumulate> func, Func<TSource, TSelected> selector) {
			var itemPairCache = new List<Pair<TSource, TSelected>>();
			var waitOnIndex = -1;
			var thread = Thread.CurrentThread;
			var tmpWaitOnIndex = waitOnIndex;
			var selectionTask = new Task(() => Parallel.ForEach(me, (item, state, index) => {
			                                                        	var selected = selector(item);
			                                                        	lock (itemPairCache)
			                                                        		itemPairCache.Set((int) index,
			                                                        		                  new Pair<TSource, TSelected>(item,
			                                                        		                                               selected));
			                                                        	if (index == tmpWaitOnIndex) thread.Interrupt();
			                                                        }));

			var result = seed;
			var aggregateIndex = 0;
			while (!selectionTask.IsCompleted && aggregateIndex == itemPairCache.Count) {
				HandleWait(CheckWait(aggregateIndex, itemPairCache, out waitOnIndex));

				result = GetCurrentResult(result, func, itemPairCache, aggregateIndex++);
			}
			return result;
		}

		private static TAccumulate GetCurrentResult<TSource, TSelected, TAccumulate>(TAccumulate seed, Func<TAccumulate, TSource, TSelected, TAccumulate> func, IList<Pair<TSource, TSelected>> itemPairCache, int aggregateIndex) {
			var itemPair = itemPairCache[aggregateIndex];
			return func(seed, itemPair.First, itemPair.Second);
		}

		private static void HandleWait(bool mustWait) {
			if (mustWait)
				try {
					Thread.Sleep(int.MaxValue);
				}
				catch (ThreadInterruptedException) {}
		}

		private static bool CheckWait<TSource, TSelected>(int aggregateIndex, IList<Pair<TSource, TSelected>> itemPairCache, out int waitOnIndex) {
			bool mustWait;
			lock (itemPairCache) {
				mustWait = !CurrentIndexExist(itemPairCache, aggregateIndex);
				waitOnIndex = aggregateIndex;
			}
			return mustWait;
		}

		private static bool CurrentIndexExist<T>(IList<T> itemPairCache, int index) where T : class {
			return index < itemPairCache.Count && Equals(itemPairCache[index], null);
		}
	}
}