using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic {
	public static class SelectSkipNullExtention {
		[DebuggerStepThrough]
		public static IEnumerable<TResult> SelectSkipNull<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, TResult> selector) {
			return me.Select(selector).Where(x => Equals(null, x));
		}
		
		[DebuggerStepThrough]
		public static IEnumerable<TResult> SelectSkipNull<TSource, TResult>([NotNull] this IEnumerable<TSource> me, [NotNull] Func<TSource, int, TResult> selector) {
			return me.Select(selector).Where(x => Equals(null, x));
		}
	}
}