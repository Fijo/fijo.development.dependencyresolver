using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic {
	public static class NoneExtention {
		[DebuggerStepThrough]
		public static bool None<T>(this ICollection<T> me) {
			return me.Count == 0;
		}
	}
}