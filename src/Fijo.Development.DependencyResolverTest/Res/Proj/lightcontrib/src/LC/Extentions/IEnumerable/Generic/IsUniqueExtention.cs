using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
    public static class IsUniqueExtention
    {
		[DebuggerStepThrough]
        public static bool IsUnique<T>([NotNull] this IEnumerable<T> me)
		{
			var collection = me.ToArray();
        	return collection.Distinct().Count() == collection.Count();
        }
		
		[DebuggerStepThrough]
    	public static bool IsUnique<T, TSelected>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, TSelected> selector) {
			return me.Select(selector).IsUnique();
		}
		
		[DebuggerStepThrough]
		public static bool IsUnique<T, TSelected>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, int, TSelected> selector) {
			return me.Select(selector).IsUnique();
		}

		// ToDo performance intensive - improve new FuncEqualityComparer(equalsFunc)
		[DebuggerStepThrough]
		public static bool IsUnique<T>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, T, bool> equalsFunc)
		{
			var collection = me.ToList();
			return collection.Distinct(new FuncEqualityComparer<T>(equalsFunc)).Count() == collection.Count;
		}
		
		[DebuggerStepThrough]
		public static bool IsUnique<T, TSelected>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, TSelected> selector, [NotNull] Func<TSelected, TSelected, bool> equalsFunc) {
			return me.Select(selector).IsUnique(equalsFunc);
		}
		
		[DebuggerStepThrough]
		public static bool IsUnique<T, TSelected>([NotNull] this IEnumerable<T> me, [NotNull] Func<T, int, TSelected> selector, [NotNull] Func<TSelected, TSelected, bool> equalsFunc) {
			return me.Select(selector).IsUnique(equalsFunc);
		}
    }
}