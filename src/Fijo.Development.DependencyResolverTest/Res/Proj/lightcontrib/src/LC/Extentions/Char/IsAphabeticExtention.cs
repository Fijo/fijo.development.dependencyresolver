using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Char
{
	public static class IsAphabeticExtention
	{
		[NotNull]
        private static readonly IList<char> AlphabeticChars = new List<char>("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_".ToCharArray());
		
		[Pure, DebuggerStepThrough, Description("Returns if if the given char is one of these �abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_�. Otherwise it will return false.")]
        public static bool IsAlphabetic([NotNull] this char? me)
        {
// ReSharper disable PossibleInvalidOperationException
            return ((char)me).IsAlphabetic();
// ReSharper restore PossibleInvalidOperationException
		}
		
		[Pure, DebuggerStepThrough, Description("Returns if if the given char is one of these �abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_�. Otherwise it will return false.")]
        public static bool IsAlphabetic(this char me)
		{
		    return AlphabeticChars.Contains(me);
		}
	}
}
