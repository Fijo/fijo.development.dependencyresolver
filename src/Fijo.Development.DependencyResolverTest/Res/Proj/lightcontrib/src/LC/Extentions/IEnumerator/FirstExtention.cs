using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class FirstExtention {
		[DebuggerStepThrough]
		public static TSource First<TSource>(this IEnumerator<TSource> me) {
			return me.IndexTh(1);
		}
	}
}