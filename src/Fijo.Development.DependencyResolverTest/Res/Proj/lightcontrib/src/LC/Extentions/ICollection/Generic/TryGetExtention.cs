using System.Diagnostics;
using System.Linq;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IDictionary.Generic
{
	public static class TryGetExtention
	{
		[DebuggerStepThrough]
		public static TValue TryGet<TKey, TValue>(this System.Collections.Generic.IDictionary<TKey, TValue> me, TKey key)
		{
			return me.TryGet(key, default(TValue));
		}

		[DebuggerStepThrough]
		public static TValue TryGet<TKey, TValue>(this System.Collections.Generic.IDictionary<TKey, TValue> me, TKey key,
		                                          TValue failValue)
		{
			TValue value;
			return me.TryGetValue(key, out value) ? value : failValue;
		}
	}

	// ToDo move this to the right place
	public static class WhereValueExtention
	{
		[DebuggerStepThrough]
		public static System.Collections.Generic.IEnumerable<TKey> WhereValue<TKey, TValue>(this System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey, TValue>> me, TValue needle) {
			return me.Where(x => x.Value.Equals(needle)).Select(x => x.Key);
		}
	}
}