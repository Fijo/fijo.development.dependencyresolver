using System.Collections.Generic;
using System.Diagnostics;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator {
	public static class ThirdExtention {
		[DebuggerStepThrough]
		public static TSource Third<TSource>(this IEnumerator<TSource> me) {
			return me.IndexTh(3);
		}
	}
}