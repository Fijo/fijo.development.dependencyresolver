using System;
using System.ComponentModel;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.Double
{
    public static class ToIntegerExtention
    {
		[Pure, DebuggerStepThrough, Description("Converts a double to int")]
        public static int ToInteger(this double me) {
			return (int) me;
		}
		
		[Pure, DebuggerStepThrough, Description("Converts a double to int, supporting rounding (if you wana use it)")]
        public static int ToInteger(this double me, bool round) {
			return !round
			       	? ToInteger(me)
			       	: Convert.ToInt32(me);
		}
    }
}