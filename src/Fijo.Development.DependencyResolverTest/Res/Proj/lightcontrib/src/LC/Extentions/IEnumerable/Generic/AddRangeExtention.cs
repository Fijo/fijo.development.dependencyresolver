﻿using System.Collections.Generic;
using System.Diagnostics;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic
{
    public static class AddRangeExtention
    {
		[DebuggerStepThrough]
		public static IEnumerable<T> AddRangeReturn<T>([NotNull] this IEnumerable<T> me, IEnumerable<T> collection) {
			return me.Concat(collection);
		}
    }
}