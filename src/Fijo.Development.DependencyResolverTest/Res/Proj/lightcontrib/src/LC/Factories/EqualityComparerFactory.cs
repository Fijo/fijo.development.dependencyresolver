using System;
using System.Collections;
using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Recursive;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Sequence;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Factories {
	public class EqualityComparerFactory : IEqualityComparerFactory {
		[Pure]
		public IEqualityComparer<T> Default<T>() {
			return EqualityComparer<T>.Default;
		}

		[Pure]
		public IEqualityComparer Default() {
			return EqualityComparer<object>.Default;
		}

		[Pure]
		public IEqualityComparer<T> Default<T>(Func<T, T, bool> @equals, Func<T, int> getHashCode = null) {
			return new FuncEqualityComparer<T>(equals, getHashCode);
		}

		[Pure]
		public IEqualityComparer<object> RecursiveSwitched<T>(IEqualityComparer<IEnumerable> recursiveComparer, IEqualityComparer<T> singleEqualityComparer, IEqualityComparer<IEnumerable<T>> genericRecursiveComparer = null) {
			return new RecursiveSwitchedEqualityComparer<T>(recursiveComparer, singleEqualityComparer, genericRecursiveComparer);
		}
		
		[Pure]
		public IEqualityComparer<IEnumerable<T>> Sequential<T>() {
			return new SequenceEqualityComparer<T>();
		}
		
		[Pure]
		public IEqualityComparer<IEnumerable<T>> Sequential<T>(Func<T, T, bool> @equals, Func<T, int> getHashCode = null) {
			return new FuncSequenceEqualityComparer<T>(equals, getHashCode);
		}
	}
}