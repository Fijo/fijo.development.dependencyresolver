using System.Collections.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Factories.Interface
{
	public interface IExecutedEnumerableFactory
	{
		ICollection<T> CreateExecutedEnumerable<T>([NotNull] ICollection<T> collection);
		ICollection<T> CreateExecutedEnumerable<T>([NotNull] IEnumerable<T> enumerable);
		ICollection<T> CreateExecutedEnumerable<T>(params T[] collection);
	}
}