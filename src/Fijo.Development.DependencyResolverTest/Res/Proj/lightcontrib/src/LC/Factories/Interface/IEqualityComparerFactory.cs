using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Factories.Interface {
	public interface IEqualityComparerFactory {
		[NotNull, Pure]
		IEqualityComparer<T> Default<T>();

		[NotNull, Pure]
		IEqualityComparer Default();

		[NotNull, Pure]
		IEqualityComparer<T> Default<T>([NotNull] Func<T, T, bool> @equals, [CanBeNull] Func<T, int> getHashCode = null);

		[NotNull, Pure]
		IEqualityComparer<object> RecursiveSwitched<T>([NotNull] IEqualityComparer<IEnumerable> recursiveComparer, [NotNull] IEqualityComparer<T> singleEqualityComparer, [CanBeNull] IEqualityComparer<IEnumerable<T>> genericRecursiveComparer = null);

		[NotNull, Pure]
		IEqualityComparer<IEnumerable<T>> Sequential<T>();

		[NotNull, Pure]
		IEqualityComparer<IEnumerable<T>> Sequential<T>([NotNull] Func<T, T, bool> @equals, [CanBeNull] Func<T, int> getHashCode = null);
	}
}