﻿using System.Collections.Generic;
using Fijo.Infrastructure.Model.Pair;
using Fijo.Infrastructure.Model.Pair.Interface;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace FijoCore.Infrastructure.LightContrib.Factories {
	public class PairFactory : IPairFactory {
		#region Pair
		public IPair<T1, T2> Pair<T1, T2>(T1 first, T2 second) {
			return new Pair<T1, T2>(first, second);
		}
		#endregion

		#region LeftRightPair
		public ILeftRightPair<TObject> LeftRightPair<TObject>(TObject left, TObject right) {
			return new LeftRightPair<TObject>(left, right);
		}
		#endregion

		#region ReplacePair
		public IReplacePair<TEntry> ReplacePair<TEntry>(TEntry needle, TEntry replacement) {
			return new ReplacePair<TEntry>(needle, replacement);
		}

		public IReplacePair<TNeelde, TReplacement> ReplacePair<TNeelde, TReplacement>(TNeelde needle, TReplacement replacement) {
			return new ReplacePair<TNeelde, TReplacement>(needle, replacement);
		}
		#endregion

		#region KeyValuePair
		public KeyValuePair<TKey, TValue> KeyValuePair<TKey, TValue>(TKey key, TValue value) {
			return new KeyValuePair<TKey, TValue>(key, value);
		}
		#endregion
	}
}