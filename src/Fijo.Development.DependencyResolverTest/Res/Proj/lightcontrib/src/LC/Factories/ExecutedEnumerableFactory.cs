using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Collection.Generic;

namespace FijoCore.Infrastructure.LightContrib.Factories
{
	public class ExecutedEnumerableFactory : IExecutedEnumerableFactory
	{
		public ICollection<T> CreateExecutedEnumerable<T>(ICollection<T> collection)
		{
			return new ExecutedEnumerable<T>(collection);
		}

		public ICollection<T> CreateExecutedEnumerable<T>(IEnumerable<T> enumerable)
		{
			return new ExecutedEnumerable<T>(enumerable.ToArray());
		}

		public ICollection<T> CreateExecutedEnumerable<T>(params T[] collection) {
			#region PreCondition
			Debug.Assert(collection != null, "collection != null");
			#endregion
			return new ExecutedEnumerable<T>(collection);
		}
	}
}