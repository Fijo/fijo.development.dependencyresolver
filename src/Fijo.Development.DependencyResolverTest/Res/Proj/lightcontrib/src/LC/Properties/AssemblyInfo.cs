﻿using System.Reflection;
using System.Runtime.InteropServices;

// Allgemeine Informationen über eine Assembly werden über die folgenden 
// Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
// die mit einer Assembly verknüpft sind.
[assembly: AssemblyTitle("FijoCore.Infrastructure.LightContrib")]
[assembly: AssemblyDescription("The most important base Extention, Services and so on, that are needed as ground base for the FijoCore, especially that they are available in the infrastructure of the FijoCore.")]

#region AssemblyEnableDefenition
[assembly: AssemblyConfiguration("@DynamicCusomConfiguration\n"
								+ "AssemblyEnableDefenitionAttibute::"
#if DEBUG
								+ "DEBUG|"
#endif
#if TRACE
								+ "TRACE|"
#endif
                                 )]
#endregion

[assembly: AssemblyCompany("Fijo")]
[assembly: AssemblyProduct("FijoCore.Infrastructure.LightContrib")]
[assembly: AssemblyCopyright("Copyright © Jonas Fischer <fijo.com@googlemail.com> 2012")]
[assembly: AssemblyTrademark("Fijo")]
[assembly: AssemblyCulture("")]

// Durch Festlegen von ComVisible auf "false" werden die Typen in dieser Assembly unsichtbar 
// für COM-Komponenten. Wenn Sie auf einen Typ in dieser Assembly von 
// COM zugreifen müssen, legen Sie das ComVisible-Attribut für diesen Typ auf "true" fest.
[assembly: ComVisible(false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
[assembly: Guid("f83aed86-726d-4105-8179-affd41e00269")]

// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion 
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
// übernehmen, indem Sie "*" eingeben:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
