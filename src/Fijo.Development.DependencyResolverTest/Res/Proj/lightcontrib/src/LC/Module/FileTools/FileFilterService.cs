using System.Collections.Generic;
using System.Linq;

namespace FijoCore.Infrastructure.LightContrib.Module.FileTools {
	public class FileFilterService {
		public IEnumerable<string> FilterByExt(IEnumerable<string> files, string ext) {
			return files.Where(file => HasExt(file, ext));
		}

		public bool HasExt(string file, string ext) {
			return file.ToLowerInvariant().EndsWith(string.Format(".{0}", ext.ToLowerInvariant()));
		}
	}
}