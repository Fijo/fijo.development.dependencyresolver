using System;
using System.Collections.Generic;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func {
	[Service]
	public class FuncSequenceEqualityComparer<T> : FuncEqualityComparerBase<IEnumerable<T>, T> {
		public FuncSequenceEqualityComparer([NotNull] Func<T, T, bool> equals, [CanBeNull] Func<T, int> getHashCode = null) : base(equals, getHashCode) {}

		public override bool Equals([NotNull] IEnumerable<T> x, [NotNull] IEnumerable<T> y) {
			return x.SequenceEqual(y, EqualsFunc);
		}

		public override int GetHashCode([NotNull] IEnumerable<T> obj) {
			return obj.GetSequenceHashCode(GetHashCodeFunc);
		}
	}
}