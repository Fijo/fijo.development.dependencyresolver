using System;
using System.ComponentModel;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface {
	public interface INumberService<TNumber> where TNumber : struct, IComparable, IConvertible {
		TNumber MaxValue { get; }
		TNumber MinValue { get; }
		TNumber ZeroValue { get; }
		[Description("if it is commarless")]
		bool IsInteger { get; }
		bool IsUnsigned { get; }
		bool HasZeroValue { get; }
		TNumber Convert(object value);
		[Pure] TNumber Sun(TNumber left, TNumber right);
		[Pure] TNumber Diff(TNumber left, TNumber right);
		[Pure] TNumber Times(TNumber left, TNumber right);
		[Pure] TNumber Divide(TNumber left, TNumber right);
		[Pure] TNumber Modulo(TNumber left, TNumber right);
	}
}