using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Module;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class ConfigurationConverterFinder : AlgorithmFinder<IConverter, TypeChangeDto> {}
}