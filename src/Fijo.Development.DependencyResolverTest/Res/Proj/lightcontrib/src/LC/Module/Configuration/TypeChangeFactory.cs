using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class TypeChangeFactory : ITypeChangeFactory {
		public TypeChangeDto Create(Type source, Type target) {
			return new TypeChangeDto(source, target);
		}
	}
}