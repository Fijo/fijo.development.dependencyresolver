using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public interface ITypeChangeFactory {
		[NotNull]
		TypeChangeDto Create([NotNull] Type source, [NotNull] Type target);
	}
}