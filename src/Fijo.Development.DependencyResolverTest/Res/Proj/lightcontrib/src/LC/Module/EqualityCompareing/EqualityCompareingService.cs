﻿using System;
using System.Data;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Equality.Equals;
using FijoCore.Infrastructure.LightContrib.Module.EqualityCompareing.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityCompareing
{
	[Service]
	public class EqualityComparingService<T> : IEqualityComparingService<T> {
		private readonly bool _allowNull;
		private readonly Func<T, dynamic>[] _compareProperties;

		public EqualityComparingService(bool allowNull, params Func<T, dynamic>[] compareProperties) {
			_allowNull = allowNull;
			_compareProperties = compareProperties;
		}

		#region Equals
		public bool Equals(T me, T obj) {
			return CheckNull(me, obj) && _compareProperties.All(func => EqualsExtention.Equals(me, obj, func));
		}

		private bool CheckNull(T me, T obj) {
			if (_allowNull) return !me.IsUndefined() || obj.IsUndefined();
			if(me.IsUndefined() || obj.IsUndefined()) throw new NoNullAllowedException();
			return true;
		}

		public new bool Equals(object me, object obj) {
			return me is T && Equals((T) me, obj);
		}
		
		public bool Equals(T me, object obj) {
			return obj is T && Equals(me, (T) obj);
		}
		#endregion
		
		#region GetHashCode
		public int GetHashCode(T obj) {
			if(CheckNull(obj)) return 0;
			return _compareProperties.Select(func => func(obj)).Aggregate(0, (current, property) => (current * 397) ^ (property != null ? property.GetHashCode() : 0));
		}

		private bool CheckNull(T me) {
			if (_allowNull) return me == null;
			if(me == null) throw new NoNullAllowedException();
			return false;
		}
		#endregion
	}
}
