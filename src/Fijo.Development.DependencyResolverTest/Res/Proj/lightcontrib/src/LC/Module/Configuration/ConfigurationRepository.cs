using System;
using System.Collections.Generic;
using System.IO;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Serialization;
using IO = System.IO;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class ConfigurationRepository : RepositoryBase<IDictionary<string, object>> {
		protected readonly string ConfigurationRelativeFilePath = string.Format("Properties{0}Config.json", IO.Path.DirectorySeparatorChar);
		protected readonly string ConfigurationFilePath;

		public ConfigurationRepository() {
			ConfigurationFilePath = IO.Path.Combine(Environment.CurrentDirectory, ConfigurationRelativeFilePath);
		}

		#region Overrides of RepositoryBase<KeyValuePair<string,object>>
		#region Overrides of RepositoryBase<IDictionary<string,object>>
		public override IDictionary<string, object> Get() {
			// ToDo use LazyInject
			var serializationFacade = Kernel.Resolve<ISerializationFacade>();
			var stream = GetFileStream();
			var res = serializationFacade.Deserialize<IDictionary<string, object>>(SerializationFormat.JSON, stream, Text.Encoding.UTF8);
			stream.Close();
			return res;
		}
		#endregion

		private FileStream GetFileStream() {
			if(!File.Exists(ConfigurationFilePath)) throw new FileNotFoundException(string.Format("Main Configuration File �{0}�", ConfigurationFilePath));
			return File.OpenRead(ConfigurationFilePath);
		}
		#endregion
	}
}