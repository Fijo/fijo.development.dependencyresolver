using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityCompareing.Interface {
	[Service]
	public interface IEqualityComparingService<T> {
		bool Equals(T me, T obj);
		bool Equals(object me, object obj);
		bool Equals(T me, object obj);
		int GetHashCode(T obj);
	}
}