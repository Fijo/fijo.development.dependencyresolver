using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using YAXLib;
using FijoCore.Infrastructure.LightContrib.Enums;
using IO = System.IO;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public class XMLSerialization : ISerialization {
		private readonly YAXSerializer _yaxSerializer = new YAXSerializer(typeof(object));
		private readonly IStreamService _streamService = Kernel.Resolve<IStreamService>();
		#region Implementation of ISerialization
		public void Serialize(object obj, IO.Stream stream, SerializationFormatting formatting, Text.Encoding encoding) {
			Serialize<object>(obj, stream, formatting, encoding);
		}

		public void Serialize<T>(T obj, IO.Stream stream, SerializationFormatting formatting, Text.Encoding encoding) {
			var streamWriter = _streamService.GetWriter(stream, encoding);
			_yaxSerializer.Serialize(obj, streamWriter);
		}

		public object Deserialize(IO.Stream stream, Text.Encoding encoding) {
			return _yaxSerializer.Deserialize(_streamService.ReadToString(stream, encoding));
		}

		public T Deserialize<T>(IO.Stream stream, Text.Encoding encoding) {
			return (T)Deserialize(stream, encoding);
		}
		#endregion
	}
}