﻿using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Sequence
{
	[Service]
	public class SequenceEqualityComparer<T> : IEqualityComparer<IEnumerable<T>>
	{
		#region Implementation of IEqualityComparer<in IEnumerable<T>>
		public bool Equals([NotNull] IEnumerable<T> x, [NotNull] IEnumerable<T> y)
		{
			return x.SequenceEqual(y);
		}

		public int GetHashCode([NotNull] IEnumerable<T> obj)
		{
			return obj.GetSequenceHashCode();
		}
		#endregion
	}
}
