using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.AlgorithmFinders.Interface;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Extentions.AlgorithmFinder;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class ConfigurationService : IConfigurationService {
		protected readonly IAlgorithmFinder<IConverter, TypeChangeDto> ConverterFinder = Kernel.Resolve<IAlgorithmFinder<IConverter, TypeChangeDto>>();
		protected readonly ITypeChangeFactory TypeChangeFactory = Kernel.Resolve<ITypeChangeFactory>();
		protected readonly IDictionary<string, object> Content;

		public ConfigurationService() {
			Content = Kernel.Resolve<IRepository<IDictionary<string, object>>>().Get();
		}

		public T Get<T>(string key) {
			object value;
			if(!Content.TryGetValue(key, out value)) throw new ConfigurationKeyNotFoundException(key, typeof(T));
			return GetConverted<T>(key, value);
		}

		private T GetConverted<T>(string key, object value) {
			var converter = ConverterFinder.Get(CreateTypeChangeDto<T>(value));
			var converted = converter.Convert(value);
			if (!(converted is T)) throw new ConfigurationInvalidValueTypeException(key, value.GetType(), typeof (T), value.ToString());
			return (T) converted;
		}

		private TypeChangeDto CreateTypeChangeDto<T>(object value) {
			return TypeChangeFactory.Create(value.GetType(), typeof(T));
		}
	}
}