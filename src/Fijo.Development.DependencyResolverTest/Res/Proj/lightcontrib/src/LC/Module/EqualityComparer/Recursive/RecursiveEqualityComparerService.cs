using System.Collections;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Reflection.Resolve;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerator;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Recursive {
	[Service]
	public class RecursiveEqualityComparerService : IRecursiveEqualityComparerService {
		public bool DefaultRecursiveSimpleCompare(object x, object y) {
			return DefaultRecursiveCompare(x, y);
		}

		public bool DefaultRecursiveSimpleCompareDeepness(object x, object y, int deepness) {
			return DefaultRecursiveCompare(x, y, deepness);
		}

		public bool DefaultRecursiveCompare(object x, object y, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true) {
			IEnumerator enumeratorX;
			IEnumerator enumeratorY;
			if (x.TryGetEnumerator(out enumeratorX) && y.TryGetEnumerator(out enumeratorY, genericEnumerable, enumerable, enumerator))
				return enumeratorX.RecursiveSequenceEqual(enumeratorY, genericEnumerable, enumerable, enumerator);
			return x.Equals(y);
		}

		public bool DefaultRecursiveCompare(object x, object y, int deepness, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true) {
			IEnumerator enumeratorX;
			IEnumerator enumeratorY;
			if (x.TryGetEnumerator(out enumeratorX) && y.TryGetEnumerator(out enumeratorY, genericEnumerable, enumerable, enumerator))
				return enumeratorX.RecursiveSequenceEqual(enumeratorY, deepness, genericEnumerable, enumerable, enumerator);
			return x.Equals(y);
		}
	}
}