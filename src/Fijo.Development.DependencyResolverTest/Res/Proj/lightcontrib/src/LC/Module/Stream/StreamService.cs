using System.IO;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Encoding;
using JetBrains.Annotations;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream {
	public class StreamService : IStreamService {
		// ToDo use LazyInject
		private IEncodingProvider EncodingProvider { get { return Kernel.Resolve<IEncodingProvider>(); } }
		public string ReadToString(System.IO.Stream stream, bool autodetect = false) {
			var streamReader = GetReader(stream, autodetect);
			return streamReader.ReadToEnd();
		}
		public string ReadToString(System.IO.Stream stream, Text.Encoding encoding) {
			var streamReader = GetReader(stream, encoding);
			return streamReader.ReadToEnd();
		}
		public StreamReader GetReader(System.IO.Stream stream, bool autodetect = false) {
			return autodetect
			       	? new StreamReader(stream, true)
			       	: GetReader(stream, null);
		}
		public StreamReader GetReader(System.IO.Stream stream, Text.Encoding encoding) {
			return new StreamReader(stream, MayGetDefault(encoding));
		}
		public StreamWriter GetWriter(System.IO.Stream stream) {
			return GetWriter(stream, null);
		}
		public StreamWriter GetWriter(System.IO.Stream stream, Text.Encoding encoding) {
			return new StreamWriter(stream, MayGetDefault(encoding));
		}

		[NotNull, Pure]
		private Text.Encoding MayGetDefault([CanBeNull] Text.Encoding encoding) {
			return encoding ?? EncodingProvider.GetDefault();
		}

		public void WriteToStream(System.IO.Stream stream, string str) {
			WriteToStream(stream, str, null);
		}
		public void WriteToStream(System.IO.Stream stream, string str, Text.Encoding encoding) {
			var streamWriter = GetWriter(stream, encoding);
			streamWriter.Write(str);
			streamWriter.Flush();
		}
	}
}