using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	public class ByteNumberService : NumberServiceBase<byte> {
		#region Implementation of INumberService<byte>
		public override byte MaxValue { get { return byte.MaxValue; } }
		public override byte MinValue { get { return byte.MinValue; } }
		public override bool IsInteger { get { return true; } }

		public override byte Sun(byte left, byte right) {
			return (byte) (left + right);
		}

		public override byte Diff(byte left, byte right) {
			return (byte) (left - right);
		}

		public override byte Times(byte left, byte right) {
			return (byte) (left * right);
		}

		public override byte Divide(byte left, byte right) {
			return (byte) (left / right);
		}

		public override byte Modulo(byte left, byte right) {
			return (byte) (left % right);
		}
		#endregion
	}
}