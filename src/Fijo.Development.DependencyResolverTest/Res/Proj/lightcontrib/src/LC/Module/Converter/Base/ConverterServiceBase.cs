﻿using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Base {
	public abstract class ConverterServiceBase<TType, TResult> {
		protected ICollection<TType> ConvertableTypes;
		private readonly IOut _out = Kernel.Resolve<IOut>();

		public bool TryConvert<T>(T obj, out TResult result) {
			return ConvertableTypes.Contains(GetType<T>())
			       	? _out.True(out result, GetConverted(obj))
			       	: _out.False(out result);
		}

		public TResult Convert<T>(T obj) {
			if (!ConvertableTypes.Contains(GetType<T>())) throw new NotConvertableException(GetNotConvertableMessage(GetType(), typeof (TType), typeof (TResult)));
			return GetConverted(obj);
		}

		private string GetNotConvertableMessage(System.Type classType, System.Type contextType, System.Type resultType) {
			return string.Format("Faild to convert using ´{0}´. Comparing by ´{1}´ - tring to covert to ´{2}´", classType.Name,
			                     contextType.Name, resultType.Name);
		}

		protected abstract TType GetType<T>();
		protected abstract TResult GetConverted<T>(T obj);
	}
}
