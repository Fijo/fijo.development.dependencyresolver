using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Number.Interface;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number {
	public class NumberConcat : INumberConcat {
		private readonly INumberServiceProvider _numberServiceProvider = Kernel.Resolve<INumberServiceProvider>();
		private readonly INumberServiceHelper _numberServiceHelper = Kernel.Resolve<INumberServiceHelper>();
		
		#region Concat
		[Pure]
		public TResult Concat<TSource, TResult>(TResult startValue = default(TResult), params TSource[] numbers) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return Concat((IEnumerable<TSource>)numbers, startValue);
		}

		[Pure]
		public TResult Concat<TSource, TResult>(IEnumerable<TSource> numbers, TResult startValue = default(TResult)) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible  {
			return Concat(numbers, _numberServiceProvider.Get<TSource>(), _numberServiceProvider.Get<TResult>(), startValue);
		}

		[Pure]
		public TResult Concat<TSource, TResult>(INumberService<TSource> sourceService, INumberService<TResult> resultService, TResult startValue = default(TResult), params TSource[] numbers) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return Concat(numbers, sourceService, resultService, startValue);
		}

		[Pure]
		public TResult Concat<TSource, TResult>(IEnumerable<TSource> numbers, INumberService<TSource> sourceService, INumberService<TResult> resultService, TResult startValue = default(TResult)) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return Concat(numbers, _numberServiceHelper.GetPossibilities(sourceService, resultService), sourceService, resultService, startValue);
		}

		[Pure]
		public TResult Concat<TSource, TResult>(TResult entryAddValue, TResult startValue = default(TResult), params TSource[] numbers) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return Concat<TSource, TResult>(numbers, entryAddValue, startValue);
		}

		[Pure]
		public TResult Concat<TSource, TResult>(IEnumerable<TSource> numbers, TResult entryAddValue, TResult startValue = default(TResult)) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return Concat(numbers, entryAddValue, _numberServiceProvider.Get<TSource>(), _numberServiceProvider.Get<TResult>(), startValue);
		}

		[Pure]
		public TResult Concat<TSource, TResult>(INumberService<TSource> sourceService, INumberService<TResult> resultService, TResult entryAddValue, TResult startValue = default(TResult), params TSource[] numbers) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return Concat(numbers, entryAddValue, sourceService, resultService, startValue);
		}

		[Pure]
		public TResult Concat<TSource, TResult>(IEnumerable<TSource> numbers, TResult entryAddValue, INumberService<TSource> sourceService, INumberService<TResult> resultService, TResult startValue = default(TResult)) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			Debug.Assert(sourceService.IsInteger, "sourceService.IsInteger");
			var currentMultiplier = (TResult) Convert.ChangeType(1, typeof (TResult));
			return numbers.Reverse().Aggregate(startValue, (current, number) => {
			    var numToHandle = (TResult) Convert.ChangeType(number, typeof (TResult));
				Debug.Assert(numToHandle.CompareTo(entryAddValue) < 0, "numToHandle.CompareTo(entryAddValue) < 0");
			    var valueToReturn = resultService.Sun(current, resultService.Times(numToHandle, currentMultiplier));
				currentMultiplier = resultService.Times(currentMultiplier, entryAddValue);
				return valueToReturn;
			});
		}
		#endregion
		
		#region Split
		[Pure]
		public IEnumerable<TResult> Split<TSource, TResult>(TSource number, int length) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return Split(number, length, _numberServiceProvider.Get<TSource>(), _numberServiceProvider.Get<TResult>());
		}

		[Pure]
		public IEnumerable<TResult> Split<TSource, TResult>(TSource number, int length, INumberService<TSource> sourceService, INumberService<TResult> resultService) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return Split(number, _numberServiceHelper.GetPossibilities(resultService, sourceService), length, sourceService, resultService);
		}

		[Pure]
		public IEnumerable<TResult> Split<TSource, TResult>(TSource number, TSource entryMaxValue, int length) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			return Split(number, entryMaxValue, length, _numberServiceProvider.Get<TSource>(), _numberServiceProvider.Get<TResult>());
		}

		[Pure]
		public IEnumerable<TResult> Split<TSource, TResult>(TSource number, TSource entryMaxValue, int length, INumberService<TSource> sourceService, INumberService<TResult> resultService) where TSource : struct, IComparable, IConvertible where TResult : struct, IComparable, IConvertible {
			Debug.Assert(sourceService.IsInteger, "sourceService.IsInteger");
			var currentMultiplier = sourceService.Convert(1);
			var currentNumber = sourceService.Convert(number);
			return Enumerable.Range(0, length).Select(x => {
               	var lastMultiplier = currentMultiplier;
				currentMultiplier = sourceService.Times(currentMultiplier, entryMaxValue);
				var rest = sourceService.Modulo(currentNumber, currentMultiplier);
				currentNumber = sourceService.Diff(currentNumber, rest);
				return resultService.Convert(sourceService.Divide(rest, lastMultiplier));
			}).Reverse();
		}
		#endregion
	}
}