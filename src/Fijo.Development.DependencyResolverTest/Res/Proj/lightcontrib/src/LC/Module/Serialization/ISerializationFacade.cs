using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	[Facade]
	public interface ISerializationFacade {
		void Serialize(SerializationFormat format, object obj, [NotNull] System.IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null);
		void Serialize<T>(SerializationFormat format, T obj, [NotNull] System.IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null);
		string Serialize(SerializationFormat format, object obj, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null);
		string Serialize<T>(SerializationFormat format, T obj, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null);
		object Deserialize(SerializationFormat format, [NotNull] System.IO.Stream stream, [CanBeNull] Text.Encoding encoding = null);
		T Deserialize<T>(SerializationFormat format, [NotNull] System.IO.Stream stream, [CanBeNull] Text.Encoding encoding = null);
		object Deserialize(SerializationFormat format, string source, [CanBeNull] Text.Encoding encoding = null);
		T Deserialize<T>(SerializationFormat format, string source, [CanBeNull] Text.Encoding encoding = null);
	}
}