using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	public class IntNumberService : NumberServiceBase<int> {
		#region Implementation of INumberService<byte>
		public override int MaxValue { get { return int.MaxValue; } }
		public override int MinValue { get { return int.MinValue; } }
		public override bool IsInteger { get { return true; } }

		public override int Sun(int left, int right) {
			return left + right;
		}

		public override int Diff(int left, int right) {
			return left - right;
		}

		public override int Times(int left, int right) {
			return left * right;
		}

		public override int Divide(int left, int right) {
			return left / right;
		}

		public override int Modulo(int left, int right) {
			return left % right;
		}
		#endregion
	}
}