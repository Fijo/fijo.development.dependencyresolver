using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	public class FloatNumberService : NumberServiceBase<float> {
		#region Implementation of INumberService<byte>
		public override float MaxValue { get { return float.MaxValue; } }
		public override float MinValue { get { return float.MinValue; } }
		public override bool IsInteger { get { return false; } }

		public override float Sun(float left, float right) {
			return left + right;
		}

		public override float Diff(float left, float right) {
			return left - right;
		}

		public override float Times(float left, float right) {
			return left * right;
		}

		public override float Divide(float left, float right) {
			return left / right;
		}

		public override float Modulo(float left, float right) {
			return left % right;
		}
		#endregion
	}
}