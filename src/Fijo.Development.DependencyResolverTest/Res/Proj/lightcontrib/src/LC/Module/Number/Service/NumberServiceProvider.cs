using System;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service {
	public class NumberServiceProvider : INumberServiceProvider {
		public INumberService<TNumber> Get<TNumber>() where TNumber : struct, IComparable, IConvertible {
			return Kernel.Resolve<INumberService<TNumber>>();
		}
	}
}