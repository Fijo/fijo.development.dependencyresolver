using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Interface {
	public interface INumberServiceProvider {
		[NotNull]
		INumberService<TNumber> Get<TNumber>() where TNumber : struct, IComparable, IConvertible;
	}
}