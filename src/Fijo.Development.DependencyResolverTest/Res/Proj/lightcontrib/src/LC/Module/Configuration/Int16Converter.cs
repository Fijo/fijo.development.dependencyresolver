namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class Int16Converter : ConverterBase<long, short> {
		#region Overrides of ConverterBase<long,int>
		public override short Convert(long source) {
			return System.Convert.ToInt16(source);
		}
		#endregion
	}
}