using System;
using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Module.Cache.Base
{
	public abstract class CacheBase<TKey, TStore>
    {
		private readonly Dictionary<TKey, TStore> _objects = new Dictionary<TKey, TStore>();

		private TObj CreateBase<TObj>()
			where TObj : class, TStore
		{
			return (TObj) Create<TObj>();
		}

		protected abstract TStore Create<TObj>()
			where TObj : class, TStore;

		protected abstract TKey ConvertKey(Type type);

		private TObj Add<T, TObj>(TKey key, TObj myObject)
			where T : class
			where TObj : class, TStore
        {
			_objects.Add(key, myObject);
        	return myObject;
        }

		private TObj Resolve<TObj>(TKey key) where TObj : class, TStore
		{
			TStore value;
			_objects.TryGetValue(key, out value);
			return (TObj) value;
		}

		public TObj Get<T, TObj>()
			where T : class
			where TObj : class, TStore {
			var key = ConvertKey(typeof (T));
			lock(_objects) return Resolve<TObj>(key) ?? Add<T, TObj>(key, CreateBase<TObj>());
		}
    }
}