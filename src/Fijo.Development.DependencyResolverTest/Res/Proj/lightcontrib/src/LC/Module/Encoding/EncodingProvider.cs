﻿using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Encoding {
	public class EncodingProvider : IEncodingProvider {
		private readonly Text.Encoding _defaultEncoding;

		public EncodingProvider() {
			var configurationService = Kernel.Resolve<IConfigurationService>();
			_defaultEncoding = Text.Encoding.GetEncoding(configurationService.Get<string>("Fijo.Infrastructure.LightContrib.Module.Encoding.DefaultEncoding"));
		}

		public Text.Encoding GetDefault() {
			return _defaultEncoding;
		}
	}
}