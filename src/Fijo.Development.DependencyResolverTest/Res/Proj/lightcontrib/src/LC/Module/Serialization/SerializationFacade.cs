using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using IO = System.IO;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public class SerializationFacade : ISerializationFacade {
		private readonly ISerializationService _serializationService = Kernel.Resolve<ISerializationService>();
		private readonly ISerializationProvider _serializationProvider = Kernel.Resolve<ISerializationProvider>();
		
		public void Serialize(SerializationFormat format, object obj, IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null) {
			_serializationService.Serialize(_serializationProvider.Get(format), obj, stream, formatting, encoding);
		}
		
		public void Serialize<T>(SerializationFormat format, T obj, IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null) {
			_serializationService.Serialize(_serializationProvider.Get(format), obj, stream, formatting, encoding);
		}

		public string Serialize(SerializationFormat format, object obj, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null) {
			return _serializationService.Serialize(_serializationProvider.Get(format), obj, formatting, encoding);
		}

		public string Serialize<T>(SerializationFormat format, T obj, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null) {
			return _serializationService.Serialize(_serializationProvider.Get(format), obj, formatting, encoding);
		}

		public object Deserialize(SerializationFormat format, IO.Stream stream, Text.Encoding encoding = null) {
			return _serializationService.Deserialize(_serializationProvider.Get(format), stream, encoding);
		}

		public T Deserialize<T>(SerializationFormat format, IO.Stream stream, Text.Encoding encoding = null) {
			return _serializationService.Deserialize<T>(_serializationProvider.Get(format), stream, encoding);
		}

		public object Deserialize(SerializationFormat format, string source, Text.Encoding encoding = null) {
			return _serializationService.Deserialize(_serializationProvider.Get(format), source, encoding);
		}

		public T Deserialize<T>(SerializationFormat format, string source, Text.Encoding encoding = null) {
			return _serializationService.Deserialize<T>(_serializationProvider.Get(format), source, encoding);
		}
	}
}