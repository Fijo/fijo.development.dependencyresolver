using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public class SerializationProvider : ISerializationProvider {
		private readonly IDictionary<SerializationFormat, ISerialization> _serializer;

		public SerializationProvider() {
			_serializer = Kernel.Resolve<IRepository<IDictionary<SerializationFormat, ISerialization>>>().Get();
		}

		public ISerialization Get(SerializationFormat format) {
			ISerialization serializer;
			if(!_serializer.TryGetValue(format, out serializer)) throw new NotSupportedException(string.Format("SerializationFormat {0}", Enum.GetName(typeof (SerializationFormat), format)));
			return serializer;
		}
	}
}