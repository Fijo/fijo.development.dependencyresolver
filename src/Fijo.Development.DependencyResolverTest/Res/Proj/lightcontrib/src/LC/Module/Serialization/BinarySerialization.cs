using System.Runtime.Serialization.Formatters.Binary;
using FijoCore.Infrastructure.LightContrib.Enums;
using IO = System.IO;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public class BinarySerialization : ISerialization {
		private readonly BinaryFormatter _binaryFormatter = new BinaryFormatter();
		#region Implementation of ISerialization
		public void Serialize(object obj, IO.Stream stream, SerializationFormatting formatting, Text.Encoding encoding) {
			Serialize<object>(obj, stream, formatting, encoding);
		}

		public void Serialize<T>(T obj, IO.Stream stream, SerializationFormatting formatting, Text.Encoding encoding) {
			_binaryFormatter.Serialize(stream, obj);
		}

		public object Deserialize(IO.Stream stream, Text.Encoding encoding) {
			return _binaryFormatter.Deserialize(stream);
		}

		public T Deserialize<T>(IO.Stream stream, Text.Encoding encoding) {
			return (T)Deserialize(stream, encoding);
		}
		#endregion
	}
}