using System;
using FijoCore.Infrastructure.LightContrib.Module.Cache.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Cache
{
	public abstract class Cache : CacheBase<Type, object>
	{
		protected override Type ConvertKey(Type type)
		{
			return type;
		}

		protected override dynamic Create<TObj>()
		{
			throw new NotImplementedException();
		}
	}
}