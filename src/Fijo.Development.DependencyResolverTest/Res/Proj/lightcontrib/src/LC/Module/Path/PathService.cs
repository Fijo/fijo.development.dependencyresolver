using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using FijoCore.Infrastructure.LightContrib.Extentions.Char;
using JetBrains.Annotations;
using IO = System.IO;

namespace FijoCore.Infrastructure.LightContrib.Module.Path {
	public class PathService : IPathService {
		[Pure, About("CorrectStringPathForComparing"), Description("don�t ignore microsoft completely to produce no time intense framework bugs")]
		protected string CorrectPath(string path) {
			#if UseCaseInsensitivePathes
			return path.ToLowerInvariant();
			#else
			return path;
			#endif
		}

		[Pure]
		public bool IsInPath(string path, string basePath) {
			var absolutePath = GetCorrectedFullPathNoPathSeperatorAtEnd(path);
			var absoluteBasePath = GetCorrectedFullPathNoPathSeperatorAtEnd(basePath);
			return InternalIsInPath(absolutePath, absoluteBasePath);
		}

		[Pure]
		private bool InternalIsInPath(string absolutePath, string absoluteBasePath) {
			return absolutePath.StartsWith(absoluteBasePath);
		}

		[Pure]
		private string GetCorrectedFullPath(string path) {
			return CorrectPath(IO.Path.GetFullPath(path));
		}

		[Pure]
		private string GetCorrectedFullPathNoPathSeperatorAtEnd(string path) {
			var correctedPath = GetCorrectedFullPath(path);
			return correctedPath.Last() == IO.Path.DirectorySeparatorChar
			       	? path.Substring(0, path.Length - 1)
			       	: path;
		}

		[Pure]
		public string GetPathRelativeTo(string path, string basePath) {
			var absolutePath = GetCorrectedFullPathNoPathSeperatorAtEnd(path);
			var absoluteBasePath = GetCorrectedFullPathNoPathSeperatorAtEnd(basePath);
			if(!InternalIsInPath(absolutePath, absoluteBasePath)) throw new PathMustBeInBasePathException(path, basePath);
			var length = absolutePath.Length;
			var startIndex = absoluteBasePath.Length;
			Debug.Assert(length >= startIndex, "absolutePath.Length >= absoluteBasePath.Length");
			if(startIndex == length) return GetRelativeCurrentPath();
			Debug.Assert(startIndex + 1 < length, "startIndex +1 < absolutePath.Length");
			return absolutePath.Substring(startIndex +1);
		}

		private readonly string DirectorySeparatorString = IO.Path.DirectorySeparatorChar.InvariantToString();

		public void MayCreateParentFolders(string path) {
			var parts = path.Split(IO.Path.DirectorySeparatorChar);
			var count = parts.Count();
			if(count <= 1) return;
			foreach(var dict in Enumerable.Range(2, count -1).Reverse().Select(x => String.Join(DirectorySeparatorString, parts.Take(x).ToArray())).TakeWhile(x => !Directory.Exists(x)).Reverse())
				Directory.CreateDirectory(dict);
		}

		[Pure]
		private string GetRelativeCurrentPath() {
			return string.Empty;
		}
		
		[Pure]
		public bool IsRelative(string path) {
			return !IsAbsolute(path);
		}

		[Pure]
		public bool IsAbsolute(string path) {
			#if !PLATFORM_UNIX
			return ContainsVolumePattern(path);
			#else
			return path.FirstOrDefault() == Path.DirectorySeparatorChar;
			#endif
		}

		[Pure]
		public bool ContainsVolumePattern(string path) {
			#if !PLATFORM_UNIX
			int skiped;
			return InternalGetVolumePattern(path, out skiped).FirstOrDefault() == IO.Path.VolumeSeparatorChar;
			#else
			return false;
			#endif
		}
		
		[Pure]
		public string GetVolumePatternInPath(string path) {
			#if !PLATFORM_UNIX
			int skiped;
			var array = InternalGetVolumePattern(path, out skiped).ToArray();
			if(array.FirstOrDefault() != IO.Path.VolumeSeparatorChar) return string.Empty;
			#if PLATFORM_WINDOWS
			if(array.Count() > 1 && array[1] == IO.Path.DirectorySeparatorChar) return InternalGetVolumePatternUntilIndex(path, skiped + 2);
			#endif
			return InternalGetVolumePatternUntilIndex(path, skiped + 1);
			#else
			return false;
			#endif
		}
		
		[Pure]
		public string GetVolumeNameFromPattern(string volumePattern) {
			#if !PLATFORM_UNIX
			if(string.IsNullOrEmpty(volumePattern)) throw new ArgumentException("can not be null or empty", "volumePattern");
			#if !PLATFORM_WINDOWS
			Debug.Assert(volumePattern.EndsWith(new string(new[] {Path.VolumeSeparatorChar})));
			return InternalGetVolumePatternUntilIndex(volumePattern, volumePattern.Length -1);
			#else
			Debug.Assert(volumePattern.EndsWith(new string(new[] {IO.Path.VolumeSeparatorChar, IO.Path.DirectorySeparatorChar})) || volumePattern.EndsWith(new string(new[] {IO.Path.VolumeSeparatorChar})));
			return InternalGetVolumePatternUntilIndex(volumePattern, volumePattern.Length - (volumePattern.Last() == IO.Path.DirectorySeparatorChar ? 2 : 1));
			#endif
			#else
			throw new PlatformNotSupportedException();
			#endif
		}

		[Pure]
		private string InternalGetVolumePatternUntilIndex(string pathOrPattern, int nameEndIndex) {
			#region PreCondition
			Debug.Assert(pathOrPattern.Length >= nameEndIndex, "pathOrPattern.Length >= nameEndIndex");
			#endregion
			#if !PLATFORM_UNIX
			return pathOrPattern.Substring(0, nameEndIndex);
			#else
			throw new PlatformNotSupportedException();
			#endif
		}

		[Pure]
		private IEnumerable<char> InternalGetVolumePattern(string path, out int skiped) {
			#if !PLATFORM_UNIX
			var internalSkiped = 0;
			var enumerable = path.SkipWhile(x => {
				var res = IsCharNotVolumeCharSeperatorOrDictionarySeperator(x);
				if(res) internalSkiped++;
				return res;
			}).ToArray();
			skiped = internalSkiped;
			return enumerable;
			#else
			throw new PlatformNotSupportedException();
			#endif
		}

		[Pure]
		private bool IsCharNotVolumeCharSeperatorOrDictionarySeperator(char c) {
			#if !PLATFORM_UNIX
			return c != IO.Path.VolumeSeparatorChar && c != IO.Path.DirectorySeparatorChar;
			#else
			return c != Path.DirectorySeparatorChar;
			#endif
		}

		private const char CygWinDictionarySeperatorChar = '/';

		// ToDo test this
		[Pure, About("path is for file/ dictionary path")]
		public string PathToCygWinPath(string path) {
			var volumePart = GetVolumePatternInPath(path);
			if(volumePart == string.Empty) return path;
			var volumeName = GetVolumeNameFromPattern(volumePart);
			Debug.Assert(!string.IsNullOrEmpty(volumeName));
			return string.Format("{0}{1}{0}{2}", CygWinDictionarySeperatorChar, volumeName, path.Substring(volumePart.Length).Replace(IO.Path.DirectorySeparatorChar, CygWinDictionarySeperatorChar));
		}
	}
}