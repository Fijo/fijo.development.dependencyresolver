using System.IO;
using JetBrains.Annotations;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Stream {
	public interface IStreamService {
		string ReadToString([NotNull] System.IO.Stream stream, bool autodetect = false);
		string ReadToString([NotNull] System.IO.Stream stream, [CanBeNull] Text.Encoding encoding);

		[NotNull]
		StreamReader GetReader([NotNull] System.IO.Stream stream, bool autodetect = false);

		[NotNull]
		StreamReader GetReader([NotNull] System.IO.Stream stream, [CanBeNull] Text.Encoding encoding);

		[NotNull]
		StreamWriter GetWriter([NotNull] System.IO.Stream stream);

		[NotNull]
		StreamWriter GetWriter([NotNull] System.IO.Stream stream, [CanBeNull] Text.Encoding encoding);

		void WriteToStream([NotNull] System.IO.Stream stream, string str);
		void WriteToStream([NotNull] System.IO.Stream stream, string str, [CanBeNull] Text.Encoding encoding);
	}
}