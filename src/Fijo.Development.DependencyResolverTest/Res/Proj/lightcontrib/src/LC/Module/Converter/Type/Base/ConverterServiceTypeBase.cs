using FijoCore.Infrastructure.LightContrib.Module.Converter.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Type.Base {
	public abstract class ConverterServiceTypeBase<TResult> : ConverterServiceBase<System.Type, TResult> {
		protected override System.Type GetType<T>() {
			return typeof (T);
		}
	}
}