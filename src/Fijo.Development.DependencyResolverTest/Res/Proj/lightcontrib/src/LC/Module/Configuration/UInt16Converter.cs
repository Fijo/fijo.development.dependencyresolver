namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class UInt16Converter : ConverterBase<long, ushort> {
		#region Overrides of ConverterBase<long,int>
		public override ushort Convert(long source) {
			return System.Convert.ToUInt16(source);
		}
		#endregion
	}
}