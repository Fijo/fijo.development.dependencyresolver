using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Encoding {
	public interface IEncodingProvider {
		Text.Encoding GetDefault();
	}
}