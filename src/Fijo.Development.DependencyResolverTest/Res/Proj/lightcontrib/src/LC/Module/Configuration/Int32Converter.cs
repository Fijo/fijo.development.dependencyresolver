namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class Int32Converter : ConverterBase<long, int> {
		#region Overrides of ConverterBase<long,int>
		public override int Convert(long source) {
			return System.Convert.ToInt32(source);
		}
		#endregion
	}
}