using Fijo.Infrastructure.Documentation.Attributes.Info;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Path {
	public interface IPathService {
		[Pure]
		bool IsInPath(string path, string basePath);

		[Pure]
		string GetPathRelativeTo(string path, string basePath);

		void MayCreateParentFolders(string path);

		[Pure]
		bool IsRelative(string path);

		[Pure]
		bool IsAbsolute(string path);

		[Pure]
		bool ContainsVolumePattern(string path);

		[Pure]
		string GetVolumePatternInPath(string path);

		[Pure]
		string GetVolumeNameFromPattern(string volumePattern);

		[Pure, About("path is for file/ dictionary path")]
		string PathToCygWinPath(string path);
	}
}