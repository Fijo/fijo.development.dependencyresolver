using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	public class LongNumberService : NumberServiceBase<long> {
		#region Implementation of INumberService<byte>
		public override long MaxValue { get { return long.MaxValue; } }
		public override long MinValue { get { return long.MinValue; } }
		public override bool IsInteger { get { return true; } }

		public override long Sun(long left, long right) {
			return left + right;
		}

		public override long Diff(long left, long right) {
			return left - right;
		}

		public override long Times(long left, long right) {
			return left * right;
		}

		public override long Divide(long left, long right) {
			return left / right;
		}

		public override long Modulo(long left, long right) {
			return left % right;
		}
		#endregion
	}
}