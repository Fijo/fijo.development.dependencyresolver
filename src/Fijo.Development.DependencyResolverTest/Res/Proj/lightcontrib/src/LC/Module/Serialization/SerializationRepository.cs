using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public class SerializationRepository : RepositoryBase<IDictionary<SerializationFormat, ISerialization>> {
		protected readonly IDictionary<SerializationFormat, ISerialization> Content;
		private readonly IPairFactory _pairFactory = Kernel.Resolve<IPairFactory>();

		public SerializationRepository() {
			Content = new Dictionary<SerializationFormat, ISerialization>
			{
				{SerializationFormat.Binary, new BinarySerialization()},
				{SerializationFormat.JSON, new JSONSerialization()},
				{SerializationFormat.XML, new XMLSerialization()}
			};
		}
		#region Overrides of RepositoryBase<IDictionary<SerializationFormat,ISerialization>>
		public override IDictionary<SerializationFormat, ISerialization> Get() {
			return Content;
		}
		#endregion
	}
}