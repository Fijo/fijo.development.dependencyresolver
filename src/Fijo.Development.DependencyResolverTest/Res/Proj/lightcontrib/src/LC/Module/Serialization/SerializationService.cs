using System.IO;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using JetBrains.Annotations;
using IO = System.IO;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public class SerializationService : ISerializationService {
		private readonly IStreamService _streamService = Kernel.Resolve<IStreamService>();

		public void Serialize(ISerialization serialization, object obj, IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null) {
			serialization.Serialize(obj, stream, formatting, encoding);
		}
		
		public void Serialize<T>(ISerialization serialization, T obj, IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null) {
			serialization.Serialize(obj, stream, formatting, encoding);
		}

		public string Serialize(ISerialization serialization, object obj, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null) {
			var stream = new MemoryStream();
			Serialize(serialization, obj, stream, formatting, encoding);
			stream.Position = 0;
			return _streamService.ReadToString(stream);
		}

		public string Serialize<T>(ISerialization serialization, T obj, SerializationFormatting formatting = SerializationFormatting.Default, Text.Encoding encoding = null) {
			var stream = GetStream();
			Serialize(serialization, obj, stream, formatting, encoding);
			stream.Position = 0;
			return _streamService.ReadToString(stream);
		}

		public object Deserialize(ISerialization serialization, IO.Stream stream, Text.Encoding encoding = null) {
			return serialization.Deserialize(stream, encoding);
		}

		public T Deserialize<T>(ISerialization serialization, IO.Stream stream, Text.Encoding encoding = null) {
			return serialization.Deserialize<T>(stream, encoding);
		}

		public object Deserialize(ISerialization serialization, string source, Text.Encoding encoding = null) {
			return Deserialize(serialization, GetStream(source), encoding);
		}

		public T Deserialize<T>(ISerialization serialization, string source, Text.Encoding encoding = null) {
			return Deserialize<T>(serialization, GetStream(source), encoding);
		}

		[NotNull, Pure]
		private IO.Stream GetStream(string content) {
			var stream = new MemoryStream();
			_streamService.WriteToStream(stream, content);
			stream.Position = 0;
			return stream;
		}

		[NotNull, Pure]
		private IO.Stream GetStream() {
			return new MemoryStream();
		}
	}
}