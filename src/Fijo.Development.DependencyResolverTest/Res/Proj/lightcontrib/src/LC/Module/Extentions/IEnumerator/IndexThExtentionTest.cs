﻿using System.Collections.Generic;
using FijoCore.Components.Extentions.IEnumerator;
using NUnit.Framework;

namespace FijoCore.ComponentTest.Extentions.IEnumerator
{
	[TestFixture]
	public class IndexThExtentionTest
	{
		[Test, Ignore("it does not the same as the indexer")]
		public void Test_IndexThExtentionWorksAsTheIndexerDo()
		{
			var collection = new List<string>
			                 	{
			                 		"test",
			                 		"hallo",
			                 		"neu",
			                 		"blah"
			                 	};
			Assert.AreEqual(collection[0], collection.GetEnumerator().IndexTh(0));
			Assert.AreEqual(collection[1], collection.GetEnumerator().IndexTh(1));
			Assert.AreEqual(collection[2], collection.GetEnumerator().IndexTh(2));
			Assert.AreEqual(collection[3], collection.GetEnumerator().IndexTh(3));
		}
	}
}
