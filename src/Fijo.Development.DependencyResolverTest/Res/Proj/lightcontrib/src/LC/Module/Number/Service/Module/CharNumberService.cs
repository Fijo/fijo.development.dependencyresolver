using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	public class CharNumberService : NumberServiceBase<char> {
		#region Implementation of INumberService<byte>
		public override char MaxValue { get { return char.MaxValue; } }
		public override char MinValue { get { return char.MinValue; } }
		public override bool IsInteger { get { return true; } }

		public override char Sun(char left, char right) {
			return (char) (left + right);
		}

		public override char Diff(char left, char right) {
			return (char) (left - right);
		}

		public override char Times(char left, char right) {
			return (char) (left * right);
		}

		public override char Divide(char left, char right) {
			return (char) (left / right);
		}

		public override char Modulo(char left, char right) {
			return (char) (left % right);
		}
		#endregion
	}
}