namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public abstract class ConverterBase<TSource, TResult> : IConverter<TSource, TResult> {
		#region Implementation of IConverter
		public abstract TResult Convert(TSource source);

		public object Convert(object source) {
			return Convert((TSource) source);
		}
		#endregion
	}
}