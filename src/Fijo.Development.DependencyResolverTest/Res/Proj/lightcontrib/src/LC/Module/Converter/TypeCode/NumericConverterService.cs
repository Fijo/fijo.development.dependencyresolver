using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Module.Converter.TypeCode.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.TypeCode {
	public class NumericConverterService : ConverterServiceTypeCodeBase<double> {
		public NumericConverterService() {
			ConvertableTypes = new[]
			                   {
			                   	System.TypeCode.Byte,
			                   	System.TypeCode.Char,
			                   	System.TypeCode.Decimal,
			                   	System.TypeCode.Double,
			                   	System.TypeCode.Int16,
			                   	System.TypeCode.Int32,
			                   	System.TypeCode.Int64,
			                   	System.TypeCode.Object,
			                   	System.TypeCode.SByte,
			                   	System.TypeCode.Single,
			                   	System.TypeCode.UInt16,
			                   	System.TypeCode.UInt32,
			                   	System.TypeCode.UInt64
			                   };
		}

		protected override double GetConverted<T>(T obj) {
			#region PreCondtion
			Debug.Assert(obj != null);
			#endregion

			return System.Convert.ToDouble(obj);
		}
	}
}