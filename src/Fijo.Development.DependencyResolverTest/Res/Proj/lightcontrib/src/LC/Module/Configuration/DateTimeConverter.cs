using System;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class DateTimeConverter : ConverterBase<string, DateTime> {
		#region Overrides of ConverterBase<string,DateTime>
		public override DateTime Convert(string source) {
			return DateTime.Parse(source);
		}
		#endregion
	}
}