using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	public class ShortNumberService : NumberServiceBase<short> {
		#region Implementation of INumberService<byte>
		public override short MaxValue { get { return short.MaxValue; } }
		public override short MinValue { get { return short.MinValue; } }
		public override bool IsInteger { get { return true; } }

		public override short Sun(short left, short right) {
			return (short) (left + right);
		}

		public override short Diff(short left, short right) {
			return (short) (left - right);
		}

		public override short Times(short left, short right) {
			return (short) (left * right);
		}

		public override short Divide(short left, short right) {
			return (short) (left / right);
		}

		public override short Modulo(short left, short right) {
			return (short) (left % right);
		}
		#endregion
	}
}