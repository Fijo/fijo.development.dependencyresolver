using FijoCore.Infrastructure.LightContrib.Enums;
using JetBrains.Annotations;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public interface ISerializationService {
		void Serialize(ISerialization serialization, object obj, System.IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null);
		void Serialize<T>(ISerialization serialization, T obj, System.IO.Stream stream, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null);
		string Serialize(ISerialization serialization, object obj, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null);
		string Serialize<T>(ISerialization serialization, T obj, SerializationFormatting formatting = SerializationFormatting.Default, [CanBeNull] Text.Encoding encoding = null);
		object Deserialize(ISerialization serialization, System.IO.Stream stream, [CanBeNull] Text.Encoding encoding = null);
		T Deserialize<T>(ISerialization serialization, System.IO.Stream stream, [CanBeNull] Text.Encoding encoding = null);
		object Deserialize(ISerialization serialization, string source, [CanBeNull] Text.Encoding encoding = null);
		T Deserialize<T>(ISerialization serialization, string source, [CanBeNull] Text.Encoding encoding = null);
	}
}