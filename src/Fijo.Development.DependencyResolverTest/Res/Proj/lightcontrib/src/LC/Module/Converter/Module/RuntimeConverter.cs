﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Enums;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Module
{
	public class RuntimeConverter {
		private class RuntimeVersionCheckEntry {
			public readonly float MinValue;
			public readonly float MaxValue;
			public readonly Runtime Runtime;
			public RuntimeVersionCheckEntry(float minValue, float maxValue, Runtime runtime) {
				MinValue = minValue;
				MaxValue = maxValue;
				Runtime = runtime;
			}
		}

		private readonly ICollection<RuntimeVersionCheckEntry> _runtimeVersions = new []
		{
			new RuntimeVersionCheckEntry(0.0f, 1.0f, Runtime.Net1_0), 
			new RuntimeVersionCheckEntry(1.1f, 1.9f, Runtime.Net1_1), 
			new RuntimeVersionCheckEntry(2.0f, 2.9f, Runtime.Net2_0), 
			new RuntimeVersionCheckEntry(3.0f, 3.4f, Runtime.Net3_0), 
			new RuntimeVersionCheckEntry(3.5f, 3.9f, Runtime.Net3_5), 
			new RuntimeVersionCheckEntry(4.0f, 4.4f, Runtime.Net4_0), 
			new RuntimeVersionCheckEntry(4.5f, 4.9f, Runtime.Net4_5) 
		};

		[Description("Expecting a format for arg ´runtime´ as given by Assembly.ImageRuntimeVersion")]
		public float ConvertToFloat(string runtime, bool onlyTheFirstSubversionDigit = true) {
			#region PreCondition
			RuntimePreCondition(runtime);
			#endregion

			var versionEnumerator = runtime.Substring(1).Split('.').Take(2).GetEnumerator();
			Debug.Assert(versionEnumerator.MoveNext());
			var mainVersion = versionEnumerator.Current;
			Debug.Assert(versionEnumerator.MoveNext());
			var subVersion = versionEnumerator.Current;
			if(onlyTheFirstSubversionDigit) subVersion = subVersion.Substring(0, 1);
			return float.Parse(string.Format("{0}.{1}", mainVersion, subVersion), CultureInfo.InvariantCulture);
		}
		
		[Description("Expecting a format for arg ´runtime´ as given by Assembly.ImageRuntimeVersion")]
		public Runtime ConvertToRuntime(string runtime) {
			#region PreCondition
			RuntimePreCondition(runtime);
			#endregion

			return ConvertRuntimeByFloat(ConvertToFloat(runtime));
		}

		private Runtime ConvertRuntimeByFloat(float runtimeVersion) {
			foreach (var runtimeVersionCheckEntry in _runtimeVersions.Where(runtimeVersionCheckEntry => runtimeVersion >= runtimeVersionCheckEntry.MinValue && runtimeVersion <= runtimeVersionCheckEntry.MaxValue)) {
				return runtimeVersionCheckEntry.Runtime;
			}

			throw new NotSupportedException(string.Format("Invalid or unknown runtime version ´{0}´", runtimeVersion));
		}

		#region Contracts
		[Conditional("DEBUG")]
		private void RuntimePreCondition(string runtime) {
			Debug.Assert(!string.IsNullOrWhiteSpace(runtime), "Invalid runtime version format - cannot be null or whitespace");
			Debug.Assert(runtime[0] == 'v', @"Invalid runtime version format - must start with ""v""");
			Debug.Assert(runtime.Substring(1).Split('.').Count() > 1, "Invalid runtime version format - must contain at least one point");
			Debug.Assert(runtime.Substring(1).Split('.').Take(2).All(x => !string.IsNullOrWhiteSpace(x)), "Invalid runtime version format - from part after the the ´v´ if you split it by ´.´ the first 2 entries can´t be null or whitespace");
			Debug.Assert(runtime.Substring(1).Split('.').Take(2).All(IsNumeric), "Invalid runtime version format - from part after the the ´v´ if you split it by ´.´ the first 2 entries must be nummeric");
		}
		#endregion
		
		#region Debug Contract Helper
		#if DEBUG
		private bool IsNumeric(string str) {
			return str.All(IsNumeric);
		}
		
		private bool IsNumeric(char c) {
			return "0123456789".Contains(c);
		}
		#endif
		#endregion
	}
}
