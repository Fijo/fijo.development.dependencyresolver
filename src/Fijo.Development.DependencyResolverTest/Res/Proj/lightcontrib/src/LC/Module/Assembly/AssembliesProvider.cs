using System;
using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Module.Assembly {
	public class AssembliesProvider {
		public IEnumerable<System.Reflection.Assembly> GetAssemblies() {
			return AppDomain.CurrentDomain.GetAssemblies();
		}
	}
}