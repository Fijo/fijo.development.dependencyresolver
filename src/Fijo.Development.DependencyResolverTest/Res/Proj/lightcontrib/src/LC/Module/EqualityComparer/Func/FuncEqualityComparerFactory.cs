using System;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Func
{
	[Obsolete]
	public class FuncEqualityComparerFactory
	{
		public Func<object, object, bool> Create() {
			return (x, y) => x.Equals(y);
		}

		public Func<T, T, bool> Create<T>() {
			return (x, y) => x.Equals(y);
		} 
	}
}