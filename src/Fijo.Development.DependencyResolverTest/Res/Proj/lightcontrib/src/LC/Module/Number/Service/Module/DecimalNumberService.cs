using FijoCore.Infrastructure.LightContrib.Module.Number.Service.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Number.Service.Module {
	public class DecimalNumberService : NumberServiceBase<decimal> {
		#region Implementation of INumberService<byte>
		public override decimal MaxValue { get { return decimal.MaxValue; } }
		public override decimal MinValue { get { return decimal.MinValue; } }
		public override bool IsInteger { get { return false; } }

		public override decimal Sun(decimal left, decimal right) {
			return left + right;
		}

		public override decimal Diff(decimal left, decimal right) {
			return left - right;
		}

		public override decimal Times(decimal left, decimal right) {
			return left * right;
		}

		public override decimal Divide(decimal left, decimal right) {
			return left / right;
		}

		public override decimal Modulo(decimal left, decimal right) {
			return left % right;
		}
		#endregion
	}
}