namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public interface IConfigurationService {
		T Get<T>(string key);
	}
}