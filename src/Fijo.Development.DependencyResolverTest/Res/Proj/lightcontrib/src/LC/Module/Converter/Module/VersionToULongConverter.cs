using System;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.Module {
	public class VersionToULongConverter {
		public ulong Convert([NotNull] Version version) {
			return (ulong) ((version.Major << 48) + (version.Minor << 32) + (version.Build << 16) + version.Revision);
		}
	}
}