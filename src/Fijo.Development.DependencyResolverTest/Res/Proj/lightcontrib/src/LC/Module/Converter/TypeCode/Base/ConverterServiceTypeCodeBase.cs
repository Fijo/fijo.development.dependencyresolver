using FijoCore.Infrastructure.LightContrib.Module.Converter.Base;

namespace FijoCore.Infrastructure.LightContrib.Module.Converter.TypeCode.Base {
	public abstract class ConverterServiceTypeCodeBase<TResult> : ConverterServiceBase<System.TypeCode, TResult> {
		protected override System.TypeCode GetType<T>() {
			return System.Type.GetTypeCode(typeof (T));
		}
	}
}