using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Interface {
	[Service]
	public interface IRecursiveEqualityComparerService {
		bool DefaultRecursiveSimpleCompare([NotNull] object x, [NotNull] object y);
		bool DefaultRecursiveSimpleCompareDeepness([NotNull] object x, [NotNull] object y, int deepness);
		bool DefaultRecursiveCompare([NotNull] object x, [NotNull] object y, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true);
		bool DefaultRecursiveCompare([NotNull] object x, [NotNull] object y, int deepness, bool genericEnumerable = true, bool enumerable = true, bool enumerator = true);
	}
}