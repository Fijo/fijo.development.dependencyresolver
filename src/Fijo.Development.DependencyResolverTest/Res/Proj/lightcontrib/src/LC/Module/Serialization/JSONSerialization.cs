using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.Stream;
using Newtonsoft.Json;
using IO = System.IO;
using Text = System.Text;

namespace FijoCore.Infrastructure.LightContrib.Module.Serialization {
	public class JSONSerialization : ISerialization {
		private readonly IMapping<SerializationFormatting, Formatting> _mapping = Kernel.Resolve<IMapping<SerializationFormatting, Formatting>>();
		private readonly IStreamService _streamService = Kernel.Resolve<IStreamService>();
		#region Implementation of ISerialization
		public void Serialize(object obj, IO.Stream stream, SerializationFormatting formatting, Text.Encoding encoding) {
			Serialize<object>(obj, stream, formatting, encoding);
		}

		public void Serialize<T>(T obj, IO.Stream stream, SerializationFormatting formatting, Text.Encoding encoding) {
			_streamService.WriteToStream(stream, JsonConvert.SerializeObject(obj, _mapping.Map(formatting)), encoding);
		}

		public object Deserialize(IO.Stream stream, Text.Encoding encoding) {
			return JsonConvert.DeserializeObject(_streamService.ReadToString(stream, encoding));
		}

		public T Deserialize<T>(IO.Stream stream, Text.Encoding encoding) {
			return JsonConvert.DeserializeObject<T>(_streamService.ReadToString(stream, encoding));
		}
		#endregion
	}
}