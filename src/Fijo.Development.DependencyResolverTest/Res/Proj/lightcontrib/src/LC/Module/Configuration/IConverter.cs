namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public interface IConverter {
		object Convert(object source);
	}

	public interface IConverter<in TSource, out TResult> : IConverter {
		TResult Convert(TSource source);
	}
}