using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Recursive
{
	[Service]
	public class RecursiveLimitedEqualityComparer<T> : IEqualityComparer<T>
	{
		#region Fields
		private readonly Func<IEnumerable<T>, IEnumerable<T>, RecursiveLimitedEqualityComparer<T>, bool> _genericRecursiveComparer;
		private readonly Func<IEnumerable, IEnumerable, int, bool> _recursiveComparer;
		private int _maxDepness;
		private bool CompareRecursive
		{
			get { return _maxDepness > 0; }
		}
		#endregion

		#region Constructor
		public RecursiveLimitedEqualityComparer(Func<IEnumerable<T>, IEnumerable<T>, RecursiveLimitedEqualityComparer<T>, bool> genericRecursiveComparer, Func<IEnumerable, IEnumerable, int, bool> recursiveComparer, int maxDepness)
		{
			_genericRecursiveComparer = genericRecursiveComparer;
			_recursiveComparer = recursiveComparer;
			_maxDepness = maxDepness;
		}
		#endregion

		private bool ContinueRecursive(T obj) {
			return obj is IEnumerable;
		}

		private bool GenericContinueRecursive(T obj) {
			return obj is IEnumerable<T>;
		}

		public void BeginCompare()
		{
			_maxDepness--;
		}

		public void EndCompare() {
			_maxDepness++;
		}

		public bool Equals(T x, T y) {
			Debug.WriteLine(string.Format("RecursiveLimitedEqualityComparer.Equals called. x = {0}, y = {1}, deepness = {2}", x, y, _maxDepness));
			Debug.WriteLine(string.Format("x is IEnumerable<T> = {0}, y is IEnumerable<T> = {1}, x is IEnumerable = {2}, y is IEnumerable = {3}, CompareRecursive = {4}", GenericContinueRecursive(x), GenericContinueRecursive(y), ContinueRecursive(x), ContinueRecursive(y), CompareRecursive));
			return !CompareRecursive
			       	? x.Equals(y)
			       	: GenericContinueRecursive(x) && GenericContinueRecursive(y)
			       	  	? _genericRecursiveComparer((IEnumerable<T>) x, (IEnumerable<T>) y, this)
						: ContinueRecursive(x) && ContinueRecursive(y)
			       	  	  	? _recursiveComparer((IEnumerable) x, (IEnumerable) y, _maxDepness)
			       	  	  	: x.Equals(y);
		}

		public int GetHashCode(T obj)
		{
			return CompareRecursive && (GenericContinueRecursive(obj) || ContinueRecursive(obj)) ? 0 : obj.GetHashCode();
		}
	}
}