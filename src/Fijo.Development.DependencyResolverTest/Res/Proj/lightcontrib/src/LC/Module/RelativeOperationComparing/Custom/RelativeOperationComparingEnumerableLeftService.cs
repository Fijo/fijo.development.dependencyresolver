using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Enums;
using FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Base;
using FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Wrapper;

namespace FijoCore.Infrastructure.LightContrib.Module.RelativeOperationComparing.Custom {
	public class RelativeOperationComparingEnumerableRightService {
		private readonly RelativeOperationComparingService _relativeOperationComparingService = Kernel.Resolve<RelativeOperationComparingService>();
		private readonly RelativeOperationComparingEnumerableWrapperService _relativeOperationComparingEnumerableWrapperService = Kernel.Resolve<RelativeOperationComparingEnumerableWrapperService>();

		public bool Compare(RelationOperation relationOperation, object left, object right) {
			return _relativeOperationComparingService.Compare(relationOperation, left, _relativeOperationComparingEnumerableWrapperService.MayRemoveEnumerable(relationOperation, right));
		}
	}
}