using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using FijoCore.Infrastructure.LightContrib.Default.Exceptions;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Recursive
{
	[Tested, Service, Description("An object equality comparer, that is although made to be called recursively. It decides automatically which EqualityComparer he uses. He prefers the IEqualityComparer<IEnumerable<T> and then the IEqualityComparer<IEnumerable>.")]
	[Pitfall("T is string", "it will interprete the string as IEnumerable<char> - so a sequential equality comparer will be used")]
	public class RecursiveSwitchedEqualityComparer<T> : IEqualityComparer<object>
	{
		#region Fields
		[NotNull] private readonly IEqualityComparer<IEnumerable> _recursiveComparer;
		[NotNull] private readonly IEqualityComparer<IEnumerable<T>> _genericRecursiveComparer;
		[NotNull] private readonly IEqualityComparer<T> _singleEqualityComparer;
		#endregion

		#region Constructor
		public RecursiveSwitchedEqualityComparer([NotNull] IEqualityComparer<IEnumerable> recursiveComparer, [NotNull] IEqualityComparer<T> singleEqualityComparer, [CanBeNull] IEqualityComparer<IEnumerable<T>> genericRecursiveComparer = null) {
			//if it isnt possible on another way wrap this to a EqualityComarer of T
			if (genericRecursiveComparer == null) genericRecursiveComparer = recursiveComparer;
			_singleEqualityComparer = singleEqualityComparer;
			_recursiveComparer = recursiveComparer;
			_genericRecursiveComparer = genericRecursiveComparer;
		}
		#endregion

		[Pure]
		private bool ContinueRecursive(object obj) {
			return obj is IEnumerable;
		}

		[Pure]
		private bool ContinueRecursiveGeneric(object obj) {
			return obj is IEnumerable<T>;
		}

		[Pure]
		private bool ContinueSingle(object obj) {
			return obj is T;
		}

		[Pure]
		private T ThrowTypeNotSupported<T>() {
			throw new NotSupportedTypeException();
		}

		#region Implementation of IEqualityComparer<in object>
		bool IEqualityComparer<object>.Equals(object x, object y) {
			if (ContinueRecursiveGeneric(x) && ContinueRecursiveGeneric(y)) return _genericRecursiveComparer.Equals((IEnumerable<T>) x, (IEnumerable<T>) y);
			if (ContinueRecursive(x) && ContinueRecursive(y)) return _recursiveComparer.Equals((IEnumerable) x, (IEnumerable) y);
			if (ContinueSingle(x) && ContinueSingle(y)) return _singleEqualityComparer.Equals((T)x, (T)y);
			return ThrowTypeNotSupported<bool>();
		}

		public int GetHashCode(object obj) {
			if (ContinueRecursiveGeneric(obj)) return _genericRecursiveComparer.GetHashCode((IEnumerable<T>) obj);
			if (ContinueRecursive(obj)) return _recursiveComparer.GetHashCode((IEnumerable) obj);
			if (ContinueSingle(obj)) return _singleEqualityComparer.GetHashCode((T) obj);
			return ThrowTypeNotSupported<int>();
		}
		#endregion
	}
}