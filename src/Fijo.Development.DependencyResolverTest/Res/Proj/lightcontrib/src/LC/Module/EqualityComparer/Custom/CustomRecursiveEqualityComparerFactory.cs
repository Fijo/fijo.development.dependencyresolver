using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Custom.Interface;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Custom
{
	public class CustomRecursiveEqualityComparerFactory : ICustomRecursiveEqualityComparerFactory {
		private readonly TypedDefaultRecursiveComparerCache _typedDefaultRecursiveComparer = new TypedDefaultRecursiveComparerCache();
		private class TypedDefaultRecursiveComparerCache : Cache.Cache {
			private readonly ICustomInternalRecursiveEqualityComparerFactory _customInternalRecursiveEqualityComparerFactory = Kernel.Resolve<ICustomInternalRecursiveEqualityComparerFactory>();

			[NotNull]
			protected override dynamic Create<TObj>() {
				return _customInternalRecursiveEqualityComparerFactory.TypedDefaultRecursiveCompare<TObj>();
			}
		}

		public IEqualityComparer<T> TypedDefaultRecursiveCompare<T>() where T : class {
			return _typedDefaultRecursiveComparer.Get<T, IEqualityComparer<T>>();
		}
	}
}