using System;
using System.Collections.Generic;
using System.Diagnostics;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	[Dto]
	public class TypeChangeDto {
		[NotNull] public readonly Type Source;
		[NotNull] public readonly Type Target;

		public TypeChangeDto([NotNull] Type source, [NotNull] Type target) {
			#region PreCondition
			Debug.Assert(source != typeof(object) || target != typeof(object), "source != typeof(object) || target != typeof(object)");
			#endregion
			Source = source;
			Target = target;
		}

		protected bool Equals([NotNull] TypeChangeDto other) {
			return FieldEquals(x => x.Source, other) && FieldEquals(x => x.Target, other);
		}

		private bool FieldEquals([NotNull] Func<TypeChangeDto, Type> fieldSelector, [NotNull] TypeChangeDto other) {
			var selfField = fieldSelector(this);
			var otherField = fieldSelector(other);
			return selfField == typeof(object) || otherField == typeof(object) || selfField == otherField;
		}

		public override bool Equals(object obj) {
			return obj is TypeChangeDto && Equals((TypeChangeDto) obj);
		}

		public override int GetHashCode() {
			return 0;
		}
	}
}