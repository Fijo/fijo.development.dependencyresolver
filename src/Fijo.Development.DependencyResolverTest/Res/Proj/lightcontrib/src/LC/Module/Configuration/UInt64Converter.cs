namespace FijoCore.Infrastructure.LightContrib.Module.Configuration {
	public class UInt64Converter : ConverterBase<long, ulong> {
		#region Overrides of ConverterBase<long,int>
		public override ulong Convert(long source) {
			return System.Convert.ToUInt64(source);
		}
		#endregion
	}
}