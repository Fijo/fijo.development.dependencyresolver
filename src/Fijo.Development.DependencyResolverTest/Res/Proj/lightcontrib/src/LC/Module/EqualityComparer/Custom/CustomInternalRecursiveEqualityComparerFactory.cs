using System.Collections;
using System.Collections.Generic;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Custom.Interface;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Recursive;

namespace FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Custom {
	public class CustomInternalRecursiveEqualityComparerFactory : ICustomInternalRecursiveEqualityComparerFactory {
		private readonly IEqualityComparerFactory _equalityComparerFactory = Kernel.Resolve<IEqualityComparerFactory>();

		public IEqualityComparer<T> TypedDefaultRecursiveCompare<T>() where T : class {
			var singleComparer = _equalityComparerFactory.Default<T>();
			var recursiveComparer = _equalityComparerFactory.Default<IEnumerable>((a, b) => Extentions.IEnumerable.RecursiveSequenceEqualExtention.RecursiveSequenceEqual(a, b));
			var genericRecursiveComparer = _equalityComparerFactory.Default<IEnumerable<T>>((a, b) => a.RecursiveSequenceEqual(b));
			return new RecursiveSwitchedEqualityComparer<T>(recursiveComparer, singleComparer, genericRecursiveComparer);
		}
	}
}