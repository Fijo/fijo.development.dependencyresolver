using System;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	public class ConfigurationInvalidValueTypeException : Exception {
		public string Key { get; protected set; }
		public Type ActualType { get; protected set; }
		public Type CorrectType { get; protected set; }
		public string ActualValue { get; protected set; }

		public ConfigurationInvalidValueTypeException(string key, Type actualType, Type correctType, string actualValue) {
			Key = key;
			ActualType = actualType;
			CorrectType = correctType;
			ActualValue = actualValue;
		}
	}
}