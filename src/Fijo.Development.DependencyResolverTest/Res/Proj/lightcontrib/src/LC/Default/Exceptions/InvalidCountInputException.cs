using System;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	public class InvalidCountInputException : Exception {
		public InvalidCountInputException() {}

		public InvalidCountInputException(string messag) : base(messag) {}
	}
}