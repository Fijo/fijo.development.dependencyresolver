﻿using System;
using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Default.Service.IListFunc.Interface;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.IListFunc
{
    public class DefaultIListFunc : IDefaultIListFunc
    {
        public bool Contains<T>(IList<T> me, T item)
        {
            #region PreCondition
            if (me == null) throw new ArgumentNullException("me");
            #endregion

            return me.Contains(item);
        }

        public void Add<T>(IList<T> me, T item)
        {
            #region PreCondition
            if (me == null) throw new ArgumentNullException("me");
            #endregion

        	me.Add(item);
        }
    }
}
