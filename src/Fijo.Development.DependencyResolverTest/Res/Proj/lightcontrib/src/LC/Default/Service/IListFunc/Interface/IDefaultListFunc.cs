using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.IListFunc.Interface
{
    public interface IDefaultIListFunc
    {
        bool Contains<T>(IList<T> me, T item);
        void Add<T>(IList<T> me, T item);
    }
}