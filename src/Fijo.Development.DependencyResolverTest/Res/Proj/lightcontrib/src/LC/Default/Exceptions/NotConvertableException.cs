﻿using System;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions
{
	public class NotConvertableException : Exception {
		public NotConvertableException() {}
		public NotConvertableException(string message) : base(message) {}
	}
}
