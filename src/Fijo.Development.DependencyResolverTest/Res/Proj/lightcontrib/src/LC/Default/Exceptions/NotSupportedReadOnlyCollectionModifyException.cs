namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions
{
	public class NotSupportedReadOnlyCollectionModifyException : NotSupportedModifyException
	{
		public NotSupportedReadOnlyCollectionModifyException()
		{
			
		}

		public NotSupportedReadOnlyCollectionModifyException(string message) : base(message)
		{
			
		}
	}
}