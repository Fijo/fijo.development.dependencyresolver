using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.ListFunc.Interface
{
    public interface IDefaultListFunc
    {
        bool Contains<T>(List<T> me, T item);
        void Add<T>(List<T> me, T item);
        void AddRange<T>(List<T> me, IEnumerable<T> collection);
    }
}