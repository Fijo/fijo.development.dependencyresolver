using System;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	public class ConfigurationKeyNotFoundException : Exception {
		public string Key { get; protected set; }
		public Type Type { get; protected set; }

		public ConfigurationKeyNotFoundException(string key, Type type) {
			Key = key;
			Type = type;
		}
	}
}