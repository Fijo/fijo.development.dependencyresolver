using System;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions
{
	public class NotSupportedModifyException : NotSupportedException
	{
		public NotSupportedModifyException()
		{
			
		}

		public NotSupportedModifyException(string message) : base(message)
		{
			
		}
	}
}