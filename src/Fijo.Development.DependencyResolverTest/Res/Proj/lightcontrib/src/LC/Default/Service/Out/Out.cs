﻿using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.Out
{
	public class Out : IOut {
		[DebuggerStepThrough]
		public bool True<T>(out T obj, T value = default(T)) {
			obj = value;
			return true;
		}

		[DebuggerStepThrough]
		public bool False<T>(out T obj, T value = default(T)) {
			obj = value;
			return false;
		}

		[DebuggerStepThrough]
		public bool Generic<T>(out T obj, bool returnType, T trueValue = default(T), T falseValue = default(T)) {
			obj = returnType ? trueValue : falseValue;
			return returnType;
		}
	}
}
