﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.IListFunc.Interface;
using FijoCore.Infrastructure.LightContrib.Default.Service.ListFunc.Interface;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.ListFunc
{
    public class DefaultListFunc : IDefaultListFunc
    {
        private readonly IDefaultIListFunc _defaultIListFunc = Kernel.Resolve<IDefaultIListFunc>();

        // ToDo High abstraction for simple functionality - may improve performance
        public bool Contains<T>(List<T> me, T item)
        {
            #region PreCondition
            Debug.Assert(_defaultIListFunc != null);
            #endregion

            return _defaultIListFunc.Contains(me, item);
        }

        // ToDo High abstraction for simple functionality - may improve performance
        public void Add<T>(List<T> me, T item)
        {
            #region PreCondition
            Debug.Assert(_defaultIListFunc != null);
            #endregion

            _defaultIListFunc.Add(me, item);
        }

        public void AddRange<T>(List<T> me, IEnumerable<T> collection)
        {
            #region PreCondition
            if (me == null) throw new ArgumentNullException("me");
            if (collection == null) throw new ArgumentNullException("collection");
            #endregion

            me.AddRange(collection);
        }
    }
}
