namespace FijoCore.Infrastructure.LightContrib.Default.Service.Out.Interface {
	public interface IOut {
		bool True<T>(out T obj, T value = default(T));
		bool False<T>(out T obj, T value = default(T));
		bool Generic<T>(out T obj, bool returnType, T trueValue = default(T), T falseValue = default(T));
	}
}