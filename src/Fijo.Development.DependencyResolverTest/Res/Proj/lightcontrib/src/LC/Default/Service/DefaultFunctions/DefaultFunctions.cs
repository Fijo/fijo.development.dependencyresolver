﻿using FijoCore.Infrastructure.LightContrib.Default.Service.DefaultFunctions.Interface;

namespace FijoCore.Infrastructure.LightContrib.Default.Service.DefaultFunctions
{
	public class DefaultFunctions : IDefaultFunctions
    {
        public bool ReturnFalse<T>(T x)
        {
        	return false;
        }

		public bool ReturnTrue<T>(T x)
        {
        	return true;
        }
    }
}