using System;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions
{
	public class NotSupportedTypeException : NotSupportedException{
		public NotSupportedTypeException() {}

		public NotSupportedTypeException(string message) : base(message) {}
	}
}