using System;

namespace FijoCore.Infrastructure.LightContrib.Default.Exceptions {
	public class PathMustBeInBasePathException : Exception {
		public readonly string Path;
		public readonly string BasePath;

		public PathMustBeInBasePathException(string path, string basePath) {
			Path = path;
			BasePath = basePath;
		}
	}
}