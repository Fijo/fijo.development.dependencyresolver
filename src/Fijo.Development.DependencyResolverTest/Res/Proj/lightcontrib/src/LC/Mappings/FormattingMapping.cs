using System;
using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.LightContrib.Enums;
using Newtonsoft.Json;

namespace FijoCore.Infrastructure.LightContrib.Mappings {
	public class FormattingMapping : IMapping<SerializationFormatting, Formatting> {
		private readonly IDictionary<SerializationFormatting, Formatting> _content;

		public FormattingMapping() {
			_content = new Dictionary<SerializationFormatting, Formatting>
			{
				{SerializationFormatting.Default, Formatting.None},
				{SerializationFormatting.Indented, Formatting.Indented},
			};
		}

		#region Implementation of IMapping<in SerializationFormatting,out Formatting>
		public Formatting Map(SerializationFormatting source) {
			Formatting formatting;
			if(!_content.TryGetValue(source, out formatting)) NotSupported(source);
			return formatting;
		}

		private void NotSupported(SerializationFormatting source) {
			throw new NotSupportedException(string.Format("Mapping SerializationFormatting {0} to Formatting", Enum.GetName(typeof (SerializationFormatting), source)));
		}
		#endregion
	}
}