﻿namespace FijoCore.Infrastructure.LightContrib.Enums
{
	public static class Count {
		public const int None = 0;
		public const int Single = 1;
		public const int Both = 2;
	}
}
