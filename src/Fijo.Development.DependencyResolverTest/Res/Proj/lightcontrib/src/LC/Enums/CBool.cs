namespace FijoCore.Infrastructure.LightContrib.Enums {
	public enum CBool {
		Always = 1,
		Never = 2,
		Custom = 3
	}
}