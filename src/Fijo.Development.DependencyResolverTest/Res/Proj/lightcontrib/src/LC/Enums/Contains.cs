namespace FijoCore.Infrastructure.LightContrib.Enums {
	public enum Contains {
		None = 0,
		Some = 1,
		All = 2
	}
}