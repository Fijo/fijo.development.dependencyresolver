namespace FijoCore.Infrastructure.LightContrib.Enums {
	public enum SerializationFormat {
		Binary,
		JSON,
		XML
	}
}