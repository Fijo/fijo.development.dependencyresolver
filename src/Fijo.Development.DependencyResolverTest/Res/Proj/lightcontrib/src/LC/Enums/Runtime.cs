using System;

namespace FijoCore.Infrastructure.LightContrib.Enums {
	[Serializable]
	public enum Runtime : byte {
		Default,
		Mono1_0,
		Net1_0,
		Net1_1,
		Mono1_1,
		Net2_0,
		Mono2_0,
		Net3_0,
		Net3_5,
		Net4_0,
		Mono2_8,
		Net4_5
	}
}