﻿using JetBrains.Annotations;

namespace FijoCore.Infrastructure.LightContrib.Delegates
{
	[NotNull]
	public delegate T MergeWithExisting<T>([NotNull] T existing, [NotNull] T value);
}
