﻿using System;
using System.Diagnostics;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface;

namespace FijoCore.Infrastructure.LightContrib.Service.UniqueAdd
{
    public class UniqueAddService : IUniqueAddService
    {
        public void AssertUniqueAdd<TAddable, T>(TAddable me, T entry, Func<TAddable, T, bool> meConatins, Action<TAddable, T> meAdd)
        {
            #region PreCondition
            if (meConatins == null) throw new ArgumentNullException("meConatins");
            if (meAdd == null) throw new ArgumentNullException("meAdd");
            #endregion

            Debug.Assert(!meConatins(me, entry));
            meAdd(me, entry);
        }

        public void UniqueAdd<TAddable, T>(TAddable me, T entry, Func<TAddable, T, bool> meConatins, Action<TAddable, T> meAdd)
        {
            #region PreCondition
            if (meConatins == null) throw new ArgumentNullException("meConatins");
            if (meAdd == null) throw new ArgumentNullException("meAdd");
            #endregion

            if (!meConatins(me, entry)) meAdd(me, entry);
        }
    }
}
