﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals.Interface;

namespace FijoCore.Infrastructure.LightContrib.Service.OrderlessEquals
{
	// ToDo improve IEnumerable executions to get a better performance here
    public class OrderlessEqualsService : IOrderlessEqualsService
    {
		[DebuggerStepThrough]
        [Description("Only use this function on two uniqued IEnumerables.")]
		public bool OrderlessUniqueEquals<T>(IEnumerable<T> me, IEnumerable<T> other) {
			return OrderlessUniqueEqualsBase(me, other, (eqMe, eqOther) => eqMe.ContainsAny(eqOther));
        }
		
		[DebuggerStepThrough]
		[Description("Only use this function on two uniqued IEnumerables.")]
		public bool OrderlessUniqueEquals<T>(IEnumerable<T> me, IEnumerable<T> other, Func<T, T, bool> equals) {
			#region PreCondition
			if (equals == null) throw new ArgumentNullException("equals");
			#endregion

			return OrderlessUniqueEqualsBase(me, other, (eqMe, eqOther) => eqMe.ContainsAny(eqOther, equals));
		}
		
		[DebuggerStepThrough]
    	private bool OrderlessUniqueEqualsBase<T>(IEnumerable<T> me, IEnumerable<T> other, Func<IEnumerable<T>, IEnumerable<T>, bool> containsAny) {
			#region PreCondition
			if (me == null) throw new ArgumentNullException("me");
			if (other == null) throw new ArgumentNullException("other");
			Debug.Assert(me.IsUnique());
			Debug.Assert(other.IsUnique());
			#endregion

			return MainOrderlessEqualsBase(me, other, containsAny);
		}
		
		[DebuggerStepThrough]
		private bool MainOrderlessEqualsBase<T>(IEnumerable<T> me, IEnumerable<T> other, Func<IEnumerable<T>, IEnumerable<T>, bool> containsAny) {
			#region PreCondition
			if (containsAny == null) throw new ArgumentNullException("containsAny");
			#endregion

			return me.Count() == other.Count() && containsAny(me, other);
		}
		
		[DebuggerStepThrough]
		private bool OrderlessEqualsBase<T>(IEnumerable<T> me, IEnumerable<T> other, Func<IEnumerable<T>, IEnumerable<T>, bool> containsAny) {
			#region PreCondition
			if (me == null) throw new ArgumentNullException("me");
			if (other == null) throw new ArgumentNullException("other");
			#endregion

			return MainOrderlessEqualsBase(me, other, containsAny);
		}
		
		[DebuggerStepThrough]
        public bool OrderlessEquals<T>(IEnumerable<T> me, IEnumerable<T> other)
        {
        	return OrderlessEqualsBase(me, other, (eqMe, eqOther) => eqMe.All(x => eqMe.CountWhere(x) == eqOther.CountWhere(x)));
        }
		
		[DebuggerStepThrough]
		public bool OrderlessEquals<T>(IEnumerable<T> me, IEnumerable<T> other, Func<T, T, bool> equals) {
			#region PreCondition
			if (equals == null) throw new ArgumentNullException("equals");
			#endregion

			return OrderlessEqualsBase(me, other, (eqMe, eqOther) => eqMe.All(x => eqMe.CountWhere(x, equals) == eqOther.CountWhere(x, equals)));
		}
    }
}