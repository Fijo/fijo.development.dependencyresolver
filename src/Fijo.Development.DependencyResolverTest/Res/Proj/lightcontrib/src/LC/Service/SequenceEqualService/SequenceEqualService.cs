using System;
using System.Collections;
using System.Collections.Generic;
using FijoCore.Infrastructure.LightContrib.Service.SequenceEqualService.Interface;

namespace FijoCore.Infrastructure.LightContrib.Service.SequenceEqualService
{
    public class SequenceEqualService : ISequenceEqualService
    {
        public bool SequenceEqual(IEnumerator me, IEnumerator other, Func<object, object, bool> equalsFunc)
        {
			while (me.MoveNext())
			{
				if (!other.MoveNext() || !equalsFunc(me.Current, other.Current))
					return false;
			}
			return !other.MoveNext();
        }

        public bool SequenceEqual<T>(IEnumerator<T> me, IEnumerator<T> other, Func<T, T, bool> equalsFunc)
        {
			while (me.MoveNext())
			{
				if (!other.MoveNext() || !equalsFunc(me.Current, other.Current))
					return false;
			}
			return !other.MoveNext();
        }
    }
}