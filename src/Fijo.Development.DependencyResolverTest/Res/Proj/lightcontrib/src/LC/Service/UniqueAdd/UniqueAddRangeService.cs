﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface;

namespace FijoCore.Infrastructure.LightContrib.Service.UniqueAdd
{
    public class UniqueAddRangeService : IUniqueAddRangeService
    {
        public void AssertUniqueAddRange<TAddable, T>(TAddable me, IEnumerable<T> collection, Func<TAddable, T, bool> meConatins, Action<TAddable, IEnumerable<T>> meAddRange)
        {
            #region PreCondition
            if (collection == null) throw new ArgumentNullException("collection");
            if (meConatins == null) throw new ArgumentNullException("meConatins");
            if (meAddRange == null) throw new ArgumentNullException("meAddRange");
            #endregion

            Debug.Assert(!collection.Any(x => meConatins(me, x)));
            meAddRange(me, collection);
        }

        public void UniqueAddRange<TAddable, T>(TAddable me, IEnumerable<T> collection, Func<TAddable, T, bool> meConatins, Action<TAddable, IEnumerable<T>> meAddRange)
        {
            #region PreCondition
            if (collection == null) throw new ArgumentNullException("collection");
            if (meConatins == null) throw new ArgumentNullException("meConatins");
            if (meAddRange == null) throw new ArgumentNullException("meAddRange");
            #endregion

            meAddRange(me, collection.Where(x => !meConatins(me, x)));
        }
    }
}
