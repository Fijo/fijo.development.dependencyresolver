using System;
using System.Collections.Generic;

namespace FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface
{
    public interface IUniqueAddRangeService
    {
        void AssertUniqueAddRange<TAddable, T>(TAddable me, IEnumerable<T> collection, Func<TAddable, T, bool> meConatins, Action<TAddable, IEnumerable<T>> meAddRange);
        void UniqueAddRange<TAddable, T>(TAddable me, IEnumerable<T> collection, Func<TAddable, T, bool> meConatins, Action<TAddable, IEnumerable<T>> meAddRange);
    }
}