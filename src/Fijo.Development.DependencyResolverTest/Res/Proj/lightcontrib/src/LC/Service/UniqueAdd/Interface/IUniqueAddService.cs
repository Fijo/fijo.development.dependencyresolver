using System;

namespace FijoCore.Infrastructure.LightContrib.Service.UniqueAdd.Interface
{
    public interface IUniqueAddService
    {
        void AssertUniqueAdd<TAddable, T>(TAddable me, T entry, Func<TAddable, T, bool> meConatins, Action<TAddable, T> meAdd);
        void UniqueAdd<TAddable, T>(TAddable me, T entry, Func<TAddable, T, bool> meConatins, Action<TAddable, T> meAdd);
    }
}