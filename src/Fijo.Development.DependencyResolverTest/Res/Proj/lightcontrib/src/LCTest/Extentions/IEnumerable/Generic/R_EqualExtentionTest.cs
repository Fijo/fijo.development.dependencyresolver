using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContribTest.Helper.InitKernel;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.IEnumerable.Generic {
	[TestFixture]
	public class RecursiveSequenceEqualExtentionTest
	{
		[SetUp]
		public void SetUp()
		{
			new InternalInitKernel().Init();
		}

		[Test]
		public void CompleteEqualButNotSameObjects_RecursiveSequenceEqual_MustReturnTrue() {
			Assert.True(GetTestData().RecursiveSequenceEqual(GetTestData()));
		}

		[Test]
		public void NotCompleteEqualObjects_RecursiveSequenceEqual_MustReturnFalse() {
			Assert.False(GetTestData().RecursiveSequenceEqual(GetTestAnotherData()));
		}

		[Test]
		public void CompleteEqualButNotSameObjects_SequenceEqual_MustReturnFalse() {
			Assert.False(GetTestData().SequenceEqual(GetTestData()));
		}

		private IList<IList<string>> GetTestData() {
			return new List<IList<string>>
			{
				new List<string> {"hallo", "welt", "neu"},
				new List<string> {"lol", "test", "rofl"},
				new List<string> {"test", "xD", "abc"}
			};
		}
		
		private IList<IList<string>> GetTestAnotherData() {
			var testData = GetTestData();
			testData[1][1] = "anderer Eintrag";
			return testData;
		}
	}
}