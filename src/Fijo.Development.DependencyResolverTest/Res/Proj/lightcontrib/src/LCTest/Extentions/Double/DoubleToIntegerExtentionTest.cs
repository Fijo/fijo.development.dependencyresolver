using FijoCore.Infrastructure.LightContrib.Extentions.Double;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.Double {
	[TestFixture]
	public class DoubleToIntegerExtentionTest
	{
		[TestCase(7.0, 7)]
		[TestCase(423.0, 423)]
		[TestCase(-295.0, -295)]
		[TestCase(-4.5, -4)]
		[TestCase(7.9, 7)]
		public void Test(double input, int expected)
		{
			Assert.AreEqual(input.ToInteger(), expected);
		}
	}
}