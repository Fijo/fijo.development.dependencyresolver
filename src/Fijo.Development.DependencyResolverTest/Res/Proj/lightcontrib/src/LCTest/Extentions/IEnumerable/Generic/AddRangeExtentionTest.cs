using System.Runtime.CompilerServices;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.IEnumerable.Generic {
	[TestFixture]
	public class AddRangeExtentionTest
	{
		[Test]
		public void TestAddRangeReturnDoesNotReturnTheSameAsTheMeContext()
		{
			var result = new[]
			             {
			             	"hallo",
			             	"test",
			             	"lol"
			             };
			var newResult = result.AddRangeReturn(new[]{"324", "welt"});
			Assert.IsFalse(RuntimeHelpers.Equals(result, newResult));
		}

		[Test]
		public void TestAddRangeReturnDoesReturnTheCorrectCollection()
		{
			var result = new[]
			             {
			             	"hallo",
			             	"test",
			             	"lol"
			             };
			var newResult = result.AddRangeReturn(new[]{"324", "welt"});

			var wanted = new[]
			             {
			             	"hallo",
			             	"test",
			             	"lol",
							"324",
							"welt"
			             };

			CollectionAssert.AreEquivalent(wanted, newResult);
		}
	}
}