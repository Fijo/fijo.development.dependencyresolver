using FijoCore.Infrastructure.LightContrib.Extentions.Double;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.Double
{
    [TestFixture]
    public class DoubleCanBeIntegerExtentionTest
    {
        [TestCase(7.0, true)]
        [TestCase(423.0, true)]
        [TestCase(-295.0, true)]
        [TestCase(-4.5, false)]
        [TestCase(7.9, false)]
        public void Test(double input, bool expected)
        {
            Assert.AreEqual(input.CanBeInteger(), expected);
        }
    }
}