﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FijoCore.Infrastructure.LightContrib.Extentions.Generic.Reflection.Resolve;
using FijoCore.Infrastructure.LightContribTest.Helper.InitKernel;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Extentions.Generic.Reflection.Resolve {
	[TestFixture]
	public class TryGetEnumeratorExtentionTest {
		[SetUp]
		public void SetUp()
		{
			new InternalInitKernel().Init();
		}
		
	/*
	[TestFixture]
	public class TryGetEnumenatorTest
	{
		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
		}

		[Test]
		public void Test()
		{
			IEnumerator result;
			TryGetEnuminatorExtention.TryGetEnuminator((IEnumerable<string>)new List<string>() { "test", "hallo" }, out result);
		}
	}
	*/
		[Test]
		public void Test()
		{
			System.Collections.IEnumerator test;
			Assert.IsTrue(new List<string> {"test", "hallo"}.TryGetEnumerator(out test));
			test.MoveNext();
			Assert.AreEqual(test.Current, "test");
			test.MoveNext();
			Assert.AreEqual(test.Current, "hallo");

			System.Collections.IEnumerator array;
			Assert.IsTrue(new[]{ "test", "hallo" }.TryGetEnumerator(out array));
			array.MoveNext();
			Assert.AreEqual(array.Current, "test");
			array.MoveNext();
			Assert.AreEqual(array.Current, "hallo");

			System.Collections.IEnumerator enumerable;
			Assert.IsTrue(new List<string> { "test", "hallo" }.Select(x => x + " test").TryGetEnumerator(out enumerable));
			enumerable.MoveNext();
			Assert.AreEqual(enumerable.Current, "test test");
			enumerable.MoveNext();
			Assert.AreEqual(enumerable.Current, "hallo test");


			/*
			Type t = typeof(List<List<string>>);


			Debug.WriteLine("   Implents IList? {0}",
	t.ImplementsInteface(typeof(IEnumerable<>)));

			Debug.WriteLine("   Is this a generic type? {0}",
	t.IsGenericType);
			Debug.WriteLine("   Is this a generic type definition? {0}",
				t.IsGenericTypeDefinition);

			Type tParam = t.GetGenericArguments().FirstOrDefault();
			Debug.WriteLine("      Type argument: {0}",
				tParam);
				*&
			/*
			Debug.WriteLine("   List {0} type arguments:",
	typeParameters.Length);
			foreach (Type tParam in typeParameters) {
				Debug.WriteLine("      Type argument: {0}",
					tParam);
			}


			*/
			//	var rofl = TestFunc((IEnumerator)new[] { "hallo", "test" }.Select(x => x + "test"));
			//	var lol = TestFunc((IEnumerator)new List<string> { "hallo", "test" }.Select(x => x + "test"));
		}


		// The following method displays information about a generic
		// type parameter. Generic type parameters are represented by
		// instances of System.Type, just like ordinary types.

		private System.Collections.IEnumerator TestFunc<T>(T obj)
		{
//			var type = obj.GetType();
//			MethodInfo mi = type.GetMethod("GetEnumerator");
//			return mi.Invoke(obj, null);
			throw new NotImplementedException();
		}
	}
}
