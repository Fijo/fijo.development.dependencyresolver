﻿using System;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using FijoCore.Infrastructure.LightContribTest.Helper.InitKernel;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Module.Configuration {
	[TestFixture]
	public class ConfigurationServiceTest {
		private IConfigurationService _configurationService;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_configurationService = Kernel.Resolve<IConfigurationService>();
		}

		[Test]
		public void CorrectConfigurationKeyOfValueTypeString_Get_ReturnsCorrectValue() {
			Assert.AreEqual("Lorem ipsum dolor sit amet, euismod sed senectus sollicitudin, lacus risus", _configurationService.Get<string>("Fijo.Infrastructure.LightContribTest.Module.Configuration.ConfigurationServiceTest.MockText"));
		}

		[Test]
		public void CorrectConfigurationKeyOfValueTypeStringInCorrectDateTimeFormat_Get_ReturnsCorrectDateTime() {
			Assert.AreEqual(new DateTime(2010, 1, 1), _configurationService.Get<DateTime>("Fijo.Infrastructure.LightContribTest.Module.Configuration.ConfigurationServiceTest.DateTimeTest"));
		}
	}
}