using System;
using System.Diagnostics;
using System.IO;
using FijoCore.Infrastructure.LightContrib.Module.Path;
using NUnit.Framework;
using IO = System.IO;

namespace FijoCore.Infrastructure.LightContribTest.Module.Path {
	[TestFixture]
	public class PathServiceTest {
		private IPathService _pathService;

		[SetUp]
		public void SetUp() {
			_pathService = new PathService();
		}

		#if !PLATFORM_UNIX
		#if PLATFORM_WINDOWS
		[TestCase(true, @"C:\")]
		[TestCase(true, @"C:\Test\gfdjgl\gfd\gd.tgf")]
		[TestCase(true, @"C:\Test\gfdjgl\gfd\")]
		[TestCase(true, @"C:\Test\gfdjgl\gfd")]
		[TestCase(true, @"C:\Test\g:fdjgl\g:fd")]
		[TestCase(true, @"c:\Test")]
		[TestCase(true, @"c:\Tes::t\")]
		[TestCase(false, @"..\.")]
		[TestCase(false, @"..\..\")]
		[TestCase(false, @"..\..\Test")]
		[TestCase(false, @"..\..\Test\text.txt")]
		[TestCase(false, @"..")]
		[TestCase(false, @".")]
		[TestCase(false, @"hallo.doc")]
		[TestCase(false, @"Daten\Test")]
		[TestCase(false, @"Daten\Test\")]
		#else
		[TestCase(true, @"V:")]
		[TestCase(true, @"V:hgfd3_4")]
		[TestCase(true, @"MAC:Test")]
		[TestCase(true, @"MAC:Test:neu")]
		[TestCase(true, @"macintosh:Tes:tesg.doc")]
		[TestCase(true, @"macintosh:Tes:tesg")]
		// ToDo nicht eindeutig - eindeutig machen
//		[TestCase(false, @"..:.")]
//		[TestCase(false, @"..:..:")]
//		[TestCase(false, @"..:..:Test")]
//		[TestCase(false, @"..:..:Test:text.txt")]
		[TestCase(false, @"..")]
		[TestCase(false, @".")]
		[TestCase(false, @"hallo.doc")]
		// ToDo nicht eindeutig - eindeutig machen
//		[TestCase(false, @"Daten:Test")]
//		[TestCase(false, @"Daten:Test:")]
		#endif
		#else
		[TestCase(false, @"/var/")]
		[TestCase(false, @"/etc/fstab")]
		[TestCase(false, @"/proc")]
		[TestCase(false, @"/")]
		[TestCase(false, @"/sbin/")]
		[TestCase(false, @"/home/Jonas/")]
		[TestCase(false, @"/home/Ubuntu/Documents")]
		[TestCase(false, @"../Documents")]
		[TestCase(false, @"..")]
		[TestCase(false, @".")]
		[TestCase(false, @"../../Test")]
		#endif
		public void ContainsVolumePatternTest(bool wantedContains, string path) {
			Assert.AreEqual(wantedContains, _pathService.ContainsVolumePattern(path));
		}
		
		#if PLATFORM_WINDOWS
		[TestCase(@"C:\", @"C:\")]
		[TestCase(@"C:\", @"C:\Test\gfdjgl\gfd\gd.tgf")]
		[TestCase(@"C:\", @"C:\Test\gfdjgl\gfd\")]
		[TestCase(@"C:\", @"C:\Test\gfdjgl\gfd")]
		[TestCase(@"C:\", @"C:\Test\g:fdjgl\g:fd")]
		[TestCase(@"c:\", @"c:\Test")]
		[TestCase(@"c:\", @"c:\Tes::t\")]
		[TestCase(@"d:\", @"d:\gdfskgj\g")]
		#else
		#if !PLATFORM_UNIX
		[TestCase(@"V:", @"V:")]
		[TestCase(@"V:", @"V:hgfd3_4")]
		[TestCase(@"MAC:", @"MAC:Test")]
		[TestCase(@"MAC:", @"MAC:Test:neu")]
		[TestCase(@"macintosh:", @"macintosh:Tes:tesg.doc")]
		[TestCase(@"macintosh:", @"macintosh:Tes:tesg")]
		#endif
		#endif
		public void GetVolumePatternInPathTest(string wantedVolumePattern, string path) {
			Assert.AreEqual(wantedVolumePattern, _pathService.GetVolumePatternInPath(path));
		}
		
		#if PLATFORM_WINDOWS
		[TestCase(@"C", @"C:\")]
		[TestCase(@"C", @"C:\")]
		[TestCase(@"c", @"c:")]
		[TestCase(@"c", @"c:")]
		[TestCase(@"d", @"d:\")]
		[TestCase(@"d", @"d:")]
		#else
		#if !PLATFORM_UNIX
		[TestCase(@"V", @"V:")]
		[TestCase(@"MAC", @"MAC:")]
		[TestCase(@"macintosh", @"macintosh:")]
		#endif
		#endif
		public void GetVolumeNameFromPatternTest(string wantedVolumeName, string volumePattern) {
			Assert.AreEqual(wantedVolumeName, _pathService.GetVolumeNameFromPattern(volumePattern));
		}

		[Conditional("PLATFORM_WINDOWS")]
		[TestCase("/C/", @"C:\")]
		[TestCase("/C/", @"C:")]
		[TestCase("/C/Tools", @"C:\Tools")]
		[TestCase("/C/Tools", @"C:Tools")]
		[TestCase("/C/Tools/Testgdf:g", @"C:\Tools\Testgdf:g")]
		[TestCase("/C/T:ools/Testgdf234_g", @"C:\T:ools\Testgdf234_g")]
		public void PathToCygWinPathTest(string translatedPath, string path) {
			Assert.AreEqual(translatedPath, _pathService.PathToCygWinPath(path));
		}
		
		[Test]
		public void OneDirectoryMissingInPath_MayCreateParentFoldersTest() {
			var path = IO.Path.Combine(IO.Path.GetTempPath(), string.Format("testpath{0}", Guid.NewGuid().ToString()));
			_pathService.MayCreateParentFolders(path);
			Assert.True(Directory.Exists(path));
		}
		
		[Test]
		public void TwoDirectoryMissingInPath_MayCreateParentFoldersTest() {
			var path = IO.Path.Combine(IO.Path.GetTempPath(), string.Format("testpath{0}", Guid.NewGuid().ToString()), "subfolderTest123");
			_pathService.MayCreateParentFolders(path);
			Assert.True(Directory.Exists(path));
		}
	}
}