using System;
using System.Collections.Generic;
using System.Linq;
using Fijo.Infrastructure.Model.Pair;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using FijoCore.Infrastructure.LightContrib.Factories.Interface;
using FijoCore.Infrastructure.LightContrib.Module.EqualityComparer.Recursive;
using FijoCore.Infrastructure.LightContribTest.Helper.InitKernel;
using JetBrains.Annotations;
using NUnit.Framework;
using SequenceEqualExtention = FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.SequenceEqualExtention;

namespace FijoCore.Infrastructure.LightContribTest.Module.EqualityComparer.Recursive {
	[TestFixture]
	public class RecursiveSwitchedEqualityComparerTest {
		private IEqualityComparerFactory _equalityComparerFactory;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_equalityComparerFactory = Kernel.Resolve<IEqualityComparerFactory>();
		}

		[Test]
		public void MockedEqualityComparers_RecursiveSwitched_SequentialComparerMustBeCalledAsExpected() {
			var watchListSequencalComparer = GetWatchList();
			IEqualityComparer<object> recursiveSwitchedEqualityComparer = new RecursiveSwitchedEqualityComparer<int>(GetSequenceComparer(watchListSequencalComparer), GetSingleComparer());
			Assert.True(recursiveSwitchedEqualityComparer.Equals(GetSequentialMockData(), GetSequentialMockData()));
			var wanted = GetSequentialMockData().Select(x => new Pair<int, int>(x, x)).ToList();
			CollectionAssert.AreEqual(wanted, watchListSequencalComparer);
		}

		[Test]
		public void MockedEqualityComparers_RecursiveSwitched_SingleComparerMustBeCalledAsExpected() {
			var watchListSingleComparer = GetWatchList();
			IEqualityComparer<object> recursiveSwitchedEqualityComparer = new RecursiveSwitchedEqualityComparer<int>(GetSequenceComparer(), GetSingleComparer(watchListSingleComparer));
			Assert.True(recursiveSwitchedEqualityComparer.Equals(GetSingleMockData(), GetSingleMockData()));
			Assert.IsTrue(watchListSingleComparer.IsSingle());
			var singleMockData = GetSingleMockData();
			var wanted = new Pair<int, int>(singleMockData, singleMockData);
			Assert.AreEqual(wanted, watchListSingleComparer.Single());
		}
		
		private int GetSingleMockData() {
			return 54325;
		}

		private IEnumerable<int> GetSequentialMockData() {
			return Enumerable.Range(0, 10);
		}

		private IEqualityComparer<System.Collections.IEnumerable> GetSequenceComparer([CanBeNull] IList<IPair<int, int>> watchListRecursiveComparer = null) {
			return _equalityComparerFactory.Default<System.Collections.IEnumerable>(
				(a, b) => SequenceEqualExtention.SequenceEqual(a, b, (x, y) => CreateLoggingEqualityDelegate(watchListRecursiveComparer)((int) x, (int) y)));
		}

		private IEqualityComparer<int> GetSingleComparer([CanBeNull] IList<IPair<int, int>> watchListRecursiveComparer = null) {
			return _equalityComparerFactory.Default(CreateLoggingEqualityDelegate(watchListRecursiveComparer));
		}

		private Func<int, int, bool> CreateLoggingEqualityDelegate([CanBeNull] IList<IPair<int, int>> watchList = null) {
			if(watchList == null) watchList = GetWatchList();
			return (a, b) => {
				watchList.Add(new Pair<int, int>(a, b));
				return true;
			};
		}

		private IList<IPair<int, int>> GetWatchList() {
			return new List<IPair<int, int>>();
		}
	}
}