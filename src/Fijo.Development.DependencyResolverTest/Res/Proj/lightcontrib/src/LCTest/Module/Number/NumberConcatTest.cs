using System.Linq;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Number.Interface;
using FijoCore.Infrastructure.LightContribTest.Helper.InitKernel;
using NUnit.Framework;

namespace FijoCore.Infrastructure.LightContribTest.Module.Number {
	[TestFixture]
	public class NumberConcatTest {
		private INumberConcat _numberConcat;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_numberConcat = Kernel.Resolve<INumberConcat>();
		}
		
		[Test]
		public void UInts_Concat_MustReturnCorrectUInteger() {
			var bytes = new uint[] {32, 1};
			var wanted = (uint) 8193;
			var result = _numberConcat.Concat<uint, uint>(bytes, entryAddValue: 256);
			Assert.AreEqual(wanted, result);
		}

		[Test]
		public void Bytes_Concat_MustReturnCorrectUInteger() {
			var bytes = new byte[] {32, 1};
			var wanted = (uint) 8193;
			var result = _numberConcat.Concat<byte, uint>(bytes);
			Assert.AreEqual(wanted, result);
		}

		[Test]
		public void BytesAndAddValue64_Concat_MustReturnCorrectUInteger() {
			var bytes = new byte[] {32, 1};
			var wanted = (uint) 2049;
			var result = _numberConcat.Concat<byte, uint>(bytes, entryAddValue: 64);
			Assert.AreEqual(wanted, result);
		}

		[Test]
		public void Bytes_Concat_MustBeInSumTheSameAs() {
			var a = _numberConcat.Concat<byte, uint>(new byte[] {32, 23});
			var b = _numberConcat.Concat<byte, uint>(new byte[] {8, 12});
			var wanted = _numberConcat.Concat<byte, uint>(new byte[] {40, 35});
			var result = a + b;
			Assert.AreEqual(wanted, result);
		}
		
		[Test]
		public void Int8193_Split_MustReturnCorrectBytes() {
			var wanted = new byte[] {32, 1};
			var result = _numberConcat.Split<uint, byte>(8193, 2).ToArray();
			CollectionAssert.AreEqual(wanted, result);
		}
		
		[Test]
		public void Int2049AndMaxValue64_Split_MustReturnCorrectBytes() {
			var wanted = new byte[] {32, 1};
			var result = _numberConcat.Split<uint, byte>(2049, 64, 2).ToArray();
			CollectionAssert.AreEqual(wanted, result);
		}
	}
}