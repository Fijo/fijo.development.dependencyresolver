﻿using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;
using FijoCore.Infrastructure.LightContrib.Helper.InitKernel;

namespace FijoCore.Infrastructure.LightContribTest.Helper.InitKernel
{
    public class InternalInitKernel : ExtendedInitKernel
    {
        public override void PreInit()
        {
            LoadModules(new LightContribInjectionModule());
        }
    }
}
