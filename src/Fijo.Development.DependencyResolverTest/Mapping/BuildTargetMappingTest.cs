﻿using System;
using System.IO;
using Fijo.Development.DependencyResolver.Enum;
using Fijo.Development.DependencyResolver.Mapping;
using Fijo.Development.DependencyResolverTest.Properties;
using FijoCore.Infrastructure.LightContrib.Enums;
using Microsoft.Build.Evaluation;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class BuildTargetMappingTest {
		private BuildTargetMapping _buildTargetMapping;
		private TestData _testData;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_buildTargetMapping = new BuildTargetMapping();
			_testData = new TestData();
		}

		[Test]
		public void Test() {
			var projectFilePath = string.Format("testconfig{0}.xml", Guid.NewGuid().ToString());
			File.WriteAllText(projectFilePath, _testData.DummyCsProjectXml);

			var project = new Project(projectFilePath);
			var result = _buildTargetMapping.Map(project);
			Assert.AreEqual("FijoCore.Infrastructure.LightContrib", result.AssemblyName);
			Assert.AreEqual(true, result.DebugSymbols);
			Assert.AreEqual(DebugType.Full, result.DebugType);
			CollectionAssert.AreEqual(new[] {"TRACE", "DEBUG", "PLATFORM_WINDOWS"}, result.DefineConstants);
			Assert.AreEqual(CompilerErrorReport.Prompt, result.ErrorReporting);
			Assert.AreEqual(512, result.FileAlignment);
			Assert.AreEqual(Architecture.Default, result.ForArchitecture);
			Assert.AreEqual(CompileConfigurationType.Default, result.ForConfiguration);
			Assert.AreEqual(false, result.Optimize);
			Assert.AreEqual("Library", result.OutputType);
			Assert.AreEqual("8.0.30703", result.ProductVersion);
			Assert.AreEqual("{A6C4590F-6C2A-41DF-8FB3-41D45E31E413}", result.ProjectGuid);
			Assert.AreEqual("FijoCore.Infrastructure.LightContrib", result.RootNamespace);
			Assert.AreEqual("2.0", result.SchemaVersion);
			Assert.AreEqual(CompileConfigurationType.Debug, result.TargetConfiguration);
			Assert.AreEqual(Architecture.AnyCPU, result.TargetArchitecture);
			Assert.AreEqual(@"R:\FijoCore.Infrastructure.LightContrib\", result.TargetDirectory);
			Assert.AreEqual(Runtime.Net4_0, result.TargetRuntimeVersion);
			Assert.AreEqual(4, result.WarningLevel);
			Assert.AreEqual(string.Empty, result.DocumentationFile);
		}
	}
}