using System;
using System.IO;
using System.Linq;
using Fijo.Development.DependencyResolver.Mapping;
using Fijo.Development.DependencyResolverTest.Properties;
using Microsoft.Build.Evaluation;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class ReferenceMappingTest {
		private ReferenceMapping _referenceMapping;
		private TestData _testData;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_referenceMapping = new ReferenceMapping();
			_testData = new TestData();
		}

		[Test]
		public void Test() {
			var projectFilePath = string.Format("testconfig{0}.xml", Guid.NewGuid().ToString());
			File.WriteAllText(projectFilePath, _testData.DummyCsProjectXml);

			var project = new Project(projectFilePath);
			var resolvedImport = project.Items.Last(x => x.ItemType == "Reference");
			var result = _referenceMapping.Map(resolvedImport);

			Assert.AreEqual("YAXLib", result.Name);
			Assert.AreEqual(@"..\..\lib\YAXLib.dll", result.Path);
		}
	}
}