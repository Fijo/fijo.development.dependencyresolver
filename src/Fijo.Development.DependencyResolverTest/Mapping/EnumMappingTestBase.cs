using Fijo.Development.DependencyResolverTest.Properties;
using Fijo.Infrastructure.DesignPattern.Mapping;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public abstract class EnumMappingTestBase<TEnum> {
		protected IMapping<TEnum, string> ToStringMapping;
		protected IMapping<string, TEnum> StringToMapping;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			ToStringMapping = Kernel.Resolve<IMapping<TEnum, string>>();
			StringToMapping = Kernel.Resolve<IMapping<string, TEnum>>();
		}

		protected void InternalMap_MustReturnCorrectResult(string value, TEnum result) {
			var actual = StringToMapping.Map(value);
			Assert.AreEqual(result, actual);
		}

		protected void InternalMap_MustReturnCorrectResult(TEnum value, string result) {
			var actual = ToStringMapping.Map(value);
			Assert.AreEqual(result, actual);
		}
	}
}