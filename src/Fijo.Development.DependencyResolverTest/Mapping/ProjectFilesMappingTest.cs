using System;
using System.IO;
using System.Linq;
using Fijo.Development.DependencyResolver.Mapping;
using Fijo.Development.DependencyResolverTest.Properties;
using Microsoft.Build.Evaluation;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class ProjectFilesMappingTest {
		private ProjectFilesMapping _projectFilesMapping;
		private TestData _testData;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_projectFilesMapping = new ProjectFilesMapping();
			_testData = new TestData();
		}

		[Test]
		public void Test() {
			var projectFilePath = string.Format("testconfig{0}.xml", Guid.NewGuid().ToString());
			File.WriteAllText(projectFilePath, _testData.DummyCsProjectXml);

			var project = new Project(projectFilePath);
			var result = _projectFilesMapping.Map(project);
			var lastReference = result.References.Last();
			
			Assert.AreEqual("YAXLib", lastReference.Name);
			Assert.AreEqual(@"..\..\lib\YAXLib.dll", lastReference.Path);
		}
	}
}