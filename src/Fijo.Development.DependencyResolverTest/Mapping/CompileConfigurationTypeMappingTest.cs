using Fijo.Development.DependencyResolver.Enum;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class CompileConfigurationTypeMappingTest : EnumMappingTestBase<CompileConfigurationType> {
		[TestCase("", CompileConfigurationType.Default)]
		[TestCase("Debug", CompileConfigurationType.Debug)]
		[TestCase("Release", CompileConfigurationType.Release)]
		public void Value_Map_MustReturnCorrectResult(string value, CompileConfigurationType result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}

		[TestCase(CompileConfigurationType.Default, "")]
		[TestCase(CompileConfigurationType.Debug, "Debug")]
		[TestCase(CompileConfigurationType.Release, "Release")]
		public void Value_Map_MustReturnCorrectResult(CompileConfigurationType value, string result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}
	}
}