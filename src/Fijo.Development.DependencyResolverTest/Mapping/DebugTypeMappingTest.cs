using Fijo.Development.DependencyResolver.Enum;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class DebugTypeMappingTest : EnumMappingTestBase<DebugType> {
		[TestCase("", DebugType.Default)]
		[TestCase("full", DebugType.Full)]
		[TestCase("pdbonly", DebugType.PdbOnly)]
		[TestCase("none", DebugType.None)]
		public void Value_Map_MustReturnCorrectResult(string value, DebugType result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}
		
		[TestCase(DebugType.Default, "")]
		[TestCase(DebugType.Full, "full")]
		[TestCase(DebugType.PdbOnly, "pdbonly")]
		[TestCase(DebugType.None, "none")]
		public void Value_Map_MustReturnCorrectResult(DebugType value, string result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}
	}
}