using FijoCore.Infrastructure.LightContrib.Enums;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class RuntimeMappingTest : EnumMappingTestBase<Runtime> {
		[TestCase("", Runtime.Default)]
		[TestCase("v4.5", Runtime.Net4_5)]
		[TestCase("v4.0", Runtime.Net4_0)]
		[TestCase("v3.5", Runtime.Net3_5)]
		[TestCase("v3.0", Runtime.Net3_0)]
		[TestCase("v2.0", Runtime.Net2_0)]
		[TestCase("v1.1", Runtime.Net1_1)]
		[TestCase("v1.0", Runtime.Net1_0)]
		public void Value_Map_MustReturnCorrectResult(string value, Runtime result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}

		[TestCase("", Runtime.Default)]
		[TestCase("v4.5", Runtime.Net4_5)]
		[TestCase("v4.0", Runtime.Net4_0)]
		[TestCase("v3.5", Runtime.Net3_5)]
		[TestCase("v3.0", Runtime.Net3_0)]
		[TestCase("v2.0", Runtime.Net2_0)]
		[TestCase("v1.1", Runtime.Net1_1)]
		[TestCase("v1.0", Runtime.Net1_0)]
		public void Value_Map_MustReturnCorrectResult(Runtime value, string result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}
	}
}