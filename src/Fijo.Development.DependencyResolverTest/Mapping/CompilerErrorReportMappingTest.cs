using Fijo.Development.DependencyResolver.Enum;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class CompilerErrorReportMappingTest : EnumMappingTestBase<CompilerErrorReport> {
		[TestCase("", CompilerErrorReport.Default)]
		[TestCase("none", CompilerErrorReport.None)]
		[TestCase("prompt", CompilerErrorReport.Prompt)]
		[TestCase("send", CompilerErrorReport.Send)]
		[TestCase("queue", CompilerErrorReport.Queue)]
		public void Value_Map_MustReturnCorrectResult(string value, CompilerErrorReport result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}

		
		[TestCase(CompilerErrorReport.Default, "")]
		[TestCase(CompilerErrorReport.None, "none")]
		[TestCase(CompilerErrorReport.Prompt, "prompt")]
		[TestCase(CompilerErrorReport.Send, "send")]
		[TestCase(CompilerErrorReport.Queue, "queue")]
		public void Value_Map_MustReturnCorrectResult(CompilerErrorReport value, string result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}
	}
}