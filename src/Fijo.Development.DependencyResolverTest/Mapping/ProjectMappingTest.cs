using System;
using System.IO;
using System.Linq;
using Fijo.Development.DependencyResolver.Mapping;
using Fijo.Development.DependencyResolverTest.Properties;
using Microsoft.Build.Evaluation;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class ProjectMappingTest {
		private ProjectMapping _projectMapping;
		private TestData _testData;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_projectMapping = new ProjectMapping();
			_testData = new TestData();
		}

		[Test]
		public void Test() {
			var projectFilePath = string.Format("testconfig{0}.xml", Guid.NewGuid().ToString());
			File.WriteAllText(projectFilePath, _testData.DummyCsProjectXml);

			var project = new Project(projectFilePath);
			var result = _projectMapping.Map(project);

			var lastReference = result.Files.References.Last();
			var directoryPath = Environment.CurrentDirectory;
			var projectPath = Path.Combine(directoryPath, projectFilePath);
			Assert.AreEqual(projectPath, result.Path);
			Assert.AreEqual(directoryPath, result.DictionaryPath);
			Assert.AreEqual("FijoCore.Infrastructure.LightContrib", result.Target.AssemblyName);
			Assert.AreEqual("YAXLib", lastReference.Name);
			var wantedPostBuildEvent = @"copy $(TargetDir)\$(TargetName).dll $(SolutionDir)\..\Fijo.Development.DependencyResolver\lib";
			Assert.AreEqual(wantedPostBuildEvent, result.Event.PostBuildEvent);
		}
	}
}