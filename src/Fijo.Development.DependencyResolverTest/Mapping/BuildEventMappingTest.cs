using System;
using System.IO;
using Fijo.Development.DependencyResolver.Mapping;
using Fijo.Development.DependencyResolverTest.Properties;
using NUnit.Framework;
using Project = Microsoft.Build.Evaluation.Project;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class BuildEventMappingTest {
		private BuildEventMapping _buildEventMapping;
		private TestData _testData;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_buildEventMapping = new BuildEventMapping();
			_testData = new TestData();
		}

		[Test]
		public void Test() {
			var projectFilePath = string.Format("testconfig{0}.xml", Guid.NewGuid().ToString());
			File.WriteAllText(projectFilePath, _testData.DummyCsProjectXml);

			var project = new Project(projectFilePath);
			var result = _buildEventMapping.Map(project);

			const string wantedPostBuildEvent = @"copy $(TargetDir)\$(TargetName).dll $(SolutionDir)\..\Fijo.Development.DependencyResolver\lib";
			Assert.AreEqual(string.Empty, result.PreBuildEvent);
			Assert.AreEqual(wantedPostBuildEvent, result.PostBuildEvent);
		}
	}
}