using Fijo.Development.DependencyResolver.Enum;
using NUnit.Framework;

namespace Fijo.Development.DependencyResolverTest.Mapping {
	[TestFixture]
	public class ArchitectureMappingTest : EnumMappingTestBase<Architecture> {
		[TestCase("", Architecture.Default)]
		[TestCase("AnyCPU", Architecture.AnyCPU)]
		[TestCase("x86", Architecture.X86)]
		[TestCase("x64", Architecture.X64)]
		public void Value_Map_MustReturnCorrectResult(string value, Architecture result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}

		[TestCase(Architecture.Default, "")]
		[TestCase(Architecture.AnyCPU, "AnyCPU")]
		[TestCase(Architecture.X86, "x86")]
		[TestCase(Architecture.X64, "x64")]
		public void Value_Map_MustReturnCorrectResult(Architecture value, string result) {
			InternalMap_MustReturnCorrectResult(value, result);
		}
	}
}